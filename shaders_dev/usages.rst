Usages
======

Depth buffers usage
-------------------

.. csv-table::
    :header: "Name", "Usage"

    "``depthtex0``", "everything"
    "``depthtex1``", "no translucent objects (water, stained glass)"
    "``depthtex2``", "no translucent objects (water, stained glass), no handheld objects"

Shadow buffers usage
--------------------

.. csv-table::
    :header: "Name", "Usage"

    "``shadowtex0``", "everything"
    "``shadowtex1``", "no translucent objects (water, stained glass)"