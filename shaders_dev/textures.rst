Textures
========

.. figure:: /images/shaders_dev/icon_textures.webp
    :align: right
    :height: 180px

    Stone bricks, actually.

Custom textures
---------------

Allows custom textures to be bound to the available shader units.
Format: ``texture.<stage>.<name>=<path>``

``<stage>``:

* ``gbuffers``: gbuffers and shadow programs
* ``deferred``: deferred programs
* ``composite``: composite and final programs

``<name>`` is the texture unit name.

The textures can be loaded from different places:

1. Shader pack
    * The texture path is relative to the folder ``shaders/``: ``texture.composite.colortex1=textures/noise.png``
2. Resource pack
    * The texture path should start with ``minecraft:``: ``texture.composite.colortex2=minecraft:textures/font/ascii.png``
3. Dynamic (lightmap, texture atlas)
    * ``texture.composite.colortex3=minecraft:dynamic/lightmap_1``
    * ``texture.composite.colortex4=minecraft:textures/atlas/blocks.png``

The suffix ``_n`` and ``_s`` can be used to load the normal/specular variant of the texture: ``minecraft:textures/atlas/blocks_n.png``

Raw textures *(binary dump)* can also be loaded: ``texture.<stage>.<name>=<path> <type> <internalFormat> <dimensions> <pixelFormat> <pixelType>``

Where:

* ``<type>`` is one of: ``TEXTURE_1D``, ``TEXTURE_2D``, ``TEXTURE_3D``, ``TEXTURE_RECTANGLE``
* ``<internalFormat>`` is the texture format, see :ref:`shaders_dev/formats:texture formats` for the available names
* ``<dimensions>`` is a list of texture dimensions, depends on the texture type
* ``<pixelFormat>`` is the pixel format, see :ref:`shaders_dev/formats:pixel formats` for the available names
* ``<pixelType>`` is the pixel type, see :ref:`shaders_dev/formats:pixel types` for the available names

It is possible to bind several textures with different types to one texture unit.
The shaders can differentiate between them based on the sampler type: ``sampler1d``, ``sampler2d``, ``sampler3d``...

In one program only one sampler type can be used per texture unit.
The suffixes ``.0`` to ``.9`` can be added to ``<name>`` to avoid duplicate property keys.

Wrap and filter modes can be configured by adding standard texture ``.mcmeta`` files, for example: ``textures/lut_3d.dat.mcmeta``.

GBuffers textures
-----------------

.. csv-table::
    :header: "ID", "Name", "Legacy name"

    "``0``", "gtexture", "texture"
    "``1``", "lightmap", ""
    "``2``", "normals", ""
    "``3``", "specular", ""
    "``4``", "shadowtex0", "shadow, watershadow"
    "``5``", "shadowtex1", "shadow (when watershadow used)"
    "``6``", "depthtex0", ""
    "``7``", "gaux1", "colortex4 <custom texture or output from deferred programs>"
    "``8``", "gaux2", "colortex5 <custom texture or output from deferred programs>"
    "``9``", "gaux3", "colortex6 <custom texture or output from deferred programs>"
    "``10``", "gaux4", "colortex7 <custom texture or output from deferred programs>"
    "``12``", "depthtex1", ""
    "``13``", "shadowcolor0", "shadowcolor "
    "``14``", "shadowcolor1", ""
    "``15``", "noisetex", ""
    "``16``", "colortex8", "<custom texture or output from deferred programs>"
    "``17``", "colortex9", "<custom texture or output from deferred programs>"
    "``18``", "colortex10", "<custom texture or output from deferred programs>"
    "``19``", "colortex11", "<custom texture or output from deferred programs>"
    "``20``", "colortex12", "<custom texture or output from deferred programs>"
    "``21``", "colortex13", "<custom texture or output from deferred programs>"
    "``22``", "colortex14", "<custom texture or output from deferred programs>"
    "``23``", "colortex15", "<custom texture or output from deferred programs>"

Shadow textures
---------------

.. csv-table::
    :header: "ID", "Name", "Legacy name"

    "``0``", "gtexture", "texture"
    "``1``", "lightmap"
    "``2``", "normals"
    "``3``", "specular"
    "``4``", "shadowtex0", "shadow, watershadow", ""
    "``5``", "shadowtex1", "shadow (when watershadow used)"
    "``7``", "gaux1", "colortex4 <custom texture>"
    "``8``", "gaux2", "colortex5 <custom texture>"
    "``9``", "gaux3", "colortex6 <custom texture>"
    "``10``", "gaux4", "colortex7 <custom texture>"
    "``13``", "shadowcolor0", "shadowcolor"
    "``14``", "shadowcolor1", ""
    "``15``", "noisetex"
    "``16``", "colortex8", "<custom texture>"
    "``17``", "colortex9", "<custom texture>"
    "``18``", "colortex10", "<custom texture>"
    "``19``", "colortex11", "<custom texture>"
    "``20``", "colortex12", "<custom texture>"
    "``21``", "colortex13", "<custom texture>"
    "``22``", "colortex14", "<custom texture>"
    "``23``", "colortex15", "<custom texture>"

Composite and deferred textures
-------------------------------

.. csv-table::
    :header: "ID", "Name", "Legacy name"

    "``0``", "colortex0", "gcolor"
    "``1``", "colortex1", "gdepth"
    "``2``", "colortex2", "gnormal"
    "``3``", "colortex3", "composite"
    "``4``", "shadowtex0", "shadow, watershadow"
    "``5``", "shadowtex1", "shadow (when watershadow used)"
    "``6``", "depthtex0", "gdepthtex"
    "``7``", "colortex4", "gaux1"
    "``8``", "colortex5", "gaux2"
    "``9``", "colortex6", "gaux3"
    "``10``", "colortex7", "gaux4"
    "``11``", "depthtex1"
    "``12``", "depthtex2"
    "``13``", "shadowcolor0", "shadowcolor"
    "``14``", "shadowcolor1"
    "``15``", "noisetex"
    "``16``", "colortex8"
    "``17``", "colortex9"
    "``18``", "colortex10"
    "``19``", "colortex11"
    "``20``", "colortex12"
    "``21``", "colortex13"
    "``22``", "colortex14"
    "``23``", "colortex15"
