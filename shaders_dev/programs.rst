Programs
========

.. figure:: /images/shaders_dev/icon_programs.webp
    :align: right
    :height: 180px

    A shiny glistening cone.

.. csv-table::
    :header: "Name", "Render", "When not defined, use"

    "``<none>``", "gui, menus", "``<none>``"

Shadow map
----------

.. csv-table::
    :header: "Name", "Render", "When not defined, use"

    "``shadow``", "everything in shadow pass", "``<none>``"
    "``shadow_solid``", "<not used>", "``shadow``"
    "``shadow_cutout``", "<not used>", "``shadow``"

Shadow composite
----------------

.. csv-table::
    :header: "Name", "Render", "When not defined, use"

    "``shadowcomp``", "<shadowcomp>", "``<none>``"
    "``shadowcomp1``", "<shadowcomp>", "``<none>``"
    "..."
    "``shadowcomp15``", "<shadowcomp>", "``<none>``"

Prepare
-------

.. csv-table::
    :header: "Name", "Render", "When not defined, use"

    "``prepare``", "<prepare>", "``<none>``"
    "``prepare1``", "<prepare>", "``<none>``"
    "..."
    "``prepare15``", "<prepare>", "``<none>``"

GBuffers
--------

.. csv-table::
    :header: "Name", "Render", "When not defined, use"

    "``gbuffers_basic``", "leash, block selection box", "``<none>``"
    "``gbuffers_textured``", "particles", "``gbuffers_basic``"
    "``gbuffers_textured_lit``", "lit particles, world border", "``gbuffers_textured``"
    "``gbuffers_skybasic``", "sky, horizon, stars, void", "``gbuffers_basic``"
    "``gbuffers_skytextured``", "sun, moon", "``gbuffers_textured``"
    "``gbuffers_clouds``", "clouds", "``<none>``"
    "``gbuffers_terrain``", "solid, cutout, cutout_mip", "``gbuffers_textured_lit``"
    "``gbuffers_terrain_solid``", "<not used>", "``gbuffers_terrain``"
    "``gbuffers_terrain_cutout_mip  <not used>``", "``gbuffers_terrain``"
    "``gbuffers_terrain_cutout``", "<not used>", "``gbuffers_terrain``"
    "``gbuffers_damagedblock``", "damaged blocks", "``gbuffers_terrain``"
    "``gbuffers_block``", "block entities", "``gbuffers_terrain``"
    "``gbuffers_beaconbeam``", "beacon beam", "``gbuffers_textured``"
    "``gbuffers_item``", "<not used>", "``gbuffers_textured_lit``"
    "``gbuffers_entities``", "entities", "``gbuffers_textured_lit``"
    "``gbuffers_entities_glowing``", "glowing entities, spectral effect", "``gbuffers_entities``"
    "``gbuffers_armor_glint``", "glint on armor and handheld items", "``gbuffers_textured``"
    "``gbuffers_spidereyes``", "eyes of spider, enderman and dragon", "``gbuffers_textured``"
    "``gbuffers_hand``", "hand and opaque handheld objects", "``gbuffers_textured_lit``"
    "``gbuffers_weather``", "rain, snow", "``gbuffers_textured_lit``"

GBuffers translucent
^^^^^^^^^^^^^^^^^^^^

.. csv-table::
    :header: "Name", "Render", "When not defined, use"

    "``gbuffers_water``", "translucent", "``gbuffers_terrain``"
    "``gbuffers_hand_water``", "translucent handheld objects", "``gbuffers_hand``"

Deferred
--------

.. csv-table::
    :header: "Name", "Render", "When not defined, use"

    "``deferred_pre``", "<virtual> flip ping-pong buffers", "``<none>``"
    "``deferred``", "<deferred>", "``<none>``"
    "``deferred1``", "<deferred>", "``<none>``"
    "..."
    "``deferred15``", "<deferred>", "``<none>``"

Composite
---------

.. csv-table::
    :header: "Name", "Render", "When not defined, use"

    "``composite_pre``", "<virtual> flip ping-pong buffers", "``<none>``"
    "``composite``", "<composite>", "``<none>``"
    "``composite1``", "<composite>", "``<none>``"
    "..."
    "``composite15``", "<composite>", "``<none>``"

Final
-----

.. csv-table::
    :header: "Name", "Render", "When not defined, use"

    "``final``", "<final>", "``<none>``"

.. important:: The programs ``shadow_solid``, ``shadow_cutout``, ``gbuffers_terrain_solid``, ``gbuffers_terrain_cutout`` and ``gbuffers_terrain_cutout_mip`` are not used

.. attention:: **TO-DO**: Separate programs for world border, entities (by id, by type), cape, elytra, wolf collar, etc.
