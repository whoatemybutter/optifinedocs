Indexes
=======

Draw buffer index
-----------------

.. csv-table::
    :header: "Prefix", "Index"

    "``colortex<0-15>``", "0-15"
    "``gcolor``", "0"
    "``gdepth``", "1"
    "``gnormal``", "2"
    "``composite``", "3"
    "``gaux1``", "4"
    "``gaux2``", "5"
    "``gaux3``", "6"
    "``gaux4``", "7"

Shadow buffer index
-------------------

.. csv-table::
    :header: "Prefix", "Index"

    "``shadowcolor``", "0"
    "``shadowcolor<0-1>``", "0-1"