Options screen
==============

.. figure:: /images/shaders_dev/icon_options.webp
    :align: right
    :height: 180px

    Pushy pushy.

Shader options are parsed from the ``.fsh`` and ``.vsh`` files located in the folder ``shaders``.
The line comment located after the option is shown as a tooltip.

Tooltip lines are split on sentence end ``". "`` *(period space)*.
Tooltip lines ending with ``!`` are automatically shown red.

One option can be present in several shader files and it will be switched simultaneously in all of them.
Ambiguous options *(different default values found)* are **disabled and can not be changed**.

* Left-clicking on an option button selects the next value,
* Right-clicking selects the previous value,
* Shift-clicking resets the option to default value

Boolean, default ON:

.. code:: c

    #define SSAO       // Screen space ambient occlusion. High performance impact.

Boolean, default OFF:

.. code:: c

    // #define SSAO    // Screen space ambient occlusion. High performance impact.

The boolean variables are recognized only if the matching "#ifdef" or "#ifndef" is also found in the same file.

List:

.. code:: c

    #define SHADOW_DARKNESS 0.10   // Shadow darkness levels [0.05 0.10 0.20]

The allowed values are given as a list ``[v1 v2 v3]`` in the comment.
The default value is automatically added if not present in the list.

Some ``const`` variables are also recognized *(for backwards compatibility with the Shaders Mod)*.
They use a structure similar to the macro variables, for example:

.. code:: c

    const int shadowMapResolution  1572;  // Shadowmap resolution [1024 1572 2048]
    const float shadowDistance     64.0;  // Draw distance of shadows [32.0 64.0 128.0 256.0]

Const variables without allowed values are, by default, not visible, unless used in a profile or configured on a screen.

The recognized ``const`` variables are:

.. code:: c

    shadowMapResolution
    shadowDistance
    shadowDistanceRenderMul
    shadowIntervalSize
    generateShadowMipmap
    generateShadowColorMipmap
    shadowHardwareFiltering
    shadowHardwareFiltering0
    shadowHardwareFiltering1
    shadowtex0Mipmap
    shadowtexMipmap
    shadowtex1Mipmap
    shadowcolor0Mipmap
    shadowColor0Mipmap
    shadowcolor1Mipmap
    shadowColor1Mipmap
    shadowtex0Nearest
    shadowtexNearest
    shadow0MinMagNearest
    shadowtex1Nearest
    shadow1MinMagNearest
    shadowcolor0Nearest
    shadowColor0Nearest
    shadowColor0MinMagNearest
    shadowcolor1Nearest
    shadowColor1Nearest
    shadowColor1MinMagNearest
    wetnessHalflife
    drynessHalflife
    eyeBrightnessHalflife
    centerDepthHalflife
    sunPathRotation
    ambientOcclusionLevel
    superSamplingLevel
    noiseTextureResolution

Labels and tooltips
-------------------

User friendly **option** labels can be loaded from language files in ``/shaders/lang/``.
Example from "/shaders/lang/en_us.lang":

.. code:: ini

    option.SHADOW_FILTER=Shadow Filter
    option.SHADOW_FILTER.comment=Smooth out edges of shadows. Very small performance hit.

User friendly **value** labels can be loaded from language files in ``/shaders/lang/``.
Example from "/shaders/lang/en_us.lang":

.. code:: ini

    value.SHADOW_FILTER.0.4f=Normal
    value.SHADOW_FILTER.0.9f=Soft

Value formatting can be added with:

.. code:: ini

    prefix.SHADOW_FILTER=(
    suffix.SHADOW_FILTER=)

Profile tooltips can be loaded from language files in ``/shaders/lang/``.
Example from "/shaders/lang/en_us.lang":

.. code:: ini

    profile.comment=Low - low. Medium - medium. Standard - standard. High - high. Ultra - ultra.

Profiles
--------

Profiles allow a set of options to be switched together
The current profile is detected based on the selected option values
If no profile matches the current option values, the profile "Custom" is selected

It is recommended that all profiles use the same list of options and only the values differ.
Disabled programs are special options and only disabling *(prefix !)* is recognized for them.

Format: ``# profile.<name>=<list of options>``

Profile options
~~~~~~~~~~~~~~~

* ``OPTION:value``: set value
* ``OPTION``: set boolean option **ON**
* ``!OPTION``: set boolean option **OFF**
* ``profile.NAME``: copy all options from another profile
* ``!program.name``: disable program ``name``. The program name may include dimension: ``world-1/gbuffers_water``

The following program names are recognized:

.. code:: c

    gbuffers_basic
    gbuffers_textured
    gbuffers_textured_lit
    gbuffers_skybasic
    gbuffers_skytextured
    gbuffers_clouds
    gbuffers_terrain
    gbuffers_terrain_solid
    gbuffers_terrain_cutout_mip
    gbuffers_terrain_cutout
    gbuffers_damagedblock
    gbuffers_water
    gbuffers_block
    gbuffers_beaconbeam
    gbuffers_item
    gbuffers_entities
    gbuffers_armor_glint
    gbuffers_spidereyes
    gbuffers_hand
    gbuffers_weather
    composite
    composite1
    composite2
    composite3
    composite4
    composite5
    composite6
    composite7
    final
    shadow
    shadow_solid
    shadow_cutout
    deferred
    deferred1
    deferred2
    deferred3
    deferred4
    deferred5
    deferred6
    deferred7
    gbuffers_hand_water

Profile examples
~~~~~~~~~~~~~~~~

.. code:: ini

    profile.LOW=SSAO:false GOD_RAYS:false SHADOW_DIST:40 !program.composite1
    profile.MED=profile.LOW GOD_RAYS SHADOW_DIST:80
    profile.HIGH=SSAO GOD_RAYS SHADOW_DIST:120

User friendly profile labels can be loaded from language files in "/shaders/lang"
Example from "/shaders/lang/en_us.lang":

.. code:: ini

    profile.LOW=Low
    profile.LOW.comment=Low quality. Intel and Mac compatible. No God Rays and SSAO
    profile.MED=Medium
    profile.MED.comment=Medium quality. Nvidia or AMD graphics card recommended
    profile.HIGH=High
    profile.HIGH.comment=High quality. Modern Nvidia or AMD graphics card required

Screen
------

* Main screen: ``screen=<list of options>``
* Sub-screen: ``screen.<NAME>=<list of options>``

Screen options
~~~~~~~~~~~~~~

.. csv-table::
    :header: "Option", "Option name"

    "[``NAME``]", "link to sub-screen ``NAME``"
    "<profile>", "profile selection"
    "<empty>", "empty slot"
    "\*", "the rest of the options not configured on any of the screens"

Screen columns
^^^^^^^^^^^^^^

By default the options are shown in two columns:

.. csv-table::

    1, 2
    3, 4
    5, 6
    ..., ...

When more than 18 options are present, the screen switches to 3 or more columns.
The option names are automatically shortened to avoid text overflow outside the button.

* Main screen: ``screen.columns=2``
* Sub-screen: ``screen.<NAME>.columns=2``

Example
-------

.. code:: ini

    screen=<empty> <empty> <profile> ABOUT <empty> <empty> [VISUAL] [POST_PROCESS] [SHADOWS] [COLOR] [SKY] [WATER] [TERRAIN] [WORLD]
    screen.VISUAL=<empty> <empty> AO AO_STRENGTH LIGHT_SHAFT LIGHT_SHAFT_STRENGTH DESATURATION DESATURATION_FACTOR REFLECTION REFLECTION_TRANSLUCENT ADVANCED_MATERIALS [ADVANCED_MATERIALS] BLACK_OUTLINE PROMO_OUTLINE TOON_LIGHTMAP WHITE_WORLD
    screen.ADVANCED_MATERIALS=<empty> <empty> MATERIAL_FORMAT REFLECTION_SPECULAR REFLECTION_RAIN REFLECTION_ROUGH REFLECTION_PREVIOUS ALBEDO_METAL <empty> <empty> PARALLAX PARALLAX_DEPTH SELF_SHADOW SELF_SHADOW_ANGLE PARALLAX_QUALITY PARALLAX_DISTANCE DIRECTIONAL_LIGHTMAP DIRECTIONAL_LIGHTMAP_STRENGTH
    screen.SUN_EFFECTS=SUN_EFFECTS GOD_RAYS LENS_FLARE RAINDROPS
    screen.WAVING_GRASS=WAVING_GRASS WAVING_LEAVES WAVING_VINES
    screen.MISC=*

User friendly screen labels can be loaded from language files in ``/shaders/lang/``.
Example from "/shaders/lang/en_us.lang":

.. code:: ini

    screen.DOF=Depth of field
    screen.DOF.comment=Depth of field effect. Adds blur to out of focus objects.
    screen.WAVING=Waving grass and leaves
    screen.WAVING.comment=Waving grass, leaves, fire and entities
