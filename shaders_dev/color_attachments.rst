Color attachments
=================

.. figure:: /images/shaders_dev/icon_color_attachments.webp
    :align: right
    :height: 180px

    Texture is "attached".

The data is passed from shader to shader using color attachments.

There are at least 4 for all machines.
For machines that can support it, there are up to 16.

.. warning:: MacOS is limited to 8 color attachments, even with a modern GPU

In the deferred, composite, and final shaders, these are referenced by the ``gcolor``, ``gdepth``, ``gnormal``, ``composite``, ``gaux1``, ``gaux2``, ``gaux3``, and ``gaux4`` uniforms.

.. note:: ``colortex0`` to ``colortex15`` can be used instead of ``gcolor``, ``gdepth``, etc.

Despite the naming, ``gnormal`` and onward are the same and can be used for any purpose, ``gcolor`` and ``gdepth`` have meaning:

* The first one, ``gcolor``, has its color cleared to the current fog color before rendering.
* The second one, ``gdepth``, has its color cleared to solid white before rendering and uses a higher precision storage buffer suitable for storing depth values.
* The rest have their color cleared to black with 0 alpha.

Each color attachment uses 2 buffers (``A`` and ``B``) with logical names ``"main"`` and ``"alt"``, which can be used as ping-pong buffers.
When the buffers are flipped, the mapping between ``main``/``alt`` and ``A``/``B`` is reversed.

Gbuffer programs always read from ``main`` (only ``gaux1-4``) and write to ``main`` buffers *(they shouldn't read and write to the same buffer at the same time)*.

Deferred/composite programs always read from ``main`` and write to ``alt`` buffers.
After a deferred/composite program is rendered, the buffers that it writes to are *flipped* so the next programs can see the current output as input.

The property ``flip.<program>.<buffer>=<true|false>`` can be used to enable or disable the flip, independent of the buffer write.
The virtual programs "``deferred_pre``" and "``composite_pre``" can be used for buffer flipping before the deferred/composite pass.

Output color attachments are configured with the "``/* DRAWBUFFERS:XYZ */``" or "``/* RENDERTARGETS: X,Y,Z */``" comment, placed in the fragment shader.
Gbuffers, deferred and composite programs can write to any color attachment, **but no more than 8 at the same time**.

If the output color attachments are not configured, then the program will **write to the first 8** color attachments.

When writing to the color attachments in the composite shader, *blending is disabled*.
Writing to color attachments that the composite shader also reads from will generate artifacts, *unless the original contents are simply copied*.
