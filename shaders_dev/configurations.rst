Configurations
==============

.. figure:: /images/shaders_dev/icon_configurations.webp
    :align: right
    :height: 180px

    Slide them around.

Vertex configuration
--------------------

.. csv-table::
    :header: "Source", "Effect", "Comment"

    "``in vec3 mc_Entity;``", "useEntityAttrib = true", ""
    "``in vec2 mc_midTexCoord;``", "useMidTexCoordAttrib = true", ""
    "``in vec4 <type> at_tangent;``", "useTangentAttrib = true", ""
    "``const int countInstances = 1;``", "when ``countInstances > 1`` the geometry will be rendered several times, see uniform ``instanceId``"

Geometry configuration
----------------------

.. csv-table::
    :header: "Source", "Effect", "Comment"

    "``#extension GL_ARB_geometry_shader4 : enable``", "Enable GL_ARB_geometry_shader4"
    "``const int maxVerticesOut = 3;``", "Set GEOMETRY_VERTICES_OUT_ARB for GL_ARB_geometry_shader4 "

Fragment configuration
----------------------

.. csv-table::
    :header: "Source", "Effect", "Comment"

    "``uniform <type> shadow;``", "shadowDepthBuffers = 1"
    "``uniform <type> watershadow;``", "shadowDepthBuffers = 2"
    "``uniform <type> shadowtex0;``", "shadowDepthBuffers = 1"
    "``uniform <type> shadowtex1;``", "shadowDepthBuffers = 2"
    "``uniform <type> shadowcolor;``", "shadowColorBuffers = 1"
    "``uniform <type> shadowcolor0;``", "shadowColorBuffers = 1"
    "``uniform <type> shadowcolor1;``", "shadowColorBuffers = 2"
    "``uniform <type> depthtex0;``", "depthBuffers = 1"
    "``uniform <type> depthtex1;``", "depthBuffers = 2"
    "``uniform <type> depthtex2;``", "depthBuffers = 3"
    "``uniform <type> gdepth;``", "if (bufferFormat[1] == RGBA) bufferFormat[1] = RGBA32F;"
    "``uniform <type> gaux1;``", "colorBuffers = 5"
    "``uniform <type> gaux2;``", "colorBuffers = 6"
    "``uniform <type> gaux3;``", "colorBuffers = 7"
    "``uniform <type> gaux4;``", "colorBuffers = 8"
    "``uniform <type> colortex4;``", "colorBuffers = 5"
    "``uniform <type> colortex5;``", "colorBuffers = 6"
    "``uniform <type> colortex6;``", "colorBuffers = 7"
    "``uniform <type> colortex7;``", "colorBuffers = 8"
    "``uniform <type> centerDepthSmooth;``", "centerDepthSmooth = true"
    "``/* SHADOWRES:1024 */``", "shadowMapWidth = shadowMapHeight = 1024"
    "``const int shadowMapResolution = 1024;``", "shadowMapWidth = shadowMapHeight = 1024"
    "``/* SHADOWFOV:90.0 */``", "shadowMapFov = 90"
    "``const float shadowMapFov = 90.0;``", "shadowMapFov = 90"
    "``/* SHADOWHPL:160.0 */``", "shadowMapDistance = 160.0"
    "``const float shadowDistance = 160.0f;``", "shadowMapDistance = 160.0"
    "``const float shadowDistanceRenderMul = -1f;``", "shadowDistanceRenderMul = -1", "When > 0 enable shadow optimization (shadowRenderDistance = shadowDistance * shadowDistanceRenderMul)"
    "``const float shadowIntervalSize = 2.0f;``", "shadowIntervalSize = 2.0"
    "``const bool generateShadowMipmap = true;``", "shadowMipmap = true"
    "``const bool generateShadowColorMipmap = true;``", "shadowColorMipmap = true"
    "``const bool shadowHardwareFiltering = true;``", "shadowHardwareFiltering = true"
    "``const bool shadowHardwareFiltering0 = true;``", "shadowHardwareFiltering[0] = true"
    "``const bool shadowHardwareFiltering1 = true;``", "shadowHardwareFiltering[1] = true"
    "``const bool shadowtexMipmap = true;``", "shadowMipmap[0] = true"
    "``const bool shadowtex0Mipmap = true;``", "shadowMipmap[0] = true"
    "``const bool shadowtex1Mipmap = true;``", "shadowMipmap[1] = true"
    "``const bool shadowcolor0Mipmap = true;``", "shadowColorMipmap[0] = true"
    "``const bool shadowColor0Mipmap = true;``", "shadowColorMipmap[0] = true"
    "``const bool shadowcolor1Mipmap = true;``", "shadowColorMipmap[1] = true"
    "``const bool shadowColor1Mipmap = true;``", "shadowColorMipmap[1] = true"
    "``const bool shadowtexNearest = true;``", "shadowFilterNearest[0] = true"
    "``const bool shadowtex0Nearest = true;``", "shadowFilterNearest[0] = true"
    "``const bool shadow0MinMagNearest = true;``", "shadowFilterNearest[0] = true"
    "``const bool shadowtex1Nearest = true;``", "shadowFilterNearest[1] = true"
    "``const bool shadow1MinMagNearest = true;``", "shadowFilterNearest[1] = true"
    "``const bool shadowcolor0Nearest = true;``", "shadowColorFilterNearest[0] = true"
    "``const bool shadowColor0Nearest = true;``", "shadowColorFilterNearest[0] = true"
    "``const bool shadowColor0MinMagNearest = true;``", "shadowColorFilterNearest[0] = true"
    "``const bool shadowcolor1Nearest = true;``", "shadowColorFilterNearest[1] = true"
    "``const bool shadowColor1Nearest = true;``", "shadowColorFilterNearest[1] = true"
    "``const bool shadowColor1MinMagNearest = true;``", "shadowColorFilterNearest[1] = true"
    "``/* WETNESSHL:600.0 */``", "wetnessHalfLife = 600 (ticks)"
    "``const float wetnessHalflife = 600.0f;``", "wetnessHalfLife = 600 (ticks)"
    "``/* DRYNESSHL:200.0 */``", "drynessHalfLife = 200 (ticks)"
    "``const float drynessHalflife = 200.0f;``", "drynessHalfLife = 200 (ticks)"
    "``const float eyeBrightnessHalflife = 10.0f;``", "eyeBrightnessHalflife = 10 (ticks)"
    "``const float centerDepthHalflife = 1.0f;``", "centerDepthSmoothHalflife = 1 (ticks)"
    "``const float sunPathRotation = 0f;``", "sunPathRotation = 0f"
    "``const float ambientOcclusionLevel = 1.0f;``", "ambientOcclusionLevel = 1.0f", "0.0f = AO disabled, 1.0f = vanilla AO"
    "``const int superSamplingLevel = 1;``", "superSamplingLevel = 1"
    "``const int noiseTextureResolution = 256;``", "noiseTextureResolution = 256"
    "``/* GAUX4FORMAT:RGBA32F */``", "buffersFormat[7] = GL_RGBA32F"
    "``/* GAUX4FORMAT:RGB32F */``", "buffersFormat[7] = GL_RGB32F"
    "``/* GAUX4FORMAT:RGB16 */``", "buffersFormat[7] = GL_RGB16"
    "``const int <bufferIndex>Format = <format>;``", "bufferFormats[index] = <format>", "See :ref:`shaders_dev/indexes:draw buffer index` and :ref:`shaders_dev/formats:texture formats`"
    "``const bool <bufferIndex>Clear = false;``", "gbuffersClear[index] = false", "Skip glClear() for the given buffer, only for ``composite`` and ``deferred`` programs"
    "``const vec4 <bufferIndex>ClearColor = vec4();``", "gbuffersClearColor[index] = vec4(r, g, b, a)", "Clear color for the given buffer, only for ``composite`` and ``deferred`` programs"
    "``const bool <bufferIndex>MipmapEnabled = true;``", "bufferMipmaps[index] = true", "Only for programs ``composite`` , ``deferred`` and ``final``"
    "``const int <shadowBufferIx>Format = <format>;``", "shadowBufferFormats[index] = <format>", "See :ref:`shaders_dev/indexes:shadow buffer index` and :ref:`shaders_dev/formats:texture formats`"
    "``const bool <shadowBufferIx>Clear = false;``", "shadowBuffersClear[index] = false", "Skip ``glClear()`` for the given shadow color buffer"
    "``const vec4 <shadowBufferIx>ClearColor = vec4();``", "shadowBuffersClearColor[index] = vec4(r, g, b, a)", "Clear color for the given shadow color buffer"
    "``/* DRAWBUFFERS:0257 */``", "drawBuffers = {0, 2, 5, 7)", "Only buffers 0 to 9 can be used."
    "``/* RENDERTARGETS: 0,2,11,15 */``", "drawBuffers = {0, 2, 11, 15}", "Buffers 0 to 15 can be used"
