Formats
=======

.. figure:: /images/shaders_dev/icon_formats.webp
    :align: right
    :height: 180px
    :class: pixel-edges

    RGB BGR GBR RBG BRG.

Texture formats
---------------

8-bit
~~~~~

.. csv-table::
    :header: "Normalized", "Signed normalized", "Integer", "Unsigned integer"

    "R8", "R8_SNORM", "R8I", "R8I"
    "RG8", "RG8_SNORM", "RG8I", "RG8I"
    "RGB8", "RGB8_SNORM", "RGB8I", "RGB8I"
    "RGBA8", "RGBA8_SNORM", "RGBA8I", "RGBA8I"

16-bit
~~~~~~


.. csv-table::
    :header: "Normalized", "Signed normalized", "Float", "Integer", "Unsigned integer"

    "R16", "R16_SNORM", "R16F", "R16I", "R16UI"
    "RG16", "RG16_SNORM", "RG16F", "RG16I", "RG16UI"
    "RGB16", "RGB16_SNORM", "RGB16F", "RGB16I", "RGB16UI"
    "RGBA16", "RGBA16_SNORM", "RGBA16F", "RGBA16I", "RGBA16UI"

32-bit
~~~~~~

.. csv-table::
    :header: "Float", "Integer", "Unsigned integer"

    "R32F", "R32I", "R32UI"
    "RG32F", "RG32I", "RG32UI"
    "RGB32F", "RGB32I", "RGB32UI"
    "RGBA32F", "RGBA32I", "RGBA32UI"

Mixed
~~~~~

.. attention:: This documentation does not information about these

* R3_G3_B2
* RGB5_A1
* RGB10_A2
* R11F_G11F_B10F
* RGB9_E5

Pixel formats
-------------

Normalized
~~~~~~~~~~

* RED
* RG
* RGB
* BGR
* RGBA
* BGRA

Integer
~~~~~~~

* RED_INTEGER
* RG_INTEGER
* RGB_INTEGER
* BGR_INTEGER
* RGBA_INTEGER
* BGRA_INTEGER

Pixel types
-----------

* BYTE
* SHORT
* INT
* HALF_FLOAT
* FLOAT
* UNSIGNED_BYTE
* UNSIGNED_BYTE_3_3_2
* UNSIGNED_BYTE_2_3_3_REV
* UNSIGNED_SHORT
* UNSIGNED_SHORT_5_6_5
* UNSIGNED_SHORT_5_6_5_REV
* UNSIGNED_SHORT_4_4_4_4
* UNSIGNED_SHORT_4_4_4_4_REV
* UNSIGNED_SHORT_5_5_5_1
* UNSIGNED_SHORT_1_5_5_5_REV
* UNSIGNED_INT
* UNSIGNED_INT_8_8_8_8
* UNSIGNED_INT_8_8_8_8_REV
* UNSIGNED_INT_10_10_10_2
* UNSIGNED_INT_2_10_10_10_REV
