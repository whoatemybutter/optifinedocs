ID mapping
==========

.. figure:: /images/shaders_dev/icon_id_mapping.webp
    :align: right
    :height: 180px

    One of many numbers.

Block ID mapping
----------------

The block ID mapping is defined in ``shaders/block.properties`` included in the shader pack.

Forge mods may add custom block mapping as ``assets/<modid>/shaders/block.properties`` in the mod JAR file.
The ``block.properties`` file can use conditional :ref:`shaders_dev/preprocessor:compilation directives` *(#ifdef, #if, etc.)*

For more details see section :ref:`shaders_dev/preprocessor:macros` A to I.
Option macros are also available.
Format: ``block.<id>=<block1> <block2> ...``
The key is the substitute block ID, the values are the blocks which are to be replaced.
Only one line per block ID is allowed.

See :doc:`/syntax` for the block matching rules.

.. code:: properties

    # Short format
    block.31=red_flower yellow_flower reeds

    # Long format
    block.32=minecraft:red_flower ic2:nether_flower botania:reeds

    # Properties
    block.33=minecraft:red_flower:type=white_tulip minecraft:red_flower:type=pink_tulip botania:reeds:type=green

Item ID mapping
---------------

The item ID mapping is defined in ``shaders/item.properties`` included in the shader pack.
Forge mods may add custom item mapping as ``/assets/<modid>/shaders/item.properties`` in the mod JAR file.

See `Block ID Mapping`_ for the overview.

.. code:: ini

    # Short format
    item.5000=diamond_sword dirt

    # Long format
    item.5001=minecraft:diamond_sword botania:reeds

Entity ID mapping
-----------------

The entity ID mapping is defined in ``shaders/entity.properties`` included in the shader pack.
Forge mods may add custom entity mapping as ``assets/<modid>/shaders/entity.properties`` in the mod JAR file.

See `Block ID Mapping`_ for the overview.

.. code:: ini

    # Short format
    entity.2000=sheep cow

    # Long format
    entity.2001=minecraft:pig botania:pixie
