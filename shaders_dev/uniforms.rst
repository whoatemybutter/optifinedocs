Uniforms
========

General uniforms
----------------

.. csv-table::
    :header: "Source", "Value"

    "``uniform int heldItemId;``", "held item ID (main hand), used only for items defined in ``item.properties``"
    "``uniform int heldBlockLightValue;``", "held item light value (main hand)"
    "``uniform int heldItemId2;``", "held item ID (off hand), used only for items defined in ``item.properties``"
    "``uniform int heldBlockLightValue2;``", "held item light value (off hand)"
    "``uniform int fogMode;``", "GL_LINEAR, GL_EXP or GL_EXP2"
    "``uniform float fogDensity;``", "0.0-1.0"
    "``uniform vec3 fogColor;``", "r, g, b"
    "``uniform vec3 skyColor;``", "r, g, b"
    "``uniform int worldTime;``", "<ticks> = worldTicks % 24000"
    "``uniform int worldDay;``", "<days> = worldTicks / 24000"
    "``uniform int moonPhase;``", "0-7"
    "``uniform int frameCounter;``", "Frame index (0 to 720719, then resets to 0)"
    "``uniform float frameTime;``", "last frame time, seconds"
    "``uniform float frameTimeCounter;``", "run time, seconds (resets to 0 after 3600s)"
    "``uniform float sunAngle;``", "0.0-1.0"
    "``uniform float shadowAngle;``", "0.0-1.0"
    "``uniform float rainStrength;``", "0.0-1.0"
    "``uniform float aspectRatio;``", "viewWidth / viewHeight"
    "``uniform float viewWidth;``", "viewWidth"
    "``uniform float viewHeight;``", "viewHeight"
    "``uniform float near;``", "near viewing plane distance"
    "``uniform float far;``", "far viewing plane distance"
    "``uniform vec3 sunPosition;``", "sun position in eye space"
    "``uniform vec3 moonPosition;``", "moon position in eye space"
    "``uniform vec3 shadowLightPosition;``", "shadow light (sun or moon) position in eye space"
    "``uniform vec3 upPosition;``", "direction up"
    "``uniform vec3 cameraPosition;``", "camera position in world space"
    "``uniform vec3 previousCameraPosition;``", "last frame cameraPosition"
    "``uniform mat4 gbufferModelView;``", "modelview matrix after setting up the camera transformations"
    "``uniform mat4 gbufferModelViewInverse;``", "inverse gbufferModelView"
    "``uniform mat4 gbufferPreviousModelView;``", "last frame gbufferModelView"
    "``uniform mat4 gbufferProjection;``", "projection matrix when the gbuffers were generated"
    "``uniform mat4 gbufferProjectionInverse;``", "inverse gbufferProjection"
    "``uniform mat4 gbufferPreviousProjection;``", "last frame gbufferProjection"
    "``uniform mat4 shadowProjection;``", "projection matrix when the shadow map was generated"
    "``uniform mat4 shadowProjectionInverse;``", "inverse shadowProjection"
    "``uniform mat4 shadowModelView;``", "modelview matrix when the shadow map was generated"
    "``uniform mat4 shadowModelViewInverse;``", "inverse shadowModelView"
    "``uniform float wetness;``", "rainStrength smoothed with wetnessHalfLife or drynessHalfLife"
    "``uniform float eyeAltitude;``", "view entity Y position"
    "``uniform ivec2 eyeBrightness;``", "x = block brightness, y = sky brightness, light 0-15 = brightness 0-240 "
    "``uniform ivec2 eyeBrightnessSmooth;``", "eyeBrightness smoothed with eyeBrightnessHalflife"
    "``uniform ivec2 terrainTextureSize;``", "not used"
    "``uniform int terrainIconSize;``", "not used"
    "``uniform int isEyeInWater;``", "1 = camera is in water, 2 = camera is in lava, 3 = camera is in powdered snow"
    "``uniform float nightVision;``", "night vision (0.0-1.0)"
    "``uniform float blindness;``", "blindness (0.0-1.0)"
    "``uniform float screenBrightness;``", "screen brightness (0.0-1.0)"
    "``uniform int hideGUI;``", "GUI is hidden"
    "``uniform float centerDepthSmooth;``", "centerDepth smoothed with centerDepthSmoothHalflife"
    "``uniform ivec2 atlasSize;``", "texture atlas size (only set when the atlas texture is bound)"
    "``uniform vec4 spriteBounds;``", "sprite bounds in the texture atlas (u0, v0, u1, v1), set when MC_ANISOTROPIC_FILTERING is enabled"
    "``uniform vec4 entityColor;``", "entity color multiplier (entity hurt, creeper flashing when exploding)"
    "``uniform int entityId;``", "entity ID"
    "``uniform int blockEntityId;``", "block entity ID (block ID for the tile entity, only for blocks specified in ``block.properties``)"
    "``uniform ivec4 blendFunc;``", "blend function (srcRGB, dstRGB, srcAlpha, dstAlpha)"
    "``uniform int instanceId;``", "instance ID when instancing is enabled (countInstances > 1), 0 = original, 1-N = copies"
    "``uniform float playerMood;``", "player mood (0.0-1.0), increases the longer a player stays underground"
    "``uniform int renderStage;``", "render stage, see :ref:`shaders_dev/preprocessor:macros`, :ref:`shaders_dev/preprocessor:j. render stages`"
    "**1.17+ Options Below**", ""
    "``uniform mat4 modelViewMatrix;``", "model view matrix"
    "``uniform mat4 modelViewMatrixInverse;``", "modelViewMatrixInverse"
    "``uniform mat4 projectionMatrix;``", "projectionMatrix"
    "``uniform mat4 projectionMatrixInverse;``", "projectionMatrixInverse"
    "``uniform mat4 textureMatrix = mat4(1.0);``", "textureMatrix = mat4(1.0)"
    "``uniform mat3 normalMatrix;``", "normal matrix"
    "``uniform vec3 chunkOffset;``", "with attribute ``vaPosition``"
    "``uniform vec3 modelOffset;``", "used by terrain chunks (with attribute ``vaPosition``) and clouds, replaces chunkOffset **(1.21.2+)**"
    "``uniform float alphaTestRef;``", "alpha test reference value, the check is ``if (color.a < alphaTestRef) discard;``"
    "``uniform float darknessFactor``", "strength of the darkness effect (0.0-1.0)"
    "``uniform float darknessLightFactor``", "lightmap variations caused by the darkness effect (0.0-1.0)"

GBuffers uniforms
-----------------

.. note:: Programs: basic, textured, textured_lit, skybasic, skytextured, clouds, terrain, terrain_solid, terrain_cutout_mip, terrain_cutout, damagedblock, water, block, beaconbeam, item, entities, armor_glint, spidereyes, hand, hand_water, weather

.. csv-table::
    :header: "Source", "Value"

    "``uniform sampler2D gtexture;``", "0"
    "``uniform sampler2D lightmap;``", "1"
    "``uniform sampler2D normals;``", ""
    "``uniform sampler2D specular;``", "3"
    "``uniform sampler2D shadow;``", "waterShadowEnabled ? 5 : 4"
    "``uniform sampler2D watershadow;``", "4"
    "``uniform sampler2D shadowtex0;``", "4"
    "``uniform sampler2D shadowtex1;``", "5"
    "``uniform sampler2D depthtex0;``", "6"
    "``uniform sampler2D gaux1;``", "7 <custom texture or output from deferred programs>"
    "``uniform sampler2D gaux2;``", "8 <custom texture or output from deferred programs>"
    "``uniform sampler2D gaux3;``", "9 <custom texture or output from deferred programs>"
    "``uniform sampler2D gaux4;``", "10 <custom texture or output from deferred programs>"
    "``uniform sampler2D colortex4;``", "7 <custom texture or output from deferred programs>"
    "``uniform sampler2D colortex5;``", "8 <custom texture or output from deferred programs>"
    "``uniform sampler2D colortex6;``", "9 <custom texture or output from deferred programs>"
    "``uniform sampler2D colortex7;``", "10 <custom texture or output from deferred programs>"
    "``uniform sampler2D colortex8;``", "16 <custom texture or output from deferred programs>"
    "``uniform sampler2D colortex9;``", "17 <custom texture or output from deferred programs>"
    "``uniform sampler2D colortex10;``", "18 <custom texture or output from deferred programs>"
    "``uniform sampler2D colortex11;``", "19 <custom texture or output from deferred programs>"
    "``uniform sampler2D colortex12;``", "20 <custom texture or output from deferred programs>"
    "``uniform sampler2D colortex13;``", "21 <custom texture or output from deferred programs>"
    "``uniform sampler2D colortex14;``", "22 <custom texture or output from deferred programs>"
    "``uniform sampler2D colortex15;``", "23 <custom texture or output from deferred programs>"
    "``uniform sampler2D depthtex1;``", "11"
    "``uniform sampler2D shadowcolor;``", "13"
    "``uniform sampler2D shadowcolor0;``", "13"
    "``uniform sampler2D shadowcolor1;``", "14"
    "``uniform sampler2D noisetex;``", "15"


Shadow uniforms
---------------

.. note:: Programs: shadow, shadow_solid, shadow_cutout

.. csv-table::
    :header: "Source", "Value"

    "``uniform sampler2D tex;``", "0"
    "``uniform sampler2D gtexture;``", "0"
    "``uniform sampler2D lightmap;``", "1"
    "``uniform sampler2D normals;``", ""
    "``uniform sampler2D specular;``", "3"
    "``uniform sampler2D shadow;``", "Is waterShadowEnabled true? Yes: 5, no: 4"
    "``uniform sampler2D watershadow;``", "4"
    "``uniform sampler2D shadowtex0;``", "4"
    "``uniform sampler2D shadowtex1;``", "5"
    "``uniform sampler2D gaux1;``", "7  <custom texture>"
    "``uniform sampler2D gaux2;``", "8  <custom texture>"
    "``uniform sampler2D gaux3;``", "9  <custom texture>"
    "``uniform sampler2D gaux4;``", "10 <custom texture>"
    "``uniform sampler2D colortex4;``", "7  <custom texture>"
    "``uniform sampler2D colortex5;``", "8  <custom texture>"
    "``uniform sampler2D colortex6;``", "9  <custom texture>"
    "``uniform sampler2D colortex7;``", "10 <custom texture>"
    "``uniform sampler2D colortex8;``", "16 <custom texture>"
    "``uniform sampler2D colortex9;``", "17 <custom texture>"
    "``uniform sampler2D colortex10;``", "18 <custom texture>"
    "``uniform sampler2D colortex11;``", "19 <custom texture>"
    "``uniform sampler2D colortex12;``", "20 <custom texture>"
    "``uniform sampler2D colortex13;``", "21 <custom texture>"
    "``uniform sampler2D colortex14;``", "22 <custom texture>"
    "``uniform sampler2D colortex15;``", "23 <custom texture>"
    "``uniform sampler2D shadowcolor;``", "13"
    "``uniform sampler2D shadowcolor0;``", "13"
    "``uniform sampler2D shadowcolor1;``", "14"
    "``uniform sampler2D noisetex;``", "15"


Composite and deferred uniforms
-------------------------------

.. note:: Programs: composite, composite1, composite2, composite3, composite4, composite5, composite6, composite7, final, deferred, deferred1, deferred2, deferred3, deferred4, deferred5, deferred6, deferred7

.. csv-table::
    :header: "Source", "Value"

    "``uniform sampler2D gcolor;``", "0"
    "``uniform sampler2D gdepth;``", "1"
    "``uniform sampler2D gnormal;``", "2"
    "``uniform sampler2D composite;``", "3"
    "``uniform sampler2D gaux1;``", "7"
    "``uniform sampler2D gaux2;``", "8"
    "``uniform sampler2D gaux3;``", "9"
    "``uniform sampler2D gaux4;``", "10"
    "``uniform sampler2D colortex0;``", "0"
    "``uniform sampler2D colortex1;``", "1"
    "``uniform sampler2D colortex2;``", "2"
    "``uniform sampler2D colortex3;``", "3"
    "``uniform sampler2D colortex4;``", "7"
    "``uniform sampler2D colortex5;``", "8"
    "``uniform sampler2D colortex6;``", "9"
    "``uniform sampler2D colortex7;``", "10"
    "``uniform sampler2D colortex8;``", "16 "
    "``uniform sampler2D colortex9;``", "17 "
    "``uniform sampler2D colortex10;``", "18"
    "``uniform sampler2D colortex11;``", "19"
    "``uniform sampler2D colortex12;``", "20"
    "``uniform sampler2D colortex13;``", "21"
    "``uniform sampler2D colortex14;``", "22"
    "``uniform sampler2D colortex15;``", "23"
    "``uniform sampler2D shadow;``", "Is waterShadowEnabled true? Yes: 5, no: 4"
    "``uniform sampler2D watershadow;``", "4"
    "``uniform sampler2D shadowtex0;``", "4"
    "``uniform sampler2D shadowtex1;``", "5"
    "``uniform sampler2D gdepthtex;``", "6"
    "``uniform sampler2D depthtex0;``", "6"
    "``uniform sampler2D depthtex1;``", "11"
    "``uniform sampler2D depthtex2;``", "12"
    "``uniform sampler2D shadowcolor;``", "13"
    "``uniform sampler2D shadowcolor0;``", "13"
    "``uniform sampler2D shadowcolor1;``", "14"
    "``uniform sampler2D noisetex;``", "15"

Custom uniforms
---------------

Define custom variables and uniforms using general mathematical expressions with brackets, constants, variables, operators and functions.
The uniforms are sent to the shaders, the variables can be used in other variables or uniforms.
The custom uniforms are updated on program change.

.. important::

    Lightmap texture matrix, 1.17+:

    .. code:: c

        const mat4 TEXTURE_MATRIX_2 = mat4(
            vec4(0.00390625, 0.0, 0.0, 0.0),
            vec4(0.0, 0.00390625, 0.0, 0.0),
            vec4(0.0, 0.0, 0.00390625, 0.0),
            vec4(0.03125, 0.03125, 0.03125, 1.0)
        );

.. csv-table:: Constants
  :header: "Name", "Type", "Meaning"

  "``pi``", "Float, constant", "Floating point, ``3.1415926``"
  "``true``", "Boolean, constant", "Truthy boolean"
  "``false``", "Boolean, constant", "False boolean"

The available biome ids, categories and precipitation types are defines as constants.
For example: ``BIOME_PLAINS``, ``BIOME_DESERT``, ``BIOME_EXTREME_HILLS``, etc.

.. csv-table:: Parameters (float)
    :header: "Constant", "Meaning"

    "biome", "biome id"
    "biome_category", "0 to 16 (CAT_NONE, CAT_TAIGA, CAT_EXTREME_HILLS, CAT_JUNGLE, CAT_MESA, CAT_PLAINS, CAT_SAVANNA, CAT_ICY, CAT_THE_END, CAT_BEACH, CAT_FOREST, CAT_OCEAN, CAT_DESERT, CAT_RIVER, CAT_SWAMP, CAT_MUSHROOM, CAT_NETHER)"
    "biome_precipitation", "0 to 2 (PPT_NONE, PPT_RAIN, PPT_SNOW)"
    "temperature", "0.0 to 1.0"
    "rainfall", "0.0 to 1.0 (humidity)"

Rain/snow is rendered for ``biome_precipitation != PPT_NONE``. If ``temperature >= 0.15``, rain is rendered, otherwise snow.

The fixed scalar uniforms are also available as parameters. For example: ``heldItemId``, ``worldTime``, ``moonPhase``, etc.

* **Vector** elements can be accessed with suffix ``.x``, ``.y``, and ``.z``. For example: ``sunPosition.y``
* **Color** elements can be accessed with suffix ``.r``, ``.g``, and ``.b``. For example: ``skyColor.r``
* **Matrix** elements can be accessed by row and column index. For example: ``gbufferModelView.0.1``

.. important::

    The dynamic uniforms ``entityColor``, ``entityId``, ``blockEntityId``, ``fogMode``, and ``fogColor`` can not be used as parameters as they may change many times per program.

.. seealso:: All other parameters, operators, functions, and more are in :ref:`cem_animation:values`.

.. csv-table::
    :header: "Name", "Return"
    :widths: 20, 40, 100

    "``smooth([id], val, [fadeInTime, [fadeOutTime]])``", "| Smooths a variable with custom fade-in time
    | The ``[id]`` must be unique; if not specified, it is generated automatically
    | Default fade time is 1 sec"

 Vector functions:

* ``vec2(x, y)``
* ``vec3(x, y, z)``
* ``vec4(x, y, z, w)``

Example
~~~~~~~

.. code:: properties

    variable.bool.isBiomeDark=in(biome, BIOME_RIVER, BIOME_FOREST)
    variable.float.valBiomeDark=smooth(1, if(isBiomeDark, 1, 0), 5)
    variable.float.valHurtDark=smooth(2, if(is_hurt, 1.3, 0), 0, 10)
    variable.float.valSwordDark=smooth(3, if(heldItemId == 276, 1, 0), 0.5, 0.5)
    uniform.float.screenDark=max(valBiomeDark, valHurtDark, valSwordDark)
    uniform.vec3.screenDark3=vec3(screenDark, heldItemId, biome)

    # More generally,
    uniform.<float|int|bool|vec2|vec3|vec4>.<name>=<expression>
    variable.<float|int|bool|vec2|vec3|vec4>.<name>=<expression>
