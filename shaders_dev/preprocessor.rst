Preprocessor directives
=======================

Compilation directives
----------------------

The shaders configuration parsing is affected by the preprocessor conditional compilation directives.
The following preprocessor directives are currently recognized:

.. code:: c

    #define <macro>
    #undef <macro>
    #ifdef <macro>
    #ifndef <macro>
    #if <int>
    #if defined <macro>
    #if !defined <macro>
    #elif <int>
    #elif defined <macro>
    #elif !defined <macro>
    #else
    #endif

The current shaderpack can be reloaded by pressing "``F3+R``" or using the command "``/reloadShaders``".

.. note:: The "``/reloadShaders``" command has no output, and is not sent to a server when run.

Properties
----------

.. note:: Because there are so many properties, some keys have their own section.
.. important:: The table does not list all the available keys, see the sections below, too.

This file can use conditional `compilation directives`_ *(#ifdef, #if, etc.)*
For more details see `Macros`_, A to I.
The settings version, ``oldLighting``, ``separateAo``, sliders, profiles, and screen are parsed without option macros.

.. figure:: /images/shaders_dev/settings.webp

    The shader settings screen, with every option visible.

.. csv-table::
    :header: "Key", "Values", "Meaning"

    "``clouds``", "fast|fancy|off", "Set clouds type, also controlled by ``Video Settings > Details > Clouds`` with higher priority"
    "``oldHandLight``", "Boolean", "| Enable or disable old hand light
    | When enabled: uses the handheld item with higher light value for the main hand
    | The old hand light is also controlled by ``Video Settings > Shaders > Old Hand Light`` with higher priority"
    "``dynamicHandLight``", "Boolean", "| Enable or disable the dynamic hand light from Dynamic Lights
    | This option can be used to disable the dynamic hand light from Dynamic Lights if the shader implements its own hand light."
    "``oldLighting``", "Boolean", "| Enable or disable old block lighting with fixed multiplier
    | The old block lighting is also controlled by ``Video Settings > Shaders > Old Lighting`` with higher priority"
    "``shadowTerrain``", "Boolean", "Enable or disable rendering of terrain *(solid, cutout, cutout_mipped)* in the shadow pass"
    "``shadowTranslucent``", "Boolean", "Enable or disable rendering of translucent blocks *(water, stained glass)* in the shadow pass"
    "``shadowEntities``", "Boolean", "Enable or disable rendering of entities in the shadow pass"
    "``shadowBlockEntities``", "Boolean", "Enable or disable rendering of block entities in the shadow pass"
    "``underwaterOverlay``", "Boolean", "Enable or disable underwater screen overlay"
    "``sun``", "Boolean", "Enable or disable sun rendering"
    "``moon``", "Boolean", "Enable or disable moon rendering"
    "``vignette``", "Boolean", "Enable or disable vignette rendering"
    "``backFace.solid``", "Boolean ", "| Enable back-face rendering per render layer, default is false
    | See :ref:`shaders_dev:block render layers`"
    "``backFace.cutout``", "Boolean", "See above"
    "``backFace.cutoutMipped``", "Boolean", "See above"
    "``backFace.translucent``", "Boolean", "See above"
    "``rain.depth``", "Boolean", "Enables rain and snow to write to the depth buffer"
    "``beacon.beam.depth``", "Boolean", "Enables beacon beam to write to the depth buffer"
    "``separateAo``", "Boolean", "When enabled the AO brightness *(smooth lighting)* is separated from ``color.rbg`` and put in ``color.a``."
    "``frustum.culling``", "Boolean", "Enable or disable `frustum culling <https://www.gamedev.net/tutorials/programming/general-and-gameplay-programming/frustum-culling-r4613/>`_"
    "``shadow.culling``", "Boolean", "Enable or disable shadow culling"
    "``version.<mc_ver>``", "<of_edition>", "| The minimum OptiFine version which is required by the shader pack
    | Each Minecraft version has to be specified separately.
    | For example: ``version.1.12.2=D1``, ``version.1.10.2=F1``, ``version.1.8=J1``"
    "``texture.noise``", "<path>", "Allows the noise texture to be loaded from the shader pack"
    "``sliders``", "<list of options>", "Options with multiple allowed values can be shown as sliders"
    "``alphaTest.<program>``", "<off|func ref>", "| The alpha test can be configured per program
    | Where:
    | * func is one of: ``NEVER``, ``LESS``, ``EQUAL``, ``LEQUAL``, ``GREATER``, ``NOTEQUAL``, ``GEQUAL``, ``GL_ALWAYS``
    | * ref: float value"
    "``blend.<program>``", "<off|src dst srcA dstA>", "| The blend mode can be configured per program.
    | Where ``src``, ``dst``, ``srcA``, and ``dstA`` are one of: ``ZERO``, ``ONE``, ``SRC_COLOR``, ``ONE_MINUS_SRC_COLOR``, ``DST_COLOR``, ``ONE_MINUS_DST_COLOR``, ``SRC_ALPHA``, ``ONE_MINUS_SRC_ALPHA``, ``DST_ALPHA``, ``ONE_MINUS_DST_ALPHA``, ``SRC_ALPHA_SATURATE``"
    "``blend.<program>.<buffer>``", "<off|src dst srcA dstA>", "| Blend mode per buffer
    | The blend mode can be configured per program and buffer
    | Where ``src``, ``dst``, ``srcA``, and ``dstA`` are one of: ``ZERO``, ``ONE``, ``SRC_COLOR``, ``ONE_MINUS_SRC_COLOR``, ``DST_COLOR``, ``ONE_MINUS_DST_COLOR``, ``SRC_ALPHA``, ``ONE_MINUS_SRC_ALPHA``, ``DST_ALPHA``, ``ONE_MINUS_DST_ALPHA``, ``SRC_ALPHA_SATURATE``"
    "``scale.<program>``", "<scale|scale offsetX ofsetY>", "| Composite render scale
    | Defines a custom viewport to be used when rendering composite and deferred programs.
    | The ``scale``, ``offsetX``, and ``offsetY`` should be between ``0.0`` and ``1.0``."
    "``flip.<program>.<buffer>``", "<true|false>", "| Ping-pong buffer flip
    | Enable or disable ping-pong buffer flip for a specific buffer name in a specific composite or deferred program.
    | When buffer flip is disabled the next composite program will use the same input and output buffers for this buffer name.
    | The last program that writes to the buffer should have flip enabled so that the following programs can read from the buffer.
    | This can be used with composite render scale to allow several composite programs to write to different regions in the same buffer.
    | Forced buffer flip can be used to read from both ping-pong buffers."
    "``size.buffer.<buffer>``", "width height", "| Buffer size
    | Define custom fixed size for a specific buffer.
    | Only ``prepare``, ``deferred``, and ``composite`` programs can render to fixed size buffers.
    | Rendering to fixed size and normal buffers at the same time is not possible.
    | When rendering to several fixed size buffers all of them must have the same size.
    | When width and height are floating point values, then the buffer size will be relative to the render size.
    | For example: ``size.buffer.colortex2=0.5 0.5`` will create the buffer with width and height that is half of the render width and height."
    "``program.<program>.enabled``", "<expression>", "| Enable or disable programs depending on shader options
    | Disabled progams are processed as not defined and instead their fallback programs will be used.
    | The program name can contain dimension folder, for example: ``program.world-1/composite2.enabled=BLOOM``
    | The expression is a boolean expression which can use shader options of type switch (on/off), for example: ``program.composite.enabled=BLOOM && !GODRAYS``"

Macros
------

The standard macros are automatically included after the ``#version`` declaration in every shader file.

A. Minecraft version
~~~~~~~~~~~~~~~~~~~~

.. code:: c

    #define MC_VERSION <value>

The value is in format 122 (major 1, minor 2, release 2)
For example: 1.9.4 -> 10904, 1.11.2 -> 11102, etc.

B. Maximum supported GL version
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: c

    #define MC_GL_VERSION <value>

The value is an integer, for example: ``210``: 2.1, ``320``: 3.2, ``450``: 4.5

C. Maximum supported GLSL version
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: c

    #define MC_GLSL_VERSION <value>

The value is an integer, for example: ``210``: 2.1, ``320``: 3.2, ``450``: 4.5

D. Operating system
~~~~~~~~~~~~~~~~~~~

One of the following:

.. code:: c

    #define MC_OS_WINDOWS
    #define MC_OS_MAC
    #define MC_OS_LINUX
    #define MC_OS_OTHER

E. Driver
~~~~~~~~~

One of the following:

.. code:: c

    #define MC_GL_VENDOR_AMD
    #define MC_GL_VENDOR_ATI
    #define MC_GL_VENDOR_INTEL
    #define MC_GL_VENDOR_MESA
    #define MC_GL_VENDOR_NVIDIA
    #define MC_GL_VENDOR_XORG
    #define MC_GL_VENDOR_OTHER

F. GPU
~~~~~~

One of the following:

.. code:: c

    #define MC_GL_RENDERER_RADEON
    #define MC_GL_RENDERER_GEFORCE
    #define MC_GL_RENDERER_QUADRO
    #define MC_GL_RENDERER_INTEL
    #define MC_GL_RENDERER_GALLIUM
    #define MC_GL_RENDERER_MESA
    #define MC_GL_RENDERER_OTHER

G. OpenGL extensions
~~~~~~~~~~~~~~~~~~~~

Macros for the supported OpenGL extensions are named like the corresponding extension with a prefix ``MC_``.
For example, the macro ``MC_GL_ARB_shader_texture_lod`` is defined when the extension ``GL_ARB_shader_texture_lod`` is supported.

Only the macros which are referenced and supported are added to the shader file.

H. Options
~~~~~~~~~~

.. code:: c

    #define MC_FXAA_LEVEL <value>             // When FXAA is enabled, values: 2, 4
    #define MC_NORMAL_MAP                     // When the normal map is enabled
    #define MC_SPECULAR_MAP                   // When the specular map is enabled
    #define MC_RENDER_QUALITY <value>         // Values: 0.5, 0.70710677, 1.0, 1.4142135, 2.0
    #define MC_SHADOW_QUALITY <value>         // Values: 0.5, 0.70710677, 1.0, 1.4142135, 2.0
    #define MC_HAND_DEPTH <value>             // Values: 0.0625, 0.125, 0.25
    #define MC_OLD_HAND_LIGHT                 // When Old Hand Light is enabled
    #define MC_OLD_LIGHTING                   // When Old Lighting is enabled
    #define MC_ANISOTROPIC_FILTERING <value>  // When anisotropic filtering is enabled

I. Textures
~~~~~~~~~~~

.. seealso:: See :doc:`/texture_properties`.

.. code:: c

    #define MC_TEXTURE_FORMAT_LAB_PBR       // Texture format LabPBR (https://github.com/rre36/lab-pbr/wiki)
    #define MC_TEXTURE_FORMAT_LAB_PBR_1_3   // Version 1.3

J. Render stages
~~~~~~~~~~~~~~~~

Constants for the uniform "``renderStage``".
The constants are given in the order in which the stages are executed.

.. code:: c

    #define MC_RENDER_STAGE_NONE <const>                    // Undefined
    #define MC_RENDER_STAGE_SKY <const>                     // Sky
    #define MC_RENDER_STAGE_SUNSET <const>                  // Sunset and sunrise overlay
    #define MC_RENDER_STAGE_CUSTOM_SKY <const>              // Custom sky
    #define MC_RENDER_STAGE_SUN <const>                     // Sun
    #define MC_RENDER_STAGE_MOON <const>                    // Moon
    #define MC_RENDER_STAGE_STARS <const>                   // Stars
    #define MC_RENDER_STAGE_VOID <const>                    // Void
    #define MC_RENDER_STAGE_TERRAIN_SOLID <const>           // Terrain solid
    #define MC_RENDER_STAGE_TERRAIN_CUTOUT_MIPPED <const>   // Terrain cutout mipped
    #define MC_RENDER_STAGE_TERRAIN_CUTOUT <const>          // Terrain cutout
    #define MC_RENDER_STAGE_ENTITIES <const>                // Entities
    #define MC_RENDER_STAGE_BLOCK_ENTITIES <const>          // Block entities
    #define MC_RENDER_STAGE_DESTROY <const>                 // Destroy overlay
    #define MC_RENDER_STAGE_OUTLINE <const>                 // Selection outline
    #define MC_RENDER_STAGE_DEBUG <const>                   // Debug renderers
    #define MC_RENDER_STAGE_HAND_SOLID <const>              // Solid handheld objects
    #define MC_RENDER_STAGE_TERRAIN_TRANSLUCENT <const>     // Terrain translucent
    #define MC_RENDER_STAGE_TRIPWIRE <const>                // Tripwire string
    #define MC_RENDER_STAGE_PARTICLES <const>               // Particles
    #define MC_RENDER_STAGE_CLOUDS <const>                  // Clouds
    #define MC_RENDER_STAGE_RAIN_SNOW <const>               // Rain and snow
    #define MC_RENDER_STAGE_WORLD_BORDER <const>            // World border
    #define MC_RENDER_STAGE_HAND_TRANSLUCENT <const>        // Translucent handheld objects
