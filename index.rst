:hide-toc:
:hide-footer:
:nosearch:
:og:image: /_static/favicon.webp

####
Home
####

===========================================
OptiDocs is the documentation for OptiFine.
===========================================

OptiDocs is a community-made project designed to document all of OptiFine.

OptiDocs contains detailed tables, pictures, tutorials, syntax specifications, and more.
It is free (as in `libre <https://en.wikipedia.org/wiki/Gratis_versus_libre>`_), `open-source <https://gitlab.com/whoatemybutter/optifinedocs>`_, and :doc:`open to contributions <contributing>`! The documentation here is in the **public domain** and may be used without attribution.

OptiDocs is current and does **not** contain specific notes or explanations for older versions, either of the base game or of the mod.

To get started, check out the left sidebar for a list of topics covered and their articles.

----

.. figure:: images/home/logo.webp
    :align: right
    :width: 128px
    :class: pixel-edges

**OptiFine is a Minecraft optimization mod.**

Here are some of OptiFine's features:

* **Performance improvements:** optimizations, FPS improvements, and efficient culling
* **Shaders:** wavy water, godrays, shadows, and clouds
* **Graphics:** mipmaps, anistropic filtering, and anti-aliasing
* **Customizability:** toggleable features, custom animations, and particles
* **Feature-rich:** adds many exclusive resource pack features
* **Support**: a vast community with a long-standing history

.. figure:: images/home/footer.webp
    :align: center
    :width: 1080px

Contents
========

.. toctree::
    :hidden:

    Home<self>

.. toctree::
    :caption: Information
    :maxdepth: 2

    capes
    cpm
    debug_keys
    easter_eggs
    faq
    install
    jvm_args
    license
    troubleshooting
    versioning

.. toctree::
    :caption: Features
    :maxdepth: 2

    better_grass
    better_snow
    block_render_layers
    cem
    cit
    colormaps
    ctm
    custom_animations
    custom_colors
    custom_guis
    custom_lightmaps
    custom_loading_screens
    custom_panoramas
    custom_sky
    dynamic_lights
    emissive_textures
    hd_fonts
    lagometer
    natural_textures
    quick_info
    random_entities
    shaders
    shaders_dev
    syntax
    texture_properties

.. toctree::
    :caption: Documentation
    :maxdepth: 2

    about
    changelog
    contributing
    json_schemas
    license
