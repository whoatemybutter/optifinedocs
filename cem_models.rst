:hero: The square block goes in the square hole

Models
######

.. figure:: images/cem/icon_models.webp
    :align: right
    :height: 180px

    A 3D model in `Blockbench <https://www.blockbench.net/>`_.

.. location::

    /assets/minecraft/optifine/cem/\*\*/\*.jem

CEM model files contain the definition of a whole entity model. It is written in JSON.

They may be located anywhere inside the ``cem`` folder.
The name of the file must match one of the entity names in :doc:`cem_entity_names`,
or be in the folder ``/assets/minecraft/optifine/cem/<entity name>``.

Keys
====

``texture``
-----------

.. values::
    :values: |file path|
    :required: No

Texture used by entity model.

``textureSize``
---------------

.. values::
    :values: Array of 2 integers
    :required: No

Texture size in pixels; ``[width, height]``.

``shadowSize``
--------------

.. values::
    :values: Decimal
    :required: No

Shadow size as a scale, from ``0.0`` to ``1.0``.

``models``
----------

.. values::
    :values: Space-split list of objects
    :required: Yes

Array of model objects that make up the entity's full model.

``baseId``
~~~~~~~~~~

.. values::
    :values: String
    :required: No

Model parent ID. If specified, all parent properties are inherited and do not need to be explicitly put.

``model``
~~~~~~~~~

.. values::
    :values: |file path|
    :required: No

Path to a :doc:`part model file <cem_parts>` (``.jpm``) from which to load the part model definition.

If this is not specified, the items in a JPM can be specified inline to this object, the parent of "model".

``id``
~~~~~~

.. values::
    :values: String
    :required: No

Model ID, can be used to reference the model as parent.

``part``
~~~~~~~~

.. values::
    :values: String
    :required: Yes

Entity part to which the part model is attached.

See :doc:`cem_entity_names` for a list of part names for each entity.

.. important:: Make sure that the part names correspond with your model's applicable entity.

``attach``
~~~~~~~~~~

.. values::
    :values: Boolean
    :required: No

How to handle replacing overlapping parts.

* True: Attach (add) to the entity part.
* False: Replace the entity part.

``scale``
~~~~~~~~~

.. values::
    :values: Float
    :required: No
    :default: ``1.0``

Render scale.
``0.0`` makes it "invisible".

Part model definitions
~~~~~~~~~~~~~~~~~~~~~~

All of the items in a :doc:`CEM parts file <cem_parts>` can be put here instead, if ``model`` is absent.

``animations``
~~~~~~~~~~~~~~

.. values::
    :values: List of objects
    :required: No
    :default: ``[]``

Refer to :doc:`CEM animation <cem_animation>` for what to place in each object in this list.

Randomized models
=================

The alternative models use the same name as the main model with a number suffix.

For example:

* wolf.jem - main model (index 1)
* wolf2.jem - alternative model (index 2)
* wolf3.jem - alternative model (index 3)

The alternative models are selected randomly based on the entity ID.

To customize the use of the alternative models, add a ``<model_name>.properties`` file in the folder where the models are located.

The properties file works identically to the properties file used by :doc:`random_entities`.
The models to be used are selected with the setting ``models.<n>=<list>`` instead of ``textures.<n>=<list>``.
The index of the current matching rule is available as the :doc:`animation<cem_animation>` parameter ``rule_index``, and can be used to cutomize the model depending on entity properties.

For more details, see :doc:`random_entities`.

Examples
--------

.. code-block:: properties
    :caption: creeper.properties, creeper.jem, creeper2.jem


    models.1=2
    name.1=James


.. code-block:: properties
    :caption: boat.properties, boat.jem, boat2.jem, boat3.jem

    models.1=2
    nbt.1.Type=spruce

    models.2=3
    nbt.2.Type=birch

.. code-block:: properties
    :caption: bed.properties, bed.jem, bed2.jem, bed3.jem

    models.1=2
    models.1=2
    name.1=James
    blocks.1=black_bed

    models.2=3
    blocks.2=orange_bed

JSON schema
===========

.. literalinclude:: schemas/cem_model.schema.json
