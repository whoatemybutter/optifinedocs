:hero: It's in the air

Better Snow
###########

.. figure:: images/better_snow/icon.webp
    :align: right
    :height: 180px

    Fence with snow "inside" it.

**Better Snow** shows a snow layer beneath specific blocks that have a `snow layer <https://minecraft.wiki/w/Snow>`__ or `snow block <https://minecraft.wiki/w/Snow_Block>`__ near them.

.. figure:: images/better_snow/settings.webp

    :menuselection:`Video Settings --> Quality`

Block list
==========

- Glass panes
- Tall grass
- Grass
- Ferns
- Flowers
- Fences
- Fence gates
- Hoppers
- Saplings
- Iron bars
- Walls

.. figure:: images/better_snow/selection.webp

    A grass platform with snow layers on one row and various blocks on the other.
    For example, the glass pane has no snow below it, but since there is a snow layer next to it, it shows as having a snow layer inside it.
