Install With Forge
##################

.. note:: This tutorial does not explain *how* to install Forge.

1. Download and install `Minecraft Forge <https://files.minecraftforge.net/net/minecraftforge/forge/>`_ for the version you want
2. Follow the instructions at :doc:`download`.
3. Open your Downloads folder in a file manager.
4. From here on, process depends on your OS:

.. note:: These steps are for the Vanilla Launcher. For MultiMC, find the mods folder for your specific Forge instance.

.. md-tab-set::

    .. md-tab-item:: :si-icon:`fontawesome/brands/windows` Windows

        1. Press :keys:`Windows + R`, and inside of the Run window, type ``%AppData%\.minecraft\``.

    .. md-tab-item:: :si-icon:`fontawesome/brands/apple` Mac

        1. | Open the Spotlight search by either:
           | Clicking the Spotlight icon in the menu bar.
           | Pressing :keys:`Command + Space`.
        2. In the Spotlight window, type ``~/library/Application Support/minecraft/``.

    .. md-tab-item:: :si-icon:`fontawesome/brands/linux` Linux

        1. Open a file manager.
        2. Change the path to ``~/.minecraft/``.

5. If there is no "mods" folder in ``.minecraft``, create one *(all lowercase; case-sensitive)*.
6. Drag the OptiFine jar from the Downloads folder into the "mods" folder.
7. Launch the game with Forge.

.. important::
    If you are intending to use OptiFine with Forge, **ensure** that OptiFine version is compatible with your Forge version.

    .. figure:: images/install/of_downloads_forge.webp

        Ensure this version is at least your Forge version!
