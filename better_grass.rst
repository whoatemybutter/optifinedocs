:hero: Grassier greener grass

Better Grass
############

.. figure:: images/better_grass/grass.webp
    :align: right
    :height: 180px

    Grass on grass.

.. location::

    /assets/minecraft/optifine/bettergrass.properties

**Better Grass** shows the side grass texture on grass blocks that are near other grass blocks.

When a Grass Block at the bottom is directly adjacent (north, east, south, or west)
to a Grass Block above, the side texture of the top block will be replaced with the top-face Grass Block texture.

.. figure:: images/better_grass/settings.webp

    :menuselection:`Video Settings --> Quality`

.. figure:: images/better_grass/selection.webp

    A showcase of various grasses and dirts.

Properties
==========

``grass``
---------

.. image:: images/better_grass/grass.webp
    :align: right
    :width: 120px

.. values::
    :values: Boolean
    :required: No
    :default: ``true``

Enables Better Grass for Grass Blocks.

``dirt_path``
-------------

.. image:: images/better_grass/path.webp
    :align: right
    :width: 120px

.. values::
    :values: Boolean
    :required: No
    :default: ``true``

Enables Better Grass for `grass path blocks <https://minecraft.wiki/w/Grass_Path>`_.

``mycelium``
------------

.. image:: images/better_grass/mycelium.webp
    :align: right
    :width: 120px

.. values::
    :values: Boolean
    :required: No
    :default: ``true``

Enables Better Grass for Mycelium.

``podzol``
----------

.. image:: images/better_grass/podzol.webp
    :align: right
    :width: 120px

.. values::
    :values: Boolean
    :required: No
    :default: ``true``

Enables Better Grass for Podzol.

``crimson_nylium``
------------------

.. image:: images/better_grass/crimson_nylium.webp
    :align: right
    :width: 120px

.. values::
    :values: Boolean
    :required: No
    :default: ``true``

Enables Better Grass for `Crimson Nylium <https://minecraft.wiki/w/Nylium>`__.

``warped_nylium``
-----------------

.. image:: images/better_grass/warped_nylium.webp
    :align: right
    :width: 120px

.. values::
    :values: Boolean
    :required: No
    :default: ``true``

Enables Better Grass for `Warped Nylium <https://minecraft.wiki/w/Nylium>`__.

``grass.snow``
--------------

.. image:: images/better_grass/grass_snow.webp
    :align: right
    :width: 120px

.. values::
    :values: Boolean
    :required: No
    :default: ``true``

Enables Better Grass for Grass Blocks that have a
`snow layer <https://minecraft.wiki/w/Snow>`__ or
`snow block <https://minecraft.wiki/w/Snow_Block>`__ on top of them.

``mycelium.snow``
-----------------

.. image:: images/better_grass/mycelium_snow.webp
    :align: right
    :width: 120px

.. values::
    :values: Boolean
    :required: No
    :default: ``true``

Enables Better Grass for Mycelium that has a
`snow layer <https://minecraft.wiki/w/Snow>`__ or
`snow block <https://minecraft.wiki/w/Snow_Block>`__ on top of them.

``podzol.snow``
---------------

.. image:: images/better_grass/podzol_snow.webp
    :align: right
    :width: 120px

.. values::
    :values: Boolean
    :required: No
    :default: ``true``

Enables Better Grass for Podzol that have a
`snow layer <https://minecraft.wiki/w/Snow>`__ or
`snow block <https://minecraft.wiki/w/Snow_Block>`__ on top of them.

``grass.multilayer``
--------------------

.. values::
    :values: Boolean
    :required: No
    :default: ``false``

Allows a transparent grass texture to be used as an overlay for the
grass block's side. If enabled, a transparent grass texture can overlay it.

If enabled:

-  Layer 1: ``grass_side``
-  Layer 2: ``grass`` *(colored by biome)*

``texture.grass``
-----------------

.. values::
    :values: |file path|
    :required: No
    :default: ``block/grass_block_top``

What texture to use for the **top** of a grass block that has Better Grass applied to it.

.. note:: This texture will be tinted (colored) by biome.

``texture.grass_side``
----------------------

.. values::
    :values: |file path|
    :required: No
    :default: ``block/grass_block_side``

What texture to use for the **side** of a grass block that has Better Grass applied to it.

``texture.dirt_path``
----------------------

.. values::
    :values: |file path|
    :required: No
    :default: ``block/dirt_path_top``

What texture to use for the **top** of a dirt path block that has Better Grass applied to it.

``texture.dirt_path_side``
--------------------------

.. values::
    :values: |file path|
    :required: No
    :default: ``block/dirt_path_side``

What texture to use for the **side** of a dirt path block that has Better Grass applied to it.

``texture.mycelium``
--------------------

.. values::
    :values: |file path|
    :required: No
    :default: ``block/mycelium_top``

What texture to use for the **top** of a Mycelium block that has Better Grass applied to it.

``texture.podzol``
------------------

.. values::
    :values: |file path|
    :required: No
    :default: ``block/podzol_top``

What texture to use for the **top** of a Podzol block that has Better Grass applied to it.

``texture.crimson_nylium``
--------------------------

.. values::
    :values: |file path|
    :required: No
    :default: ``block/crimson_nylium``

What texture to use for the **top** of a `Crimson Nylium <https://minecraft.wiki/w/Nylium>`__
block that has Better Grass applied to it.

``texture.warped_nylium``
-------------------------

.. values::
    :values: |file path|
    :required: No
    :default: ``block/warped_nylium``

What texture to use for the **top** of a `Warped Nylium <https://minecraft.wiki/w/Nylium>`__
block that has Better Grass applied to it.

``texture.snow``
----------------

.. values::
    :values: |file path|
    :required: No
    :default: ``block/snow``

What texture to use for the **top** of a `Snow block <https://minecraft.wiki/w/Snow_block>`_
that has Better Grass applied to it.

Examples
========

Only Grass
----------

.. code:: ini

    mycelium=false
    podzol=false

Not Grass
---------

.. code:: ini

    grass=false
    dirt_path=false
    crimson_nylium=false
    warped_nylium=false

Texture for grass sides
-----------------------

.. code:: ini

    texture.grass_side=block/redstone_block
    texture.grass=block/emerald_block

Disable snowy blocks
--------------------

.. code:: ini

    grass.snow=false
    mycelium.snow=false
    podzol.snow=false


JSON schema
===========

.. note:: Although this page is ``.properties`` based, it can be mapped to JSON.

.. literalinclude:: ./schemas/better_grass.schema.json
