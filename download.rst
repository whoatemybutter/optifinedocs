:hero: For the low, low price of $0.00

Downloading
###########

To download OptiFine, first determine what version you will want to download OptiFine for.

Visit `<https://optifine.net/downloads>`_, and find the heading that has your version.

.. note::
    The latest minecraft version will always be displayed.
    For older versions, click "Show all versions" at the bottom of the page.

.. image:: images/install/of_downloads.webp
    :align: center

Only download the most recent OptiFine version for your Minecraft version.
Click the large blue "Download" button, or the "(mirror)" link.

If you want an older OptiFine version, click the "+ More" link under the latest OptiFine version for that Minecraft version.
If you want a preview version of OptiFine, click the "+ Preview versions" link.

.. important::

    If you are intending to use OptiFine with Forge, **ensure** that OptiFine version is compatible with your Forge version.

    .. figure:: images/install/of_downloads_forge.webp

        Ensure this version is at least your Forge version!


You should now be downloading a ``.jar`` file.
