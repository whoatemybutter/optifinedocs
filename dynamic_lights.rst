:hero: Glow in the dark dinosaurs

Dynamic Lights
##############

.. figure:: images/dynamic_lights/icon.webp
    :align: right
    :height: 180px

    A dropped torch item.

.. location::

    /assets/minecraft/optifine/dynamic_lights.properties

    /assets/<MOD>/optifine/dynamic_lights.properties

**Dynamic Lights** allows hand-held and dropped light-emitting items, such as torches, to illuminate the blocks around them in the world.

.. figure:: images/dynamic_lights/settings.webp

    :menuselection:`Video Settings --> Quality`

.. figure:: images/dynamic_lights/cave.webp

    An example of dynamic lighting; the held-item torch illuminates the blocks around the player, and the dropped glowstone item also does the same.

This configuration file allows **mods** to define dynamic light levels for entities and items.

Properties
==========

``entities``
------------

.. values::
    :values: String: ``entity:light_level``
    :required: No

Entity light levels.
The entity name is automatically expanded with the mod's ID, if applicable.

The light level should be between ``0`` and ``15``.
For example: ``entities=basalz:15 blitz:7``.

.. important:: This does not work for ``minecraft:`` entities.

``items``
---------

.. values::
    :values: String: ``item:light_level``
    :required: No

Item light levels.
The item name is automatically expanded with the mod's ID, if applicable.

The light level should be between ``0`` and ``15``.
For example: ``items=florb:15 morb:7``.

.. important:: This does not work for ``minecraft:`` items.

JSON schema
===========

.. note:: Although this page is ``.properties`` based, it can be mapped to JSON.

.. literalinclude:: ./schemas/dynamic_lights.schema.json
