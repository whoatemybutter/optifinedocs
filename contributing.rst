:hero: There's no I in team

Contributing
############

OptiDocs always welcomes contributions and changes.
Whether you want to add a new page, add more to a section, or change an image, your contributions are welcome.

This guide outlines how to contribute - no matter if you're a beginner or git god.

Basic topics
============

Before contributing, there's a few topics and systems you should know about.

rST
---

OptiDocs' source documents are written in `reStructuredText <https://en.wikipedia.org/wiki/ReStructuredText>`__ (rST).
rST is a markup system originally used in Python but has since been expanded to general use.
In order to write or contribute to OptiDocs, you **must** be familiar with rST.
rST is not `Markdown <https://www.markdownguide.org/>`__, and Markdown is not rST.

You should start `here <https://docutils.sourceforge.io/docs/user/rst/quickstart.html>`__;
only use quick "cheatsheets" once you are comfortable with the syntax and caveats.

Sphinx
------

OptiDocs uses `Sphinx <https://www.sphinx-doc.org/en/master/>`__ to generate the documentation from the rST.
Sphinx is widely used, from the `Linux kernel <https://docs.kernel.org/>`__ to small project documentation.

You do not need to be highly knowledgeable about Sphinx, but it is important to be aware of what it is and what it offers.
**Sphinx is not rST**. Sphinx takes in rST documents to generate cohesive and interconnected documentation.

`Sphinx-Immaterial <https://pypi.org/project/sphinx-immaterial/>`__ is the theme used for OptiDocs.
This theme includes some unique features that OptiDocs takes advantage of, so you should learn the theme-specific options.
Specifically, the ``.. md-tab-set`` and custom checkbox styles are used often.

git
---

OptiDocs uses `git <https://en.wikipedia.org/wiki/Git>`__ as the version control system.
You **must** be familiar with how git works at a basic level.
You should be familiar with how merge requests (also called *pull requests*) work.


GitLab
------

OptiDocs' source is on GitLab, a source code hosting website.
GitLab is not the same as GitHub, and GitLab is not git.

You will need a GitLab account to contribute; creating one is easy.
For help using GitLab, see `<https://docs.gitlab.com/>`__.

Repository overview
===================

The source for OptiDocs is at `<https://gitlab.com/whoatemybutter/optifinedocs>`__.

OptiDocs uses 2 'core' branches:

* ``master``: Production-ready, stable; ReadTheDocs builds this for the main site.
* ``dev``: Staging area; periodically merged into ``master`` for main site updates.

You should **always** upstream and merge request to ``dev``.
You cannot make a merge request into master.

The ``dev`` branch is the default branch, meaning merge requests are made against it by default.

Aside from ``.rst`` files, there are a number of files with a special purpose:

* ``.readthedocs.yaml`` configures ReadTheDocs integration. **You should never modify this.**
* ``Makefile`` allows easy ``make clean``, ``make html``, etc. Linux commands. **You should never modify this.**
* ``.gitignore`` prevents adding files that should not be tracked.
* ``conf.py`` tells Sphinx how to build the documentation.
* ``requirements.txt`` tells Python what dependencies are needed.
* ``_static/`` contains media that Sphinx explicitly includes automatically.
* ``_templates/`` contains Jinja templates for adding HTML meta tags.
* ``extensions/`` contains custom Sphinx extensions.
* ``images/`` contains all images, in WebP format.
* ``include/`` contains miscellanous JSON.
* ``schemas/`` contains JSON schemas.

Creating issues
===============

You can create an issue about anything you want, from a typo to an overhaul of a section of pages.
Read about issues and how to create them `here <https://docs.gitlab.com/ee/user/project/issues/>`__.
You can also label your issues appropriately so they can be better organized and worked on.

If you have a merge request, referencing the issue with ``#<issue number>`` will link it on the issue's page.

Creating merge requests
=======================

The **only way** to get your own changes included into OptiDocs is to start a merge request.
A merge request begins with a branch to merge.

1. Clone the repository
    Cloning the repository gives you a complete copy of the OptiDocs source.

    .. code:: shell

        git clone https://gitlab.com/whoatemybutter/optidocs.git
2. Create a new branch
    Depending on what you want to do, you can simply just work on ``dev`` directly.
    However, you may want to checkout a separate branch off of ``dev``, and then merge *that* in.

    .. code:: shell

        git checkout -b your-branch-name-here dev
3. Make changes
    Make your changes, then add it to your index.

    .. code:: shell

        git add FILESYOUCHANGED FILE2 FILE3
4. Run Sphinx locally
    Before committing, you should test your changes to make sure that they both compile and look right.

    .. code:: shell

        python -m pip install -r requirements.txt
        sphinx-build . _build

    To re-build, delete ``_build`` *first*.

    .. code:: shell

        rm -R _build

5. Commit, push to your remote
    Committing records the changes in your index to the repository.
    If you have a signing key set, use ``-S`` also.

    .. code:: shell

        git commit -m "Commit message"

    .. important:: GitLab does not support signed push. When pushing with a signing key, use ``--no-signed``.
6. Submit a merge request from your remote to OptiDocs' ``dev`` branch
    Your "remote" is your online version of the repository.
    You can choose to merge from any branch of your repositories.

    It is up to you if you want to delete the source branch.
    Do not enable squashing.
7. Review and wait
    Wait for your merge request to be reviewed.
    You may get additional feedback about your merge request before it would be merged.
    Be sure to check back and pay attention to your inbox - GitLab sends e-mails about activity.

Merge requests, when merged, uses **merge commits**.
You should *not* squash your commits.

Merge requests are automatically built by ReadTheDocs and have their own dedicated link;
this is done in the external pipeline.

Licensing
=========

.. figure:: images/about/cc0.svg
    :align: right
    :width: 250px

**Remember**, OptiDocs is **public domain**. Therefore, other contributors must *also* adhere to this.
This means that for any changes you create and submit upstream (as in a merge request) to OptiDocs,
you agree to **relinquish them to the public domain**.

More specifically, you license them under `CC0 <https://creativecommons.org/share-your-work/public-domain/cc0/>`_.
This places your changes into the public domain, worldwide.
This *also* means that any work you do, you assert that it is wholly your own work (or that it is *also* in the public domain).

**By submitting a merge request, you agree to this.**

.. important::

    If you **do not** license under CC0, you unfortunately cannot contribute.
    This is because we cannot license *your* work in order to keep the whole of OptiDocs under the public domain.
