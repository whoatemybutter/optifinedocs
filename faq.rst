:hero: Would you, could you

FAQ
###

This document is a collection of the most frequently asked questions by users and their answers.

Do I need to configure anything to get results?
===============================================

**No**. OptiFine's default options are sufficient for most use cases.
You can configure different options and find the trade-off between quality and performance that works best for you.

Can I bundle OptiFine?
======================

**No**, unless you have explicit written permission from sp614x.

Where is the output log?
========================

.. figure:: images/faq/output_log.webp
    :align: right

Depending on your launcher and operating system, the location of the output log file will vary.

If you are using the default Minecraft launcher, go to the ``General`` tab and tick the ``Open the output log when Minecraft: Java Edition starts`` button.

For more specific and in-depth instructions, see `this page <https://help.ggservers.com/en-us/article/where-to-find-client-side-logs-7upje9/>`_ (not associated with OptiDocs; third-party).

Why do I get a warning message when launching?
==============================================

.. figure:: images/faq/mod_warning.webp
    :align: right

**This warning can be safely ignored.**
It shows when launching *any* modded instance of the game. It **does not** mean your installation is malicious, corrupt, or otherwise wrong.

I can't find OptiFine in my launcher
====================================

.. figure:: images/faq/modded_ticked.webp
    :align: right

    A red arrow points to the "Modded" checkbox.

Mojang updated the Minecraft launcher and added a toggle to show or hide modded versions of the game.
Go to the "Installations" tab and make sure the Modded box is checked.

Why isn't OptiFine open-source?
===============================

**In summary**, legal and technical complexities prevent OptiFine from becoming open-source.

The core of OptiFine involves extensive reorganizations of Minecraft's rendering code, making it impractical to publish the full source code due to potential violations of Minecraft's EULA.
While it is technically possible to extract changes as non-violating patches, this poses challenges.
OptiFine relies on a custom version of Mod Coder Pack (MCP), and the non-standard MCP scripts cannot be distributed due to licensing restrictions.
Even if patches were released, collaboration would be restricted, undermining the purpose of going open-source.
Additionally, changing the development process to accommodate patches would be a non-trivial task.

What happens if development stops?
==================================

**OptiFine will not die.**

Java programs are not difficult to decompile.

In the event that sp614x, the developer, is no longer working,
virtually anyone with the right knowledge could decompile OptiFine in its entirety,
compare it to decompiled vanilla Minecraft code, and extract the patches.

Alternatively, if sp614x ever decides to quit, he is willing to publish OptiFine's patches to GitHub.
There are no plans to stop OptiFine's development.

Either way, OptiFine can live again.

Is there OptiFine for Bedrock?
==============================

.. danger:: Anything claiming to be OptiFine for Bedrock is completely unofficial and most likely a scam or malware.

**No.**

The Bedrock Edition of Minecraft is not at all supported by OptiFine.
It uses an entirely different engine and bears little resemblance to Java Edition.

What does the FPS counter mean?
===============================

**Average FPS/minimum FPS.**

The minimum is the lowest FPS recorded recently.

How do I change my cape?
========================

You can login to the cape change page and select a new design.
The cape change page has a timeout of 2 minutes, and the cape has to be changed during this time.

After paying, how long until I see my cape?
===========================================

The cape is activated automatically when the donation payment is complete.
Please note that some payment types (bank transfer, eCheck, etc.) may take several days to complete.
A notification email is sent to the donation email address when the cape is activated.

How do I know if my cape is on?
===============================

You can login to the cape change page. It will show the current cape design or a notification if the cape is not active.
The cape change page can automatically correct upper- and lower-case errors in the username.

I did not get a confirmation email, and I do not see the cape
=============================================================

Most probably, your donation was received without a username. You can login on the donator login page and assign a username for the cape.

I got a confirmation email, but I do not see the cape
=====================================================

**Please ensure:**

* The username is correctly spelled; **upper- and lower-case matters**.
* OptiFine is correctly installed (:doc:`installation instructions <install>`).
* The option :menuselection:`Video Settings --> Details --> Show Capes` is **ON**.
* The option :menuselection:`Skin Customization --> Cape` is **ON**.
* No firewall or router is blocking OptiFine from accessing the OptiFine server where the cape is located.
* The cape is visible in single-player worlds. Some servers may modify the player's identity and thus prevent correct cape fetching.

I gave the wrong username when donating
=======================================

If the username capitalization is not correct, then login on the cape change page, and the capitalization should be automatically fixed.

If the username is wholly incorrect, you can login on the donator login page, where you can assign a new username.

Can I temporarily deactivate my cape?
=====================================

**Yes.**

You can deactivate and reactivate the cape on the cape change page with its checkbox.

Can I move my cape to another account?
======================================

**Yes.**

You can move the cape to another username on the cape change page.
After the cape is moved to a new username, it can **only be modified from the new account!**

If you move the cape to the wrong username, you can recover it on the donator login page.

My cape is now missing. Can I recover it?
=========================================

**Yes.**

The cape was most probably moved by someone who knows your Minecraft login credentials.
You can recover the cape on the donator login page, where you can assign a new username for the cape.

You should change your login and secure your account to avoid the cape being moved again.

I can't access the cape servers
===============================

There are many reasons why you can't access the cape server (``s.optifine.net``):

1. An antivirus is blocking the cape servers.
2. Your ISP is blocking the cape servers.
3. You have an application that is intentionally blocking the cape servers *(such as Mantle)*.

In some cases, you may have to manually change your network settings. Here are the instructions:

1. Open Notepad with Administrator. Search "Notepad" in your Windows Search Bar, right-click on it, and press :guilabel:`Run as Administrator`; accept the prompt.
2. Press :menuselection:`File --> Open`.
3. In the bottom right corner of the file window, change ``Text Documents (.txt)`` to ``All Files (.*)``.
4. Navigate to ``C:\Windows\System32\drivers\etc``.
5. Open the ``hosts`` file.
6. Delete the **entire line** that contains ``s.optifine.net`` or **anything** that contains ``mojang``.
7. Press :keys:`Control + S` to save.
8. Close Notepad and restart your computer.

.. raw:: html

    <video width=100% controls muted playsinline>
        <source src="_static/cape_notepad.mp4" type="video/mp4">
    Your browser does not support the video tag.
    </video>

I can't use the Mojang pattern for my cape
==========================================

The Mojang pattern (referred to in the game as "Thing") is specifically disallowed to prevent the impersonation of Mojang employees.

Remove it from your cape design.

.. figure:: images/faq/thing_pattern.webp

    The culprit; the resemblance is uncanny.

Why do I get the "Invalid cape design" error?
=============================================

**Your design has more than 8 layers**.

There are a maximum of 8 patterns, **not** including the base color.

Why are there no fully custom capes?
====================================

There are a couple of reasons why this isn't allowed:

1. Impersonation of official capes
2. Moderation of inappropriate images
3. Those who have custom capes are for very specific and unique reasons.

Custom capes will no longer be given for whatever reason.

My OptiFine cape not show on PvP clients
========================================

Some PvP clients block OptiFine capes from showing to promote buying their own cosmetics.

In general, OptiFine doesn't provide support for PvP clients, as most of them illegally redistribute OptiFine without permission.

OptiFine highly recommends using either standalone OptiFine or Forge with individual mods.

I moved my cape, but I don't see it
===================================

**All OptiFine capes are disabled when moved.**

This is to prevent abuse, such as buying a cape for a celebrity and using an offensive pattern.

The cape can be activated again by the new cape owner by opening the game and navigating to :menuselection:`Options --> Skin Customization --> OptiFine Cape --> Open Cape Editor`.

Do I have to update my cape if I change my username?
====================================================

**No.**

OptiFine will update your username automatically, but it may take up to 24 hours before this change goes into effect.

Can I change my donator e-mail?
===============================

**No.**

This functionality is not available currently.
