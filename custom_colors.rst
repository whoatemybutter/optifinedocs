:hero: Orange is the new black

Custom Colors
#############

.. figure:: images/custom_colors/icon.webp
    :align: right
    :height: 180px
    :class: pixel-edges

    Different colored potions.

.. location::

    /assets/minecraft/optifine/color.properties

**Custom Colors** can modify the hardcoded colors for various particles, fogs, and miscellanea.

Values only need to be provided for the properties that need to be changed.

.. figure:: images/custom_colors/settings.webp

    :menuselection:`Video Settings --> Quality`

The default Minecraft values for each property are given below *for convenience*.

Potions
=======

For potions with more than 1 effect,
the final color is the average of the applicable colors, weighted by the level of each potion effect.

.. note:: ``potion.water`` is a plain bottle of water

.. csv-table::
    :header: "Key", "Default"

    "potion.absorption", ".. color-box:: #2552a5"
    "potion.bad_omen", ".. color-box:: #0b6138"
    "potion.blindness", ".. color-box:: #1f1f23"
    "potion.conduit_power", ".. color-box:: #1dc2d1"
    "potion.darkness", ".. color-box:: #292721"
    "potion.dolphins_grace", ".. color-box:: #88a3be"
    "potion.fire_resistance", ".. color-box:: #ff9900"
    "potion.glowing", ".. color-box:: #94a061"
    "potion.haste", ".. color-box:: #d9c043"
    "potion.health_boost", ".. color-box:: #f87d23"
    "potion.hero_of_the_village", ".. color-box:: #44ff44"
    "potion.hunger", ".. color-box:: #587653"
    "potion.infested", ".. color-box:: #8c9b8c"
    "potion.instant_damage", ".. color-box:: #a9656a"
    "potion.instant_health", ".. color-box:: #f82423"
    "potion.invisibility", ".. color-box:: #f6f6f6"
    "potion.jump_boost", ".. color-box:: #fdff84"
    "potion.levitation", ".. color-box:: #ceffff"
    "potion.luck", ".. color-box:: #59c106"
    "potion.mining_fatigue", ".. color-box:: #4a4217"
    "potion.nausea", ".. color-box:: #551d4a"
    "potion.night_vision", ".. color-box:: #c2ff66"
    "potion.oozing", ".. color-box:: #99ffa3"
    "potion.poison", ".. color-box:: #87a363"
    "potion.raid_omen", ".. color-box:: #de4058"
    "potion.regeneration", ".. color-box:: #cd5cab"
    "potion.resistance", ".. color-box:: #9146f0"
    "potion.saturation", ".. color-box:: #f82423"
    "potion.slowness", ".. color-box:: #8bafe0"
    "potion.slow_falling", ".. color-box:: #f3cfb9"
    "potion.speed", ".. color-box:: #33ebff"
    "potion.strength", ".. color-box:: #ffc700"
    "potion.trial_omen", ".. color-box:: #16a6a6"
    "potion.unluck", ".. color-box:: #c0a44d"
    "potion.water_breathing", ".. color-box:: #98dac0"
    "potion.weakness", ".. color-box:: #484d48"
    "potion.weaving", ".. color-box:: #78695a"
    "potion.wind_charged", ".. color-box:: #bdc9ff"
    "potion.wither", ".. color-box:: #736156"
    "potion.water", ".. color-box:: #385dc6"

Spawner eggs
============

.. figure:: images/custom_colors/spawnegg_label.webp
    :width: 300px
    :class: pixel~edges

    Red are the shell, blue are the spots.

.. csv-table::
    :header: "Key", "Default", "Notes"

    "egg.shell.<entity>", "*None*", "Shell color"
    "egg.spots.<entity>", "*None*", "Spots color"

``<entity>`` controls what spawn egg the colors apply to
Colons must be escaped: ``egg.spots.minecraft\:creeper=000000``.

Map pixels
==========

.. csv-table::
    :header: "Key", "Default", "Notes"

    "map.air", ".. color-box:: #000000", "Air and unrendered and void"
    "map.grass", ".. color-box:: #7fb238", ""
    "map.sand", ".. color-box:: #f7e9a3", "Yellow sand"
    "map.cloth", ".. color-box:: #c7c7c7", "Wool"
    "map.tnt", ".. color-box:: #ff0000", ""
    "map.ice", ".. color-box:: #a0a0ff", "Ice and packed ice and blue ice"
    "map.iron", ".. color-box:: #a7a7a7", ""
    "map.foliage", ".. color-box:: #007c00", "Grass and tall grass and any flower and ferns"
    "map.clay", ".. color-box:: #a4a8b8", ""
    "map.dirt", ".. color-box:: #976d4d", "Dirt and coarse dirt and rooted dirt"
    "map.stone", ".. color-box:: #707070", ""
    "map.water", ".. color-box:: #4040ff", ""
    "map.wood", ".. color-box:: #8f7748", "Any planks"
    "map.quartz", ".. color-box:: #fffcf5", "Any quartz"
    "map.gold", ".. color-box:: #faee4d", ""
    "map.diamond", ".. color-box:: #5cdbd5", ""
    "map.lapis", ".. color-box:: #4a80ff", ""
    "map.emerald", ".. color-box:: #00d93a", ""
    "map.podzol", ".. color-box:: #815631", ""
    "map.netherrack", ".. color-box:: #700200", ""
    "map.deepslate", ".. color-box:: #646464", "Any deepslate"
    "map.raw_iron", ".. color-box:: #d8af93", ""
    "map.glow_lichen", ".. color-box:: #7fa796", ""
    "map.white_terracotta", ".. color-box:: #d1b1a1", ""
    "map.orange_terracotta", ".. color-box:: #9f5224", ""
    "map.magenta_terracotta", ".. color-box:: #95576c", ""
    "map.light_blue_terracotta", ".. color-box:: #706c8a", ""
    "map.yellow_terracotta", ".. color-box:: #ba8524", ""
    "map.lime_terracotta", ".. color-box:: #677535", ""
    "map.pink_terracotta", ".. color-box:: #a04d4e", ""
    "map.gray_terracotta", ".. color-box:: #392923", ""
    "map.light_gray_terracotta", ".. color-box:: #876b62", ""
    "map.cyan_terracotta", ".. color-box:: #575c5c", ""
    "map.purple_terracotta", ".. color-box:: #7a4958", ""
    "map.blue_terracotta", ".. color-box:: #4c3e5c", ""
    "map.brown_terracotta", ".. color-box:: #4c3223", ""
    "map.green_terracotta", ".. color-box:: #4c522a", ""
    "map.red_terracotta", ".. color-box:: #8e3c2e", ""
    "map.black_terracotta", ".. color-box:: #251610", ""
    "map.crimson_nylium", ".. color-box:: #bd3031", ""
    "map.crimson_stem", ".. color-box:: #943f61", ""
    "map.crimson_hyphae", ".. color-box:: #5c191d", ""
    "map.warped_nylium", ".. color-box:: #167e86", ""
    "map.warped_stem", ".. color-box:: #3a8e8c", ""
    "map.warped_hyphae", ".. color-box:: #562c3e", ""
    "map.warped_wart_block", ".. color-box:: #14b485", ""
    "map.white", ".. color-box:: #ffffff", ""
    "map.orange", ".. color-box:: #d87f33", ""
    "map.magenta", ".. color-box:: #b24cd8", ""
    "map.light_blue", ".. color-box:: #6699d8", ""
    "map.yellow", ".. color-box:: #e5e533", ""
    "map.lime", ".. color-box:: #7fcc19", ""
    "map.pink", ".. color-box:: #f27fa5", ""
    "map.gray", ".. color-box:: #4c4c4c", ""
    "map.light_gray", ".. color-box:: #999999", ""
    "map.cyan", ".. color-box:: #4c7f99", ""
    "map.purple", ".. color-box:: #7f3fb2", ""
    "map.blue", ".. color-box:: #334cb2", ""
    "map.brown", ".. color-box:: #664c33", ""
    "map.green", ".. color-box:: #667f33", ""
    "map.red", ".. color-box:: #993333", ""
    "map.black", ".. color-box:: #191919", ""

Sheep coats
===========

.. csv-table::
    :header: "Key", "Default"

    "sheep.white", ".. color-box:: #e6e6e6"
    "sheep.orange", ".. color-box:: #ba6015"
    "sheep.magenta", ".. color-box:: #953a8d"
    "sheep.light_blue", ".. color-box:: #2b86a3"
    "sheep.yellow", ".. color-box:: #bea22d"
    "sheep.lime", ".. color-box:: #609517"
    "sheep.pink", ".. color-box:: #b6687f"
    "sheep.gray", ".. color-box:: #353b3d"
    "sheep.light_gray", ".. color-box:: #757571"
    "sheep.cyan", ".. color-box:: #107575"
    "sheep.purple", ".. color-box:: #66258a"
    "sheep.blue", ".. color-box:: #2d337f"
    "sheep.brown", ".. color-box:: #623f25"
    "sheep.green", ".. color-box:: #465d10"
    "sheep.red", ".. color-box:: #84221c"
    "sheep.black", ".. color-box:: #151518"

Collars
=======

Used on tamed wolf and cat collars.

.. figure:: images/custom_colors/wolf.webp
    :width: 100px

    The red collar on a tamed wolf.

.. figure:: images/custom_colors/cat.webp
    :width: 100px

    The red collar on a tamed cat.

.. csv-table::
    :header: "Key", "Default"

    "collar.white", ".. color-box:: #f9fffe"
    "collar.orange", ".. color-box:: #f9801d"
    "collar.magenta", ".. color-box:: #c74ebd"
    "collar.light_blue", ".. color-box:: #3ab3da"
    "collar.yellow", ".. color-box:: #fed83d"
    "collar.lime", ".. color-box:: #80c71f"
    "collar.pink", ".. color-box:: #f38baa"
    "collar.gray", ".. color-box:: #474f51"
    "collar.light_gray", ".. color-box:: #9d9d97"
    "collar.cyan", ".. color-box:: #169c9c"
    "collar.purple", ".. color-box:: #8932b8"
    "collar.blue", ".. color-box:: #3c44aa"
    "collar.brown", ".. color-box:: #835432"
    "collar.green", ".. color-box:: #5e7c16"
    "collar.red", ".. color-box:: #b02e26"
    "collar.black", ".. color-box:: #1d1d21"

Dyes
====

Base color for banners, beacon beam, tropical fish.
Also affects wolf and cat collars if they are unspecified.

.. csv-table::
    :header: "Key", "Default"

    "dye.white", ".. color-box:: #f9fffe"
    "dye.orange", ".. color-box:: #f9801d"
    "dye.magenta", ".. color-box:: #c74ebd"
    "dye.light_blue", ".. color-box:: #3ab3da"
    "dye.yellow", ".. color-box:: #fed83d"
    "dye.lime", ".. color-box:: #80c71f"
    "dye.pink", ".. color-box:: #f38baa"
    "dye.gray", ".. color-box:: #474f52"
    "dye.light_gray", ".. color-box:: #9d9d97"
    "dye.cyan", ".. color-box:: #169c9c"
    "dye.purple", ".. color-box:: #8932b8"
    "dye.blue", ".. color-box:: #3c44aa"
    "dye.brown", ".. color-box:: #835432"
    "dye.green", ".. color-box:: #5e7c16"
    "dye.red", ".. color-box:: #b02e26"
    "dye.black", ".. color-box:: #1d1d21"

Text
====

.. note:: Colors below ``text.code.15`` are for *text shadows*, if enabled in options

.. csv-table::
    :header: "Key", "Default"

    "text.xpbar", ".. color-box:: #80ff20"
    "text.boss", ".. color-box:: #ff00ff"
    "text.sign", ".. color-box:: #000000"
    "text.code.0", ".. color-box:: #000000"
    "text.code.1", ".. color-box:: #0000aa"
    "text.code.2", ".. color-box:: #00aa00"
    "text.code.3", ".. color-box:: #00aaaa"
    "text.code.4", ".. color-box:: #aa0000"
    "text.code.5", ".. color-box:: #aa00aa"
    "text.code.6", ".. color-box:: #ffaa00"
    "text.code.7", ".. color-box:: #aaaaaa"
    "text.code.8", ".. color-box:: #555555"
    "text.code.9", ".. color-box:: #5555ff"
    "text.code.10", ".. color-box:: #55ff55"
    "text.code.11", ".. color-box:: #55ffff"
    "text.code.12", ".. color-box:: #ff5555"
    "text.code.13", ".. color-box:: #ff55ff"
    "text.code.14", ".. color-box:: #ffff55"
    "text.code.15", ".. color-box:: #ffffff"
    "text.code.16", ".. color-box:: #000000"
    "text.code.17", ".. color-box:: #00002a"
    "text.code.18", ".. color-box:: #002a00"
    "text.code.19", ".. color-box:: #002a2a"
    "text.code.20", ".. color-box:: #2a0000"
    "text.code.21", ".. color-box:: #2a002a"
    "text.code.22", ".. color-box:: #2a2a00"
    "text.code.23", ".. color-box:: #2a2a2a"
    "text.code.24", ".. color-box:: #151515"
    "text.code.25", ".. color-box:: #15153f"
    "text.code.26", ".. color-box:: #153f15"
    "text.code.27", ".. color-box:: #153f3f"
    "text.code.28", ".. color-box:: #3f1515"
    "text.code.29", ".. color-box:: #3f153f"
    "text.code.30", ".. color-box:: #3f3f15"
    "text.code.31", ".. color-box:: #3f3f3f"

Resource loading screen
=======================

.. notconfused::

    :doc:`custom_loading_screens`

.. figure:: images/custom_colors/loading.webp

    Red is ``screen.loading``;
    blue is ``screen.loading.outline``;
    green is ``screen.loading.progress``;
    purple is ``screen.loading.background``

.. csv-table::
    :header: "Key", "Meaning", "Default"

    "screen.loading", "Background color", ".. color-box:: #ffffff"
    "screen.loading.bar", "Loading bar background color, behind bar", ".. color-box:: #ffffff"
    "screen.loading.outline", "Loading bar outline color", ".. color-box:: #000000"
    "screen.loading.progress", "Loading bar foreground color", ".. color-box:: #e22837"
    "screen.loading.blend", "| Logo blending mode. Space-separated of 4 fields, ordered as ``src``, ``dst``, ``dstA``, ``dstB``.
    | Each field must be one of |blendfields|", "*None on all 4 fields*"

.. |blendfields| replace:: ``ZERO``, ``ONE``, ``SRC_COLOR``, ``ONE_MINUS_SRC_COLOR``, ``DST_COLOR``, ``ONE_MINUS_DST_COLOR``, ``SRC_ALPHA``, ``ONE_MINUS_SRC_ALPHA``, ``DST_ALPHA``, ``ONE_MINUS_DST_ALPHA``, ``SRC_ALPHA_SATURATE``

Miscellaneous
=============

.. csv-table::
    :header: "Key", "Meaning", "Default"

    "particle.water", "| Base water particle color *(splashes, bubbles, drops)*.
    | Biome water color multiplier is applied to this value.
    | If the base water texture is grey, in which coloring is via ``misc/watercolor#.png``, this should be set to ``ffffff``", ".. color-box:: #334cff"
    "particle.portal", "| Base portal particle color.
    | A random multiplier between ``0.4`` and ``1.0`` is applied to all three R, G, B values", ".. color-box:: #ff4ce5"
    "fog.end", "Fog used in The End dimension", ".. color-box:: #181318"
    "sky.end", "Color of the sky in The End dimension", ".. color-box:: #282828"
    "lilypad", "Single color, used across **all** biomes", ".. color-box:: #208030"


Aliases
=======

.. csv-table::
    :header: "From", "To"

    "map.snow", "map.white"
    "map.adobe", "map.orange"
    "map.silver", "map.light_gray"
    "map.lightBlue", "map.light_blue"
    "collar.silver", "collar.light_gray"
    "collar.lightBlue", "collar.light_blue"
    "dye.silver", "dye.light_gray"
    "dye.lightBlue", "dye.light_blue"
    "sheep.silver", "sheep.light_gray"
    "sheep.lightBlue", "sheep.light_blue"

Non-color options
=================

Despite its name, other properties not related to colors can also be controlled.

.. csv-table::
    :header: "Key", "Default", "Notes"

    "clouds", "*Unset*", "Override cloud type. Must be one of ``fast``, ``fancy``, ``none``"
    "yVariance", "*Unset*", "If set, add a random integer to the Y coordinate before sampling from the :doc:`colormaps`. Only for ``grid`` colormap format"
    "palette.format", "*Unset*", "Default format for colormaps if not set. Must be one of ``grid``, ``vanilla``, ``fixed``"
    "xporb.time", "628", "Experience orb animation duration (milliseconds)"

Colormaps options
-----------------

.. important:: Although Custom Colors manages both of the files in the two separated lists, they are kept separate because of their file location.

Custom Colors allows the tints of different blocks, entities, and enviornments to be changed with a texture.
Because of their location, you can find this list at :ref:`Colormaps <colormaps:Other colors>` and at :ref:`Lightmaps <custom_lightmaps:Other lightmaps>`.
