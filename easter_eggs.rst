:hero: `<https://www.youtube.com/watch?v=0vyUhiNK8hs>`_

Easter Eggs
###########

.. figure:: images/easter_eggs/icon.webp
    :align: right
    :height: 180px

    Steve with a witch hat 🎃

There are easter eggs on both the OptiFine website and in the OptiFine client.
Some are only visible at certain dates, while others are still visible.

X-Clacks-Overhead
=================

The `OptiFine website <https://optifine.net>`_ returns standard `HTTP headers <https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers>`_.

In addition to the normal headers, the website also returns: ``x-clacks-overhead: GNU Terry Pratchett``.
X-Clacks-Overhead is an unofficial HTTP header to memorialize `Terry Pratchett <https://en.wikipedia.org/wiki/Terry_Pratchett#Death>`_.

.. figure:: images/easter_eggs/xclacks.webp
    :width: 300px

    The header, shown in Firefox.

sp614x's birthday
=================

On the main menu, the splash message will say "Happy birthday, sp614x!" if the current computer date is August 14.

.. figure:: images/easter_eggs/birthday.webp

Cosmetics
=========

Custom Player Models
--------------------

:doc:`Custom Player Models <cpm>` is a feature that can change or add to the player model. Some CPMs are used seasonally.

Capes
-----

Some special :doc:`capes <capes>` or event-related capes may be given to specific people or at specific times.

"10" Cape
~~~~~~~~~~

See :ref:`capes:anniversary capes`.

Secret project capes
~~~~~~~~~~~~~~~~~~~~

See :ref:`capes:unknown project capes`.
