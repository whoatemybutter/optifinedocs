:hero: Star light, star bright

Custom Sky
##########

.. figure:: images/custom_sky/icon.webp
    :align: right
    :height: 180px

    A space skybox with a moon.

.. location::

    /assets/minecraft/optifine/sky/world\*/\*.properties

    /assets/minecraft/optifine/sky/\*\*/\*.png

**Custom Sky** changes the skybox texture and can apply different sets of skies depending on the time, biome, heights, weather, and more.

.. figure:: images/custom_sky/settings.webp

    :menuselection:`Video Settings --> Quality`

Place the file in:

* ``~/sky/world0/`` for Overworld.
* ``~/sky/world1/`` for End.
* ``~/sky/world-1/`` for Nether.

in the resource pack.

Each file represents one layer of the sky.
OptiFine will load them in order by their number, applying one on top of the previous.

Additionally, two special properties files are applied to the sun and moon if present.
This is mainly intended to allow overriding the blend method used by the sun and moon:

* ``~/sky/world0/sun.properties``: replaces ``sun.png``.
* ``~/sky/world0/moon_phases.properties``: replaces ``moon_phases.png``.

.. list-table::

    * - .. figure:: images/custom_sky/sun.webp

            The Vanilla ``sun.png``.

      - .. figure:: images/custom_sky/moon_phases.webp

            The Vanilla ``moon_phases.png``.

Instead of a full skybox, the source texture should match the layout of ``sun.png`` or ``moon_phases.png``.

.. important::

    The "world0" in the path refers to the overworld.
    If there were other worlds with skies *(the Nether and End do not use the standard sky rendering# methods)*, their files would be in ``~/sky/world<world number>``.

.. figure:: images/custom_sky/skybox.webp

    A simple template for a skybox, with directions labelled.
    Image is from `this merge request <https://github.com/sp614x/optifine/pull/3687/commits/6fde1c5424216e8b80e921072c00a09b2282bf08>`_

.. figure:: images/custom_sky/star_sky.webp

    An example of how a skybox might look.

Properties
==========

.. note:: ``speed`` does not affect the fading in and out; this always occurs on a 24-hour cycle.

``startFadeIn``
---------------

.. values::
    :values: String: hh:mm 24-hour format
    :required: No

Fade in/out times.
All times are in hh:mm 24-hour format. See `Time format`_.

If no times are specified, the layer is always rendered.

``endFadeIn``
-------------

.. values::
    :values: See above
    :required: No
    :default: None

``endFadeOut``
--------------

.. values::
    :values: See above
    :required: No
    :default: None

``source``
----------

.. values::
    :values: |file path|
    :required: No
    :default: ``skyN.png`` in same directory, N is properties' file name N.

Path to source texture.

Multiple properties files can reuse the same source.

``blend``
----------

.. values::
    :values: ``add``, ``subtract``, ``multiply``, ``dodge``, ``burn``, ``screen``, ``replace``, ``overlay``, or ``alpha``
    :required: No
    :default: ``add``

The `blending method <https://en.wikipedia.org/wiki/Blend_modes>`_ between fading of layers.

Here, 'previous layer' can refer to the default sky or to the previous custom sky defined by ``sky<N-1>.properties``.
Supported blending methods are:

* ``add``: Add this sky bitmap to the previous layer. In case of overflow, white is displayed.
* ``subtract``: Subtracts this sky bitmap to the previous layer. In case of negative values, black is displayed.
* ``multiply``: Multiply the previous RGBA values by the RGBA values in the current bitmap.
* ``dodge``: Lightens the sky bitmap.
* ``burn``: Darkens the sky bitmap.
* ``screen``: Inverts both layers, multiplies them, and then inverts that result.
* ``replace``: Replace the previous layer entirely with the current bitmap. There is no gradual fading with this method; if brightness computed from the fade times is >0, the full pixel value is used.
* ``overlay``: RGB value > 0.5 brightens the image, < 0.5 darkens.
* ``alpha``:  Weighted average by alpha value.

``rotate``
----------

.. values::
     :values: Boolean
     :required: No
     :default: ``true``

Whether or not the source texture should rotate with the time of day.

``speed``
---------

.. values::
     :values: Positive float
     :required: No
     :default: ``1.0``

Rotation speed as a multiple of the default of one 360-degree cycle per game day.

A value of ``0.5`` rotates every two days.

.. info::

    Irrational values can be useful to make clouds appear in different positions each day.

``axis``
--------

.. values::
    :values: Space-split list of three floats
    :required: No
    :default: ``0.0 0.0 1.0``

The axis of rotation of the skybox.
If a player is looking in the given direction, the skybox will appear to be rotating clockwise around the line of sight.

Default rotation is along the southern axis (rising in the east and setting in the west).

For reference, the vectors corresponding to the six cardinal directions are below.
However, the rotation axis can be any vector except ``0.0 0.0 0.0``::

    down  =  0 -1  0
    up    =  0  1  0
    north =  0  0 -1
    south =  0  0  1
    west  = -1  0  0
    east  =  1  0  0

``days``
--------

.. values::
    :values: Space-split list of integers or integer |range|
    :required: No

The days for which the layer is to be rendered.

Days are numbered from 0 to ``daysLoop``-1, for example: ``days=0 2-4 6``.

``daysLoop``
------------

.. values::
    :values: Positive integer
    :required: No
    :default: ``8``

Number of days in a loop, see above.

``weather``
-----------

.. values::
    :values: ``clear``, ``rain``, or ``thunder``
    :required: No
    :default: ``clear``

Under what weather for which the layer is to be rendered.
Several values can be specified separated by spaces, for example ``weather=clear rain thunder``.

``biomes``
----------

.. values::
    :values: Space-split list of `biome IDs <https://minecraft.wiki/w/Biome#Biome_IDs>`_
    :required: No
    :default: None

Limit the sky to only certain biomes.

``heights``
-----------

.. values::
     :values: Space-split list of integers or integer |range|
     :required: No
     :default: *None*

Limit the sky to only certain heights.

Since 1.18, negative values may be specified for height. When used in a range they have to be put in parenthesis ``( )``: ``(-3)-64``.

``transition``
--------------

.. values::
    :values: Integer
    :required: No
    :default: ``1``

Transition time *(in seconds)* for the layer brightness.
It is used to smooth sharp transitions, for example between different biomes.

Time format
===========

All times are in hh:mm 24-hour format.

For reference,

.. csv-table::

    Sunrise, 6:00, ``/time set 0``
    Noon, 12:00, ``/time set 6000``
    Sunset, 18:00, ``/time set 12000``
    Midnight, 0:00, ``/time set 18000``

The fade times control the brightness when blending.

- between startFadeIn and endFadeIn:   0 up to 1
- between endFadeIn and startFadeOut:  always 1
- between startFadeOut and endFadeOut: 1 down to 0
- between endFadeOut and startFadeIn:  always 0

.. important:: ``startFadeOut`` does not need to be specified; its value is uniquely determined by the other three.

Blender model
=============

.. note:: This Blender model was contributed by `usernamegeri <https://www.curseforge.com/members/usernamegeri/>`_.

You can generate the vector coordinates that are required for the axis of rotation of the skybox by using a pre-made tool for Blender.
It can be found :download:`here <_static/skybox_rotation.blend>`.

.. raw:: html

    <video width=100% controls muted loop playsinline>
        <source src="_static/skybox.webm" type="video/webm">
        Your browser does not support the video tag.
    </video>

JSON schema
===========

.. note:: Although this page is ``.properties`` based, it can be mapped to JSON.

.. literalinclude:: ./schemas/custom_sky.schema.json
