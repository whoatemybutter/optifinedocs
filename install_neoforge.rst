Install With NeoForge
#####################

OptiFine is not yet available for `NeoForge <https://neoforged.net/>`_.
See issue `GH-7626 <https://github.com/sp614x/optifine/issues/7626>`_.
