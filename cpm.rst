:hero: I wear this crown alone

Custom Player Models
####################

.. figure:: images/cpm/icon.webp
	:align: right
	:height: 180px

	sp614x with 2 CPMs active

**Custom Player Models** (**CPM**; sometimes called **Special Cosmetics**) is a feature that can modify player models.
They are loaded on join and can be associated with a specific player.

CPMs are loaded from an online OptiFine server, but may also be specified to work with local files.

.. figure:: images/capes/cape_change.webp
	:width: 750px

	Available online CPMs can be chosen under the Cape Change menu.

CPMs cannot be bought and like :ref:`capes:special capes`, they are *normally* not available to normal donators.

Seasonal CPMs
=============

Some CPMs are available to all donators during certain time periods.

.. figure:: images/capes/cape_change.webp
	:width: 750px

	Available CPMs can be chosen under the Cape Change menu. Here, the Halloween hat is available and enabled.

Christmas
---------

Uses :ref:`cpm:santa hat`.

.. csv-table::
	:header: "Date applied", "Date removed"

	"2020/12/25", "2021/01/01"
	"2021/12/24", "2022/01/01"
	"2022/12/23", "2023/01/01"
	"2023/12/23", "2024/01/01"
	"2024/12/24", "2025/01/01"

Halloween
---------

Uses :ref:`cpm:jingy hat`.

.. csv-table::
	:header: "Date applied", "Date removed"

	"2021/10/30", "2021/11/07"
	"2022/10/26", "2022/11/07"
	"2023/10/30", "2023/11/08"
	"2024/10/29", "2024/11/07"

List of online models
=====================

This is a list of all CPMs available from the OptiFine server.

Santa Hat
---------

A `santa hat <https://en.wikipedia.org/wiki/Santa_suit>`_.

.. md-tab-set::

	.. md-tab-item:: Information

		* ID: ``hat_santa``
		* Model file: `<https://optifine.net/items/hat_santa/model.cfg>`_
		* Texture file: `<https://optifine.net/items/hat_santa/users/sp614x.png>`_

	.. md-tab-item:: Texture

		.. figure:: images/cpm/textures/hat_santa.webp
			:width: 400%
			:class: pixel-edges

	.. md-tab-item:: Render

		.. figure:: images/cpm/renders/hat_santa.webp
			:width: 140px

	.. md-tab-item:: JSON

		.. literalinclude:: include/cpm/hat_santa.json

MrCheeze
--------

Rings at the top and bottom of the head.
It resembles the rings at the top of Lego blocks; MrCheeze's cape is a reference to Legos, as well as his skin and profile.
The ID is the username of an OptiFine Discord moderator.

.. md-tab-set::

	.. md-tab-item:: Information

		* ID: ``mrcheeze``
		* Model file: `<https://optifine.net/items/mrcheeze/model.cfg>`_
		* Texture file: *Uses player's skin*

	.. md-tab-item:: Render

		.. figure:: images/cpm/renders/mrcheeze.webp
			:width: 140px

	.. md-tab-item:: JSON

		.. literalinclude:: include/cpm/mrcheeze.json

Jiingy Hat
----------

A `ushanka <https://en.wikipedia.org/wiki/Ushanka>`_, a Russian hat.
The ID is the username of an OptiFine Discord moderator.

.. md-tab-set::

	.. md-tab-item:: Information

		* ID: ``jiingy``
		* Model file: `<https://optifine.net/items/jiingy/model.cfg>`_
		* Texture file: `<https://optifine.net/items/jiingy/users/sp614x.png>`_

	.. md-tab-item:: Texture

		.. figure:: images/cpm/textures/jiingy.webp
			:width: 400%
			:class: pixel-edges

	.. md-tab-item:: Render

		.. figure:: images/cpm/renders/jiingy.webp
			:width: 140px

	.. md-tab-item:: JSON

		.. literalinclude:: include/cpm/jiingy_hat.json

Jiingy Scarf
------------

A blue and white scarf.
The ID is the username of an OptiFine Discord moderator.

.. md-tab-set::

	.. md-tab-item:: Information

		* ID: ``jiingy_scarf``
		* Model file: `<https://optifine.net/items/jiingy_scarf/model.cfg>`_
		* Texture file: `<https://optifine.net/items/jiingy_scarf/users/sp614x.png>`_

	.. md-tab-item:: Texture

		.. figure:: images/cpm/textures/jiingy_scarf.webp
			:width: 400%
			:class: pixel-edges

	.. md-tab-item:: Render

		.. figure:: images/cpm/renders/jiingy_scarf.webp
			:width: 140px

	.. md-tab-item:: JSON

		.. literalinclude:: include/cpm/jiingy_scarf.json

Kai Ears
--------

Cat ears. Likely for KaiAF, an OptiFine Discord moderator.

.. md-tab-set::

	.. md-tab-item:: Information

		* ID: ``kai_ears``
		* Model file: `<https://optifine.net/items/kai_ears/model.cfg>`_
		* Texture file: *Uses player's skin*

	.. md-tab-item:: Render

		.. figure:: images/cpm/renders/kai_ears.webp
			:width: 140px

	.. md-tab-item:: JSON

		.. literalinclude:: include/cpm/kai_ears.json

Kai Tail
--------

A cat's tail. Likely for KaiAF, an OptiFine Discord moderator.

.. md-tab-set::

	.. md-tab-item:: Information

		* ID: ``kai_tail``
		* Model file: `<https://optifine.net/items/kai_tail/model.cfg>`_
		* Texture file: *Uses player's skin*

	.. md-tab-item:: Render

		.. figure:: images/cpm/renders/kai_tail.webp
			:width: 140px

	.. md-tab-item:: JSON

		.. literalinclude:: include/cpm/kai_tail.json

Back Sword
----------

An old Iron Sword at the player's back.

.. md-tab-set::

	.. md-tab-item:: Information

		* ID: ``back_sword``
		* Model file: `<https://optifine.net/items/back_sword/model.cfg>`_
		* Texture file: `<https://optifine.net/items/back_sword/users/sp614x.png>`_

	.. md-tab-item:: Texture

		.. figure:: images/cpm/textures/back_sword.webp
			:width: 400%
			:class: pixel-edges

	.. md-tab-item:: Render

		.. figure:: images/cpm/renders/back_sword.webp
			:width: 140px

	.. md-tab-item:: JSON

		.. literalinclude:: include/cpm/back_sword.json

Back Pickaxe
------------

An old Iron Pickaxe at the player's back.

.. md-tab-set::

	.. md-tab-item:: Information

		* ID: ``back_pickaxe``
		* Model file: `<https://optifine.net/items/back_pickaxe/model.cfg>`_
		* Texture file: `<https://optifine.net/items/back_pickaxe/users/sp614x.png>`_

	.. md-tab-item:: Texture

		.. figure:: images/cpm/textures/back_pickaxe.webp
			:width: 400%
			:class: pixel-edges

	.. md-tab-item:: Render

		.. figure:: images/cpm/renders/back_pickaxe.webp
			:width: 140px

	.. md-tab-item:: JSON

		.. literalinclude:: include/cpm/back_pickaxe.json

Back Axe
--------

An old Iron Axe at the player's back.

.. md-tab-set::

	.. md-tab-item:: Information

		* ID: ``back_axe``
		* Model file: `<https://optifine.net/items/back_axe/model.cfg>`_
		* Texture file: `<https://optifine.net/items/back_axe/users/sp614x.png>`_

	.. md-tab-item:: Texture

		.. figure:: images/cpm/textures/back_axe.webp
			:width: 400%
			:class: pixel-edges

	.. md-tab-item:: Render

		.. figure:: images/cpm/renders/back_axe.webp
			:width: 140px

	.. md-tab-item:: JSON

		.. literalinclude:: include/cpm/back_axe.json

Back Quiver
-----------

A `quiver <https://minecraft.wiki/w/Quiver>`_ at the player's back.
Quivers are a remnant of Minecraft; the texture was removed in 1.9 snapshot 15w31a.

.. md-tab-set::

	.. md-tab-item:: Information

		* ID: ``back_quiver``
		* Model file: `<https://optifine.net/items/back_quiver/model.cfg>`_
		* Texture file: `<https://optifine.net/items/back_quiver/users/sp614x.png>`_

	.. md-tab-item:: Texture

		.. figure:: images/cpm/textures/back_quiver.webp
			:width: 400%
			:class: pixel-edges

	.. md-tab-item:: Render

		.. figure:: images/cpm/renders/back_quiver.webp
			:width: 140px

	.. md-tab-item:: JSON

		.. literalinclude:: include/cpm/back_quiver.json

Back Bow
--------

This cosmetic has an old-textured Bow at the player's back.

.. md-tab-set::

	.. md-tab-item:: Information

		* ID: ``back_bow``
		* Model file: `<https://optifine.net/items/back_bow/model.cfg>`_
		* Texture file: `<https://optifine.net/items/back_bow/users/sp614x.png>`_

	.. md-tab-item:: Texture

		.. figure:: images/cpm/textures/back_bow.webp
			:width: 400%
			:class: pixel-edges

	.. md-tab-item:: Render

		.. figure:: images/cpm/renders/back_bow.webp
			:width: 140px

	.. md-tab-item:: JSON

		.. literalinclude:: include/cpm/back_bow.json

Back Carrot on a Stick
----------------------

An old Carrot on a Stick at the player's back.

.. md-tab-set::

	.. md-tab-item:: Information

		* ID: ``back_carrot_stick``
		* Model file: `<https://optifine.net/items/back_carrot_stick/model.cfg>`_
		* Texture file: `<https://optifine.net/items/back_carrot_stick/users/sp614x.png>`_

	.. md-tab-item:: Texture

		.. figure:: images/cpm/textures/back_carrot_stick.webp
			:width: 400%
			:class: pixel-edges

	.. md-tab-item:: Render

		.. figure:: images/cpm/renders/back_carrot_stick.webp
			:width: 140px

	.. md-tab-item:: JSON

		.. literalinclude:: include/cpm/back_carrot_stick.json

Back Fishing Rod
----------------

An old Fishing Rod at the player's back.

.. md-tab-set::

	.. md-tab-item:: Information

		* ID: ``back_fishing_rod``
		* Model file: `<https://optifine.net/items/back_fishing_rod/model.cfg>`_
		* Texture file: `<https://optifine.net/items/back_fishing_rod/users/sp614x.png>`_

	.. md-tab-item:: Texture

		.. figure:: images/cpm/textures/back_fishing_rod.webp
			:width: 400%
			:class: pixel-edges

	.. md-tab-item:: Render

		.. figure:: images/cpm/renders/back_fishing_rod.webp
			:width: 140px

	.. md-tab-item:: JSON

		.. literalinclude:: include/cpm/back_fishing_rod.json

Body Boobs
----------

.. md-tab-set::

	.. md-tab-item:: Information

		* ID: ``body_boobs``
		* Model file: `<https://optifine.net/items/body_boobs/model.cfg>`_
		* Texture file: *Uses player's skin*

	.. md-tab-item:: Render

		.. figure:: images/cpm/renders/body_boobs.webp
			:width: 140px

	.. md-tab-item:: JSON

		.. literalinclude:: include/cpm/body_boobs.json

Body Sword
----------

An old Iron Sword at the player's front.

.. md-tab-set::

	.. md-tab-item:: Information

		* ID: ``body_sword``
		* Model file: `<https://optifine.net/items/body_sword/model.cfg>`_
		* Texture file: `<https://optifine.net/items/body_sword/users/sp614x.png>`_

	.. md-tab-item:: Texture

		.. figure:: images/cpm/textures/body_sword.webp
			:width: 400%
			:class: pixel-edges

	.. md-tab-item:: Render

		.. list-table::

			* - .. figure:: images/cpm/renders/body_sword_front.webp
					:width: 140px

					Front

			* - .. figure:: images/cpm/renders/body_sword_back.webp
					:width: 140px

					Back

	.. md-tab-item:: JSON

		.. literalinclude:: include/cpm/body_sword.json

Body Hearth
-----------

A heart at the player's chest.
*Hearth* is likely to be a typo, intended to be *heart*.

.. md-tab-set::

	.. md-tab-item:: Information

		* ID: ``body_hearth``
		* Model file: `<https://optifine.net/items/body_hearth/model.cfg>`_
		* Texture file: `<https://optifine.net/items/body_hearth/users/sp614x.png>`_

	.. md-tab-item:: Texture

		.. figure:: images/cpm/textures/body_sword.webp
			:width: 400%
			:class: pixel-edges

	.. md-tab-item:: Render

		.. figure:: images/cpm/renders/body_hearth.webp
			:width: 140px

	.. md-tab-item:: JSON

		.. literalinclude:: include/cpm/body_hearth.json

Arrow Hat
---------

An arrow through the player's head.

.. md-tab-set::

	.. md-tab-item:: Information

		* ID: ``hat_arrow``
		* Model file: `<https://optifine.net/items/hat_arrow/model.cfg>`_
		* Texture file: `<https://optifine.net/items/hat_arrow/users/sp614x.png>`_

	.. md-tab-item:: Texture

		.. figure:: images/cpm/textures/hat_arrow.webp
			:width: 400%
			:class: pixel-edges

	.. md-tab-item:: Render

		.. list-table::

			* - .. figure:: images/cpm/renders/hat_arrow.webp
					:width: 140px

					Initial

			* - .. figure:: images/cpm/renders/hat_arrow_front.webp
					:width: 140px

					Front

	.. md-tab-item:: JSON

		.. literalinclude:: include/cpm/hat_arrow.json

Axe Hat
-------

An axe through the player's head.

.. md-tab-set::

	.. md-tab-item:: Information

		* ID: ``hat_axe``
		* Model file: `<https://optifine.net/items/hat_axe/model.cfg>`_
		* Texture file: `<https://optifine.net/items/hat_axe/users/sp614x.png>`_

	.. md-tab-item:: Texture

		.. figure:: images/cpm/textures/hat_axe.webp
			:width: 400%
			:class: pixel-edges

	.. md-tab-item:: Render

		.. error:: Missing render

	.. md-tab-item:: JSON

		.. literalinclude:: include/cpm/hat_axe.json

Bee Antenna
-----------

A bee antenna coming out of the east side of the player's head.

.. md-tab-set::

	.. md-tab-item:: Information

		* ID: ``hat_bee``
		* Model file: `<https://optifine.net/items/hat_bee/model.cfg>`_
		* Texture file: `<https://optifine.net/items/hat_bee/users/sp614x.png>`_

	.. md-tab-item:: Texture

		.. figure:: images/cpm/textures/hat_bee.webp
			:width: 400%
			:class: pixel-edges

	.. md-tab-item:: Render

		.. figure:: images/cpm/renders/hat_bee.webp
			:width: 140px

	.. md-tab-item:: JSON

		.. literalinclude:: include/cpm/hat_bee.json

Jingy Hat
---------

A modified Witch Hat, used for Halloween.

The name *Jingy* is misspelled; it is supposed to be *Jiingy*.
The ID is a reference to `Jiingy <https://namemc.com/profile/Jiingy.1>`_, an OptiFine Discord admin.

.. md-tab-set::

	.. md-tab-item:: Information

		* ID: ``hat_jingy``
		* Model file: `<https://optifine.net/items/hat_jingy/model.cfg>`_
		* Texture file: `<https://optifine.net/items/hat_jingy/users/sp614x.png>`_

	.. md-tab-item:: Texture

		.. figure:: images/cpm/textures/hat_jingy.webp
			:width: 400%
			:class: pixel-edges

	.. md-tab-item:: Render

		.. figure:: images/cpm/renders/hat_jingy.webp
			:width: 140px

	.. md-tab-item:: JSON

		.. literalinclude:: include/cpm/hat_jingy.json

Link Hat
--------

A prototype of the Santa Hat.

.. md-tab-set::

	.. md-tab-item:: Information

		* ID: ``hat_link``
		* Model file: `<https://optifine.net/items/hat_link/model.cfg>`_
		* Texture file: `<https://optifine.net/items/hat_link/users/sp614x.png>`_

	.. md-tab-item:: Texture

		.. figure:: images/cpm/textures/hat_link.webp
			:width: 400%
			:class: pixel-edges

	.. md-tab-item:: Render

		.. figure:: images/cpm/renders/hat_link.webp
			:width: 140px


	.. md-tab-item:: JSON

		.. literalinclude:: include/cpm/hat_link.json

Pickaxe Hat
-----------

An old Iron Pickaxe through the player's head.

.. md-tab-set::

	.. md-tab-item:: Information

		* ID: ``hat_pickaxe``
		* Model file: `<https://optifine.net/items/hat_pickaxe/model.cfg>`_
		* Texture file: `<https://optifine.net/items/hat_pickaxe/users/sp614x.png>`_

	.. md-tab-item:: Texture

		.. figure:: images/cpm/textures/hat_pickaxe.webp
			:width: 400%
			:class: pixel-edges

	.. md-tab-item:: Render

		.. error:: Missing render

	.. md-tab-item:: JSON

		.. literalinclude:: include/cpm/hat_pickaxe.json

Reddit Hat
----------

Similar to ``hat_bee``, except that the antenna is more centered, and the yellow dot is white.
It is intended to look like Snoo's (the alien-like mascot of Reddit) single antenna.

.. md-tab-set::

	.. md-tab-item:: Information

		* ID: ``hat_reddit``
		* Model file: `<https://optifine.net/items/hat_reddit/model.cfg>`_
		* Texture file: `<https://optifine.net/items/hat_reddit/users/sp614x.png>`_

	.. md-tab-item:: Texture

		.. figure:: images/cpm/textures/hat_reddit.webp
			:width: 400%
			:class: pixel-edges

	.. md-tab-item:: Render

		.. figure:: images/cpm/renders/hat_reddit.webp
			:width: 140px

	.. md-tab-item:: JSON

		.. literalinclude:: include/cpm/hat_reddit.json

Reindeer Antlers
----------------

Two reindeer antlers from the top of the player's head.

.. md-tab-set::

	.. md-tab-item:: Information

		* ID: ``hat_reindeer``
		* Model file: `<https://optifine.net/items/hat_reindeer/model.cfg>`_
		* Texture file: `<https://optifine.net/items/hat_reindeer/users/sp614x.png>`_

	.. md-tab-item:: Texture

		.. figure:: images/cpm/textures/hat_reindeer.webp
			:width: 400%
			:class: pixel-edges

	.. md-tab-item:: Render

		.. error:: Missing render

	.. md-tab-item:: JSON

		.. literalinclude:: include/cpm/hat_reindeer.json

Shovel Hat
----------

An old Iron Shovel in the player's head.

.. md-tab-set::

	.. md-tab-item:: Information

		* ID: ``hat_shovel``
		* Model file: `<https://optifine.net/items/hat_shovel/model.cfg>`_
		* Texture file: `<https://optifine.net/items/hat_shovel/users/sp614x.png>`_

	.. md-tab-item:: Texture

		.. figure:: images/cpm/textures/hat_shovel.webp
			:width: 400%
			:class: pixel-edges

	.. md-tab-item:: Render

		.. error:: Missing render

	.. md-tab-item:: JSON

		.. literalinclude:: include/cpm/hat_shovel.json

Witch Hat
---------

This cosmetic looks like the Vanilla `Witch's <https://minecraft.wiki/w/Witch>`_ hat. It is not the same as ``hat_jingy``.

.. md-tab-set::

	.. md-tab-item:: Information

		* ID: ``hat_witch``
		* Model file: `<https://optifine.net/items/hat_witch/model.cfg>`_
		* Texture file: `<https://optifine.net/items/hat_witch/users/sp614x.png>`_

	.. md-tab-item:: Texture

		.. figure:: images/cpm/textures/hat_witch.webp
			:width: 400%
			:class: pixel-edges

	.. md-tab-item:: Render

		.. figure:: images/cpm/renders/hat_witch.webp
			:width: 140px

	.. md-tab-item:: JSON

		.. literalinclude:: include/cpm/hat_witch.json

Nose Up
-------

A nose, raised up.

.. md-tab-set::

	.. md-tab-item:: Information

		* ID: ``head_nose_up``
		* Model file: `<https://optifine.net/items/head_nose_up/model.cfg>`_
		* Texture file: *Uses player's skin*

	.. md-tab-item:: Render

		.. figure:: images/cpm/renders/head_nose_up.webp
			:width: 140px

	.. md-tab-item:: JSON

		.. literalinclude:: include/cpm/head_nose_up.json

Nose Down
---------

A nose, lowered down.

.. md-tab-set::

	.. md-tab-item:: Information

		* ID: ``head_nose_up``
		* Model file: `<https://optifine.net/items/head_nose_down/model.cfg>`_
		* Texture file: *Uses player's skin*

	.. md-tab-item:: Render

		.. figure:: images/cpm/renders/head_nose_down.webp
			:width: 140px

	.. md-tab-item:: JSON

		.. literalinclude:: include/cpm/head_nose_down.json

Villager Nose
-------------

A Villager's nose.

.. md-tab-set::

	.. md-tab-item:: Information

		* ID: ``head_nose_villager``
		* Model file: `<https://optifine.net/items/head_nose_villager/model.cfg>`_
		* Texture file: `<https://optifine.net/items/head_nose_villager/users/sp614x.png>`_

	.. md-tab-item:: Texture

		.. figure:: images/cpm/textures/head_nose_villager.webp
			:width: 400%
			:class: pixel-edges

	.. md-tab-item:: Render

		.. figure:: images/cpm/renders/head_nose_villager.webp
			:width: 140px

	.. md-tab-item:: JSON

		.. literalinclude:: include/cpm/head_nose_villager.json

Mouse Ears
----------

An mouse's ears in the player's head.

.. md-tab-set::

	.. md-tab-item:: Information

		* ID: ``ears_mouse``
		* Model file: `<https://optifine.net/items/ears_mouse/model.cfg>`_
		* Texture file: `<https://optifine.net/items/ears_mouse/users/sp614x.png>`_

	.. md-tab-item:: Texture

		.. figure:: images/cpm/textures/ears_mouse.webp
			:width: 400%
			:class: pixel-edges

	.. md-tab-item:: Render

		.. figure:: images/cpm/renders/ears_mouse.webp
			:width: 140px

	.. md-tab-item:: JSON

		.. literalinclude:: include/cpm/ears_mouse.json

Angel Wings
-----------

An angel's wings.

.. md-tab-set::

	.. md-tab-item:: Information

		* ID: ``wings_angel``
		* Model file: `<https://optifine.net/items/wings_angel/model.cfg>`_
		* Texture file: `<https://optifine.net/items/wings_angel/users/sp614x.png>`_

	.. md-tab-item:: Texture

		.. figure:: images/cpm/textures/wings_angel.webp
			:width: 400%
			:class: pixel-edges

	.. md-tab-item:: Render

		.. figure:: images/cpm/renders/wings_angel.webp
			:width: 140px

	.. md-tab-item:: JSON

		.. literalinclude:: include/cpm/wings_angel.json

Wolverine Hand
--------------

Long "claws" coming out of the player's hand.

.. md-tab-set::

	.. md-tab-item:: Information

		* ID: ``hand_wolverine``
		* Model file: `<https://optifine.net/items/hand_wolverine/model.cfg>`_
		* Texture file: `<https://optifine.net/items/hand_wolverine/users/sp614x.png>`_

	.. md-tab-item:: Texture

		.. figure:: images/cpm/textures/hand_wolverine.webp
			:width: 400%
			:class: pixel-edges

	.. md-tab-item:: Render

		.. error:: Missing render

	.. md-tab-item:: JSON

		.. literalinclude:: include/cpm/hand_wolverine.json

Technical details
=================

After trying to load a player's cape, OptiFine will also check *base* + ``/users/USERNAME.cfg``,
where ``USERNAME`` is the player's **case-corrected** username.

The term "**base**" refers to how OptiFine is loading the CPM, and thus where it's looking.
CPMs may be loaded from 2 places:

* :si-icon:`fontawesome/solid/globe` Online

	.. important:: Here, **base** is the *URL* ``http://s.optifine.net``

	By default, OptiFine will load CPMs through the OptiFine website if local CPMs are not enabled.
	OptiFine will use the URL ``http://s.optifine.net`` as the 'base' for file locations and model references.

* :si-icon:`fontawesome/solid/folder` Locally

	.. important:: Here, **base** is the *file path* ``.minecraft/playermodels``

	If enabled, OptiFine will load CPMs through files locally stored on your computer.
	Instead of querying a website, it will use your filesystem as the 'base' for file locations and model references.

	Local CPMs are not enabled by default. To enable local CPMs:

	1. Set ``-Dplayer.models.local=true`` in :doc:`jvm_args`.
	2. Create a ``playermodels/`` folder in your ``.minecraft`` folder. *Or, if you are using MultiMC, your instance's folder.*

	.. warning:: When local CPMs are enabled, online CPMs will not load.

If the path is to a valid `JSON <https://en.wikipedia.org/wiki/JSON>`_ file, OptiFine will process it.

For each object in the root ``"items"`` array, OptiFine will load the model referenced in ``"type"``.
Models are obtained from *base* + ``/items/TYPE/model.cfg``, where ``TYPE`` is the value of key ``"type"``.

Next, if the model does not use the player's skin and does not specify a texture,
OptiFine will load that texture from *base* + ``items/TYPE/users/USERNAME.png``.

If a texture cannot be loaded, red wool replaces it.

.. warning::

	Models with ``"usePlayerTexture": true`` and ``"textureSize": [64, 32]`` will not load correctly, as they depend on old 64 x 32 player skin sizes.

In summary, this structure is:

.. code:: none

	BASE/ (URL or local folder)
		users/
			USERNAME.cfg (what models a player has)
		items/
			MODEL/
				model.cfg (the model)
				users/
					USERNAME.cfg (the model's texture for this player)

Limitations
===========

CPMs have some limitations that may make them unsuitable for certain needs:

* *(local only)* They cannot be in resource packs.
* *(local only)* You can only target specific players by username.
* They require OptiFine, and *(local only)* specific JVM arguments.
* They are client-side, and *(local only)* other players will not see your own CPMs.
* They do not support animations, as in :doc:`cem_animation`.

External links
==============

.. warning:: |elwarn|

* `Video guide by Ewan Howell <https://www.youtube.com/watch?v=0rT-q95dpV0>`_

JSON schema
===========

.. literalinclude:: ./schemas/cpm_player.schema.json
	:caption: Player CFG file

.. literalinclude:: ./schemas/cpm_model.schema.json
	:caption: Model file
