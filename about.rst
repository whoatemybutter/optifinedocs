:hero: All about the documentation

About
#####

This is the about page for the OptiFine **documentation**.

Is this official?
=================

**No**.

This documentation itself is not official, but the source of most of it was created by sp614x and can be found
`on GitHub <https://github.com/sp614x/optifine/tree/master/OptiFineDoc/doc>`_.

The ReadTheDocs documentation is maintained by WhoAteMyButter, and sp614x does not edit it directly;
hosted documentation is updated to the upstream later.

Is this legitimate?
===================

**Yes**.

The *original* documentation here can be found `on GitHub`_.
This project aims to make the source human-readable while also expanding on it with better explanations, details, examples, and more.

When was this made?
===================

1. Check :doc:`the changelog <changelog>` for the initial release date and for any further changes.
2. Go to `the source <https://gitlab.com/whoatemybutter/optifinedocs>`_ and check the commit history.

I found a bug!
==============

.. quote::

    Use the source, Luke!

Head over to :doc:`contributing` and see what you can do!

What's the license?
===================

.. figure:: images/about/cc0.svg
    :align: right
    :width: 200px

    The icon for CC0.

Both the OptiFine documentation both here, and at the GitHub repository, are in the public domain.
This documentation is explicitly licensed under `CC0 <https://creativecommons.org/share-your-work/public-domain/cc0/>`_.

This means you are free to reproduce it, modify it, sell it, print it, and translate it, among other freedoms (free as in libre).

What about older versions?
==========================

OptiDocs does not offer documentation for older versions (such as 1.8). There are many reasons why, but here are three:

1. Maintaining different versions of documentation for different game versions would be excessively difficult.
2. Older versions are old, and may have bugs that have already been fixed in newer releases.
3. The vast majority of Minecraft servers do not use older versions. [1]_

Regardless, if you require the ability to see older documentation, check the `commit history of the GitHub source <https://github.com/sp614x/optifine/commits/master/OptiFineDoc/doc>`_.
It is up to you to find the correct commit for your needs.

.. [1] See `<https://bstats.org/global/bukkit#minecraftVersion>`_.
