:hero: My potions are too strong for you, traveler

Limitations
###########

.. figure:: images/cem/icon_limitations.webp
    :align: right
    :height: 180px
    :class: pixel-edges

    Nuh-uh

While CEM is a powerful feature, it does have some limitations.

Parent bones
============

On each CEM model, the model is limited to using the parent bones that that entity has by default.
A full list of these can be found :doc:`here <cem_entity_names>`.
Every single element added must be inside one of these parent bones.
Adding a new parent bone will cause the model to fail to load in-game.

Pivot points
============

The pivot points of the entities **cannot be modified in Vanilla**.
An example of something that cannot be modified is moving the leg of a cow, as that would require moving its leg's pivot point.

However, this can be done using :doc:`CEM Animation <cem_animation>`.
While modelling the entity, pivot points on bones will behave as expected, allowing elements to rotate around a point, however this is not the case for parent bones.

When a template model is loaded, all the parent bones will already have their pivot points set up correctly.
Do not touch these, or the model will break when loaded in-game.

If, for whatever reason, the pivot points need to be moved in the model, this is how it works::

    The elements are tied to the pivot point in game.
    Increasing the gap between the pivot point and the elements will increase the gap between the actual pivot point and the elements in game.

    For example, if the pivot point is moved 12 pixels east inside the model, the elements will appear 12 pixels west in game.
    This is because, as the pivot point cannot be moved in game, the elements will move instead.
    The elements render relative to the pivot point.
