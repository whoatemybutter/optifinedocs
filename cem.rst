:hero: Transformers, roll out

Custom Entity Models
####################

.. figure:: images/cem/icon.webp
    :align: right
    :height: 180px

    He's had surgery

.. location::

    /assets/minecraft/optifine/cem/\*\*/\*.jem

    /assets/minecraft/optifine/cem/\*\*/\*.jpm

**Custom Entity Models** (**CEM**) can completely change how an entity appears, animates, and moves.

The file format for CEM are `JSON <https://en.wikipedia.org/wiki/JSON>`_ with the extensions ``.jem`` and ``.jpm``.

:doc:`Model files <cem_models>` (``.jem``) contain a list of :doc:`entity part models <cem_parts>`, which may be loaded inline or from a file ``.jpm`` file.
These model files define a texture, and a list of submodels. These submodels define what part of an entity they attach to, how they attach, their :doc:`animations <cem_animation>`, and more.

.. figure:: images/cem/settings.webp

    :menuselection:`Video Settings --> Quality`

.. toctree::
    :maxdepth: 1

    cem_models
    cem_parts
    cem_animation
    cem_entity_names
    cem_limitations

External links
==============

.. warning:: |elwarn|

* `Model information overview <https://wynem.com/cem/>`_
* `Video guide by Ewan Howell <https://youtube.com/watch?v=arj2eim42KI>`_
* `CEM Template Loader for Blockbench <https://ewanhowell.com/plugins/cem-template-loader>`_
