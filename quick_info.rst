:hero: Packed like sardines

Quick Info
##########

.. figure:: images/quick_info/icon.webp
    :align: right
    :height: 156px
    :class: pixel-edges

    Snippet of "compact" labels.

**Quick Info** is a configurable heads-up-display positioned at the top of the screen.

It is similar to the Debug Screen, but it is customizable, includes less information overall, and is compact.
If the Debug Screen is open, Quick Info is hidden.

It replaces the older :menuselection:`Show FPS` option.

.. figure:: images/quick_info/example.webp
    :height: 90px
    :class: pixel-edges

    An example of the Quick Info screen, all options enabled, detailed labels

Options
=======

.. figure:: images/quick_info/settings.webp
    :align: right

    :menuselection:`Video Settings --> Quick Info`

Some options are simple boolean toggles *(on/off)*, while some are not *(off/compact/full)*.

The "All ON" and "All OFF" shortcut buttons toggle all options except for :guilabel:`Labels` and :guilabel:`Background`.

.. csv-table::
    :header: "Name", "Options", "Example (assume *detailed* labels)"
    :widths: 13, 52, 35

    "Rendered Chunks", "ON/OFF: Number of rendered chunks", "``Chunks: 6``"
    "Rendered Particles", "ON/OFF: Number of rendered particles", "``Particles: 105``"
    "Rendered Entities", "ON/OFF: Number of rendered entities + block entities", "``Entities: 14+7``"
    "GPU load", "ON/OFF: GPU load ratio for rendering this frame", "``GPU: 56%``"
    "Biome", "ON/OFF: Current biome name", "``Biome: the_void``"
    "Light", "ON/OFF: Sky light level, block light level", "``Light: 7 sky, 4 block``"
    "Native Memory", "OFF/Compact/Full:

    * Compact: Buffers used/buffers max + textures (in MB)
    * Full: Bufferes used/buffers max + textures, GPU memory (in MB)", "``Native: 15/4096+43 MB, GPU: 0+43 MB``"
    "Memory", "OFF/Compact/Full:

    * Compact: Used memory/total available memory (in MB)
    * Full: Used memory/total available memory (in MB), allocation rate (in MB/s)", "``Memory: 366/4096 MB, Allocation: 8 MB/s``"
    "Target Fluid", "OFF/Compact/Full:

    * Compact: Fluid name
    * Full: Fluid name (target X, Y, Z)", "``Target Fluid: water (5, -60, 13)``"
    "Target Block", "OFF/Compact/Full:

    * Compact: Block name
    * Full: Block name (target X, Y, Z)", "``Target Block: lime_shulker_box (9, 31, -60)``"
    "Target Entity", "OFF/Compact/Full:

    * Compact: Entity name
    * Full: Entity name (target X, Y, Z)", "``Target Entity: painting (5.000, -59.500, 13.969)``"
    "Framerate", "OFF/Compact/Full:

    * Compact: Average frames per second
    * Full: Average and minimum frames per second", "``171/93 fps``"
    "Chunk Updates", "ON/OFF: Number of chunk updates per frame", "``Updates: 2``"
    "Position", "OFF/Compact/Full:

    * Compact: (X, Y, Z) coords
    * Full: (X, Y, Z) coords, chunk-relative [CX, CY, CZ] coords", "``Position: (6.113, -59.000, 12.870) [6, 5, 12]``"
    "Facing", "OFF/Compact/Full:

    * Compact: Direction name [axis±]
    * Full: Direction name [axis±] (yaw/pitch)", "``Facing: south [Z+] (41.8/55.2)``"
    "Labels", "Compact/Full/Detailed:

    * Compact: Abbreviated labels, integer numbers, joins lines, no units
    * Full: Unabbreviated labels, integer numbers
    * Detailed: Unabbreviated labels, full numbers", ""
    "Background", "ON/OFF: Translucent text background", ""
