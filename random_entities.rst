:hero: RGB slimes

Random Entities
###############

.. location::

    /assets/minecraft/optifine/random/\*\*/\*.png

    /assets/minecraft/optifine/random/\*\*/\*.properties

.. figure:: images/random_entities/icon.webp
    :align: right
    :height: 180px

    Rabbits can be `frogs <https://www.youtube.com/watch?v=Y9tbKb_fgqg>`_.

**Random Entities** changes an entity's texture based on its qualities, such as position, NBT rules, the time, and more.

.. figure:: images/random_entities/settings.webp

    :menuselection:`Video Settings --> Quality`

.. figure:: images/random_entities/piglin.webp

    Random entities applied to a `Piglin <https://minecraft.wiki/w/Piglin>`_

Vanilla textures are assigned a texture number of ``1``.

Files
=====

This file can be placed in the ``optifine/random/`` folder of the resource pack, parallel to the vanilla texture in ``textures/``:

.. md-tab-set::

    .. md-tab-item:: Primary (vanilla) texture

        * ``/assets/minecraft/textures/entity/creeper/creeper.png``

    .. md-tab-item:: Alternates

        * ``/assets/minecraft/optifine/random/entity/creeper/creeper2.png``
        * ``/assets/minecraft/optifine/random/entity/creeper/creeper3.png``
        * ``/assets/minecraft/optifine/random/entity/creeper/creeper4.png``
        * *etc.*

    .. md-tab-item:: Properties *(optional)*

        * ``/assets/minecraft/optifine/random/entity/creeper/creeper.properties``

Textures ending with a digit use the separator ``.``.

.. md-tab-set::

    .. md-tab-item:: Primary (vanilla) texture

        * ``/assets/minecraft/textures/entity/warden/warden_pulsating_spots_2.png``

    .. md-tab-item:: Alternates

        * ``/assets/minecraft/optifine/random/entity/warden/warden_pulsating_spots_2.2.png``
        * ``/assets/minecraft/optifine/random/entity/warden/warden_pulsating_spots_2.3.png``
        * ``/assets/minecraft/optifine/random/entity/warden/warden_pulsating_spots_2.4.png``

    .. md-tab-item:: Properties (optional)

        * ``/assets/minecraft/optifine/random/entity/warden/warden_pulsating_spots_2.properties``

The textures and configurations in ``/assets/minecraft/optifine/mob/`` are also supported.

Matching order
==============

Each rule specifies a range of entity textures to use and one or more conditions under which to use them.

The entity coordinates when it spawns *(single player)* or when it is first seen by the client *(multiplayer)* are checked against each rule in sequence.

The first rule that matches wins.
If no rule matches, the default texture (e.g., creeper.png) is used.

If no ``.properties`` :doc:`file <syntax>` is present for an entity, then all available textures are used for that type of entity.

Entities with multiple textures will use the ``.properties`` file for the base texture.

In other words, all of these do not need to be created explicitly:

* ``wolf.properties``
* ``wolf_tame.properties``
* ``wolf_angry.properties``

Just ``wolf.properties`` will work for all three, provided there are the same number of textures for each.
Similarly for ``"_eyes"`` and ``"_overlay"``.

Properties
==========

.. important::
    For all of these, ``N`` is a number that links the individual properties together to form a rule.
    ``N`` starts at 1. You **should not** skip numbers.

``textures.N``
--------------

.. values::
    :values: Space-split list of texture indices
    :required: Yes

List of entity textures to use.

The texture index ``1`` is the default texture from ``/assets/minecraft/texture``.
Alternatively, the legacy property ``skins.N`` can be used.

``skins.N``
-----------

.. values::
    :values: See ``textures.N``
    :required: No

``weights.N``
-------------

.. values::
    :values: Space-split list of integers
    :required: No
    :default: None

List of weights to apply to the random choice.

.. important::

    Weights do not have to total 100, or any other particular value.
    The number of weights should match the number of textures.

``biomes.N``
------------

.. values::
    :values: List of strings
    :required: No
    :default: None

List of biomes.

The vanilla biome names are listed `here <https://minecraft.wiki/Biome#Biome_IDs>`_.
Biomes added by mods can also be used.

If the first character is ``!``, the list is **inverted**. For example:

.. code-block:: properties

    biomes.1=!jungle deep_ocean birch_forest

will match a biome that is *neither* a Jungle, Deep Ocean, or Birch Forest.

.. note:: Prefixing each biome name with ``!`` does nothing.


``heights.N``
-------------

.. values::
    :values: |range| of integers
    :required: No
    :default: *None*

Height ranges.

Replaces legacy ``minHeight`` and ``maxHeight`` properties.
Since 1.18, negative values may be specified for height. When used in a range they have to be put in parenthesis ``( )``: ``(-3)-64``.

``name.N``
----------

.. values::
    :values: String
    :required: No

Entity name.

Uses the :ref:`syntax:strings` syntax.

``professions.N``
-----------------

.. values::
    :values: Space-split list of strings: ``none``, ``armorer``, ``butcher``, ``cartographer``, ``cleric``, ``farmer``, ``fisherman``, ``fletcher``, ``leatherworker``, ``librarian``, ``mason``, ``nitwit``, ``shepherd``, ``toolsmith``, and ``weaponsmith``, along with an optional level experience format
    :required: No

List of villager professions with optional levels.

Entry format: ``<profession>[:level1,level2,...]``.

Example:

* Professions farmer (all levels) or librarian (levels 1,3,4): ``professions=farmer librarian:1,3-4``.
* Professions fisher, shepard, nitwit: ``professions=fisherman shepherd nitwit``.

``colors.N``
------------

.. values::
    :values: Space-split list of strings: ``white``, ``orange``, ``magenta``, ``light_blue``, ``yellow``, ``lime``, ``pink``, ``gray``, ``light_gray``, ``cyan``, ``purple``, ``blue``, ``brown``, ``green``, ``red``, and ``black``
    :required: No

List of wolf/cat collar colors or sheep/llama/shulker box/bed colors.

Example: ``colors.2=pink magenta purple``.

The legacy property ``collarColors`` is also recognized.

``baby.N``
----------

.. values::
    :values: Boolean
    :required: No

If entity is a baby.
Only valid for mobs.

``health.N``
------------

.. values::
    :values: Space-split list of integers or integer |range|, percents
    :required: No

Range of health values; can also be given in percent.
Only valid for mobs.

Example:

.. code:: properties

    health.1=10
    health.2=5-8 10-12
    health.3=0-50%

``moonPhase.N``
---------------

.. values::
    :values: Space-split list of integers or integer |range|
    :required: No

List of moon phases, from 0 to 7.

Example:

.. code:: properties

    moonPhase.1=3
    moonPhase.2=0 1 2
    moonPhase.1=0-2 4-7

``dayTime.N``
-------------

.. values::
    :values: Space-split list of integers or integer |range|
    :required: No
    :default: None

List of day times in ticks (0-24000)

Example:

.. code:: properties

    dayTime.1=2000-10000
    dayTime.2=0-1000 18000-24000

``weather.N``
-------------

.. values::
    :values: Space-split list of strings: ``clear``, ``rain``, and ``thunder``
    :required: No

Weather conditions.

``sizes.N``
-----------

.. values::
     :values: Space-split list of integers or integer |range|
     :required: No

Size of entity, if applicable.

Slimes and magma cubes *naturally* spawn in three sizes: ``0=small, 1=medium, 3=big``.
*Naturally* spawning phantoms only spawn in one size, ``0``.

.. important:: This is only valid for mobs with multiple sizes (0-255 for slimes and magma cubes, and 0-64 for phantoms).

Example:

.. code:: properties

    textures.1=3
    textures.2=0 1 3
    textures.3=0-2 4-7

``nbt.N.TAG``
-------------

.. values::
    :values: :ref:`NBT matching rule <syntax:nbt format>`
    :required: No
    :default: None

:ref:`syntax:NBT format`-based rule.

.. important:: See :ref:`syntax:client-side data`.

``blocks.N``
------------

.. values::
    :values: Block
    :required: No
    :default: None

For entities, this checks the block on which the entity is standing.
For block entities, this checks the block of the block entity.

Examples
========

Creeper
-------

Uses ``creeper10.png`` through ``creeper14.png`` for all underground creepers.
``creeper13.png`` will be used 7.3% *(3/(10+10+10+3+10))* of the time.

.. code:: properties

    textures.1=10-14
    weights.1=10 10 10 3 10
    heights.1=0-55

    # Use 5, 7, 9 in high, hilly areas.
    textures.2=5 7 9
    biomes.2=windswept_hills desert forest badlands jagged_peaks stony_peaks
    heights.2=80-255

    # Fallback rule if nothing else matches.
    # Remember that if no rule matches, only the base creeper/creeper.png will be used.
    textures.3=1-4 6 8 15-20

Omit Vanilla texture
--------------------

Uses ``slime2.png`` through ``slime16.png`` for all slimes, not including the vanilla texture.

.. code:: properties

    textures.1=2-16

External links
==============

.. warning:: |elwarn|

* `Tutorial by Ewan Howell <https://ewanhowell.com/guides/random-entity-textures>`_

JSON schema
===========

.. note:: Although this page is ``.properties`` based, it can be mapped to JSON.

.. literalinclude:: ./schemas/random_entities.schema.json
