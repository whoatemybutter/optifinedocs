:hero: The installation wizard must be stopped

Installation
############

This page outlines the steps to install OptiFine for any version on any operating system.

.. toctree::

    install_pre
    download
    install_vanilla
    install_neoforge
    install_forge
    install_multimc

If, after any of these steps, you run into a problem, see :doc:`troubleshooting`.
