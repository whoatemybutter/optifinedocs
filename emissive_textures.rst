:hero: May be radioactive

Emissive Textures
#################

.. figure:: images/emissive_textures/icon.webp
    :align: right
    :height: 180px

    Emissive diamond ore.

.. location::

    /assets/minecraft/optifine/emissive.properties

**Emissive Textures** add a second texture on top of a block which will render with no darkness and will be unaffected by lightmaps.

.. important:: They do not change the actual lighting around them. This does not affect actual lighting, just the pixel's brightness.

It is possible to add overlays to block textures, which will always rendered with full brightness.
This can simulate light emitting parts of the textures.

.. figure:: images/emissive_textures/ores.webp

    An example of emissive ores in a dark water cave.

.. figure:: images/emissive_textures/settings.webp

    :menuselection:`Video Settings --> Quality`

The emissive overlays have the same name as the base texture + custom suffix.
For example:

* ``bedrock.png``: base texture
* ``bedrock_e.png``: emissive overlay

The emissive overlays are rendered in the same block layer as the base texture, except overlays for textures from layer ``SOLID``, which are rendered as ``CUTOUT_MIPPED``.
The overlays can also be used for items, mobs and block entities.

Properties
==========

``suffix.emissive``
-------------------

.. values::
    :values: String
    :required: No
    :default: ``_e``

The suffix a file must have to be registered as emissive.


Armor trims
===========

Emissive `armor trim <https://minecraft.wiki/w/Smithing_Template>`_ textures are defined by adding one of the following material suffixes to the trim base name:

* ``amethyst``
* ``copper``
* ``diamond``
* ``diamond_darker``
* ``emerald``
* ``gold``
* ``gold_darker``
* ``iron``
* ``iron_darker``
* ``lapis``
* ``netherite``
* ``netherite_darker``
* ``quartz``
* ``redstone``

For example (if ``suffix.emissive`` is ``_e``)::

    coast_amethyst_e.png, host_iron_darker_e.png, dune_leggings_netherite_e.png

Translucency
============

Textures that have the ability to be translucent (such as slime blocks) may have their brightness changed by an emissive texture.

For example, if a pixel's transparency is 50% (0x7F in bytes), that emissive pixel will be half as 'bright'.

Limitations
===========

Emissives cannot be applied to:

* End Crystals
* End Crystal Beams
* Ender Dragon
* Paintings
* Clouds
* End Sky
* Rain
* Snowflakes (weather effect)
* Particles
* Tridents

External links
==============

.. warning:: |elwarn|

* `Tutorial by Ewan Howell <https://ewanhowell.com/guides/emissive-textures>`_

JSON schema
===========

.. note:: Although this page is ``.properties`` based, it can be mapped to JSON.

.. literalinclude:: ./schemas/emissive_textures.schema.json
