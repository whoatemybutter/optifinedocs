:hero: Some assembly required

Parts
#####

.. location::

    /assets/minecraft/optifine/cem/\*\*/\*.jpm

CEM part files contain the definition of one model part to be referenced in a CEM model file.

Keys
====


``invertAxis``
--------------

.. values::
    :values: String: permutation of "xyz"
    :required: No

Axes to invert: "xyz" inverts the X, Y, Z axes.
An empty string inverts none of them and is equal the key being absent.

``translate``
-------------

.. values::
    :values: Array of 3 integers
    :required: No

`Translate <https://en.wikipedia.org/wiki/Translation_(geometry)>`__ (move) the texture by the 3 integers ``x``, ``y``, and ``z``.

``rotate``
----------

.. values::
    :values: Array of 3 integers
    :required: No

`Rotation <https://en.wikipedia.org/wiki/Rotation>`__ (spin) the texture by the 3 integers ``angle_x``, ``angle_y``, and ``angle_z``.

``mirrorTexture``
-----------------

.. values::
    :values: String permutation of "uv"
    :required: No

Texture axis to mirror.
``"uv"`` will mirror both the U and V axis.
An empty string mirrors along no axis and is equal the key being absent.

``boxes``
---------

.. values::
    :values: Array of objects
    :required: No

Array of part model boxes.

``textureOffset``
~~~~~~~~~~~~~~~~~

.. values::
    :values: Array of 2 integers
    :required: Yes

Texture offset for the box format; ``[x, y]``.

``uv<FACE>``
~~~~~~~~~~~~

.. values::
    :values: Array of 4 strings
    :required: Yes

``<FACE>`` is one of ``Down``, ``Up``, ``North``, ``South``, ``West``, or ``East``.

UV for face. Ordered as [``u1``, ``v1``, ``u2``, ``v2``].

``coordinates``
~~~~~~~~~~~~~~~

.. values::
    :values: Array of 6 integers
    :required: Yes

Box position and dimensions.
Ordered as [``x``, ``y``, ``z``, ``width``, ``height``, ``depth``].

``sizeAdd``
~~~~~~~~~~~

.. values::
    :values: Integer
    :required: No

Size increment added to all dimensions; can be used for asymmetric scaling.

``sizesAdd``
~~~~~~~~~~~~

.. values::
    :values: Array of integer
    :required: No

Separate size increments can be used for asymmetric scaling.

Ordered as [``size_add_x``, ``size_add_y``, ``size_add_z``].

``sprites``
-----------

.. values::
    :values: Array of objects
    :required: No

List of 3D sprite models; depth 1.

``textureOffset``
~~~~~~~~~~~~~~~~~

.. values::
    :values: Array of 2 integers
    :required: Yes

Texture offset; ``[x, y]``.

``coordinates``
~~~~~~~~~~~~~~~

.. values::
    :values: Array of 6 integers
    :required: Yes

Box position and dimensions.
Ordered as [``x``, ``y``, ``z``, ``width``, ``height``, ``depth``].

``sizeAdd``
~~~~~~~~~~~

.. values::
    :values: Integer
    :required: No

Size increment added to all dimensions; can be used for asymmetric scaling.

``submodel``
------------

.. values::
    :values: Self, CEM part object
    :required: No

A sub-model part; attached to the parent, moving and rotating with it.

``submodels``
-------------

.. values::
    :values: List of CEM part objects
    :required: No

A list of sub-model parts; attached to the parent, moving and rotating with it.

Texture UV
==========

.. warning:: Texture UV cannot have both specifications, either ``"textureOffset"`` or ``uv<face>``, not both.

Texture UV can be specified in box format with:

-  ``"textureOffset"``, or
-  ``"uvDown"``, ``"uvUp"``, ``"uvNorth"``, ``"uvSouth"``, ``"uvWest"``, and ``"uvEast"``.

.. figure:: images/cem/model_box.webp

    The box format UV mapping.

Attachment points
=================

Entities can have items/blocks attached to them.
The attachments system allows you to render these items/blocks attached to model parts of your choice.

.. csv-table::
    :header: "Attachment name", "Supported models"

    "left_handheld_item", "allay, armor_stand, drowned, evoker, giant, husk, illusioner, piglin, piglin_brute, pillager, skeleton, stray, vex, vindicator, wither_skeleton, zombie, zombie_villager, zombified_piglin"
    "right_handheld_item", "allay, armor_stand, drowned, evoker, giant, husk, illusioner, piglin, piglin_brute, pillager, skeleton, stray, vex, vindicator, wither_skeleton, zombie, zombie_villager, zombified_piglin"

JSON schema
===========

.. literalinclude:: ./schemas/cem_part.schema.json
