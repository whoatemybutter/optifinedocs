Install With MultiMC
####################

.. note:: This applies to derivatives of MultiMC, like PolyMC or Prism.

Read `<https://github.com/MultiMC/Launcher/wiki/MultiMC-and-OptiFine#as-a-json-patch>`_.
