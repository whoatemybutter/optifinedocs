:hero: Minecraft bling

Capes
#####

.. figure:: images/capes/icon.webp
    :width: 180px
    :align: right

    The standard cape design.

**Capes** are a cosmetic given to OptiFine users who have `donated <https://optifine.net/donate>`_ and checked the option for it.

The cape can be edited by opening the options menu in :menuselection:`Skin Customization --> OptiFine Cape --> Open Cape Editor`.

Options
=======

Capes can be customized in 2 main styles: standard and banner.

Standard
--------

The **standard** design features the OptiFine logo (OF).
It has 10 pre-defined color swatches, alongside the 'Custom' option.
The Custom option allows changing the color of the top, bottom, text, and text shadow. The top and bottom form a gradient.

.. info:: Standard cape designs
    :collapsible:

    .. figure:: images/capes/standard.webp
        :width: 200px
        :class: pixel-edges

        Standard
    .. figure:: images/capes/red.webp
        :width: 200px
        :class: pixel-edges

        Red
    .. figure:: images/capes/yellow.webp
        :width: 200px
        :class: pixel-edges

        Yellow
    .. figure:: images/capes/green.webp
        :width: 200px
        :class: pixel-edges

        Green
    .. figure:: images/capes/cyan.webp
        :width: 200px
        :class: pixel-edges

        Cyan
    .. figure:: images/capes/blue.webp
        :width: 200px
        :class: pixel-edges

        Blue
    .. figure:: images/capes/purple.webp
        :width: 200px
        :class: pixel-edges

        Purple
    .. figure:: images/capes/white.webp
        :width: 200px
        :class: pixel-edges

        White
    .. figure:: images/capes/gray.webp
        :width: 200px
        :class: pixel-edges

        Gray
    .. figure:: images/capes/black.webp
        :width: 200px
        :class: pixel-edges

        Black

Banner
------

The **banner** design lets a banner pattern be used as a design.
The banner format used is not identical to Minecraft's. Since February 14, 2025, banner capes allow 32 color options and up to 16 layers.

The colors are:

.. info:: List of colors
    :collapsible:

    * .. color-box:: #1d1d21
    * .. color-box:: #b02e26
    * .. color-box:: #5e7c16
    * .. color-box:: #835432
    * .. color-box:: #3c44aa
    * .. color-box:: #169c9c
    * .. color-box:: #9d9d97
    * .. color-box:: #474f52
    * .. color-box:: #f38baa
    * .. color-box:: #80c71f
    * .. color-box:: #fed83d
    * .. color-box:: #3ab3da
    * .. color-box:: #c74ebd
    * .. color-box:: #f9801d
    * .. color-box:: #f9fffe
    * .. color-box:: #383838
    * .. color-box:: #a8781c
    * .. color-box:: #208219
    * .. color-box:: #ab9b35
    * .. color-box:: #2e77ab
    * .. color-box:: #ab2c87
    * .. color-box:: #44ad79
    * .. color-box:: #cfcfcf
    * .. color-box:: #787878
    * .. color-box:: #ff936b
    * .. color-box:: #32c739
    * .. color-box:: #d99a2e
    * .. color-box:: #5571fa
    * .. color-box:: #884ad9
    * .. color-box:: #3bd9a7
    * .. color-box:: #f0f0cc
    * .. color-box:: #5c1c1c
    * .. color-box:: #882d2d
    * .. color-box:: #74ab68
    * .. color-box:: #e29f00
    * .. color-box:: #688dab
    * .. color-box:: #ab689e
    * .. color-box:: #68ab95
    * .. color-box:: #7b68aa
    * .. color-box:: #ff9ceb
    * .. color-box:: #ffaf9c
    * .. color-box:: #aeff9c
    * .. color-box:: #ffd99c
    * .. color-box:: #9cd2ff
    * .. color-box:: #b89cff
    * .. color-box:: #9cffde
    * .. color-box:: #fff89c

To design a banner, go to `<https://livzmc.net/banner/>`_.

.. figure:: images/capes/livzmc.webp
    :width: 860px
    :align: center

    The LivzMC cape/banner editor.

Banner designs have some restrictions and limitations:

* Banners cannot have over 16 layers, *not including the base color*.
* Banners cannot contain the `"Thing" pattern <https://minecraft.wiki/w/Banner_Pattern>`_ (which resembles the Mojang logo).
* Banners with offensive patterns may be manually removed at any time.

A collection of the most popular banner cape designs is available at https://optifine.net/banners.

Anniversary capes
=================

Anniversary capes are cape styles that are only usable at certain times and dates.

10th anniversary
----------------

Most notably, the "10" cape replaced the Classic design during OptiFine's 10th anniversary.

.. csv-table::
    :header: "Texture file", "Texture resolution", "Dates applied (YYYY/MM/DD)"

    ".. image:: images/capes/10/original.webp", "46px by 22px", "2021/04/08 - 2021/04/15"

April 8th, 2021, marked OptiFine's 10th birthday.
To celebrate, all classic "OF" capes were changed to a "10" design.

Colors were the same as the standard capes designs'.

.. figure:: images/capes/10/player.webp

    How the "10" cape looks on a player.

.. info:: 10th anniversary cape designs
    :collapsible:

    .. figure:: images/capes/10/original.webp
        :width: 200px
        :class: pixel-edges

        Standard
    .. figure:: images/capes/10/red.webp
        :width: 200px
        :class: pixel-edges

        Red
    .. figure:: images/capes/10/yellow.webp
        :width: 200px
        :class: pixel-edges

        Yellow
    .. figure:: images/capes/10/green.webp
        :width: 200px
        :class: pixel-edges

        Green
    .. figure:: images/capes/10/cyan.webp
        :width: 200px
        :class: pixel-edges

        Cyan
    .. figure:: images/capes/10/blue.webp
        :width: 200px
        :class: pixel-edges

        Blue
    .. figure:: images/capes/10/purple.webp
        :width: 200px
        :class: pixel-edges

        Purple
    .. figure:: images/capes/10/white.webp
        :width: 200px
        :class: pixel-edges

        White
    .. figure:: images/capes/10/gray.webp
        :width: 200px
        :class: pixel-edges

        Gray
    .. figure:: images/capes/10/black.webp
        :width: 200px
        :class: pixel-edges

        Black

Special capes
=============

Some players have "special" capes, given by sp614x.
These players are often long-time contributors or supporters of OptiFine.

These capes **are not** purchaseable, and are not given to regular donators.

.. important::

    *Postmaster* is the only cape with an official name. All other capes' names are **not official** and are given only for categorization.


Code 1
------

.. figure:: images/capes/special/code1.webp
    :width: 30em
    :class: pixel-edges

Designed by jckt.
Given to jckt for moderating the OptiFine Discord, and creating the OptiFine Discord bot.

Code 2
------

.. figure:: images/capes/special/code2.webp
    :width: 30em
    :class: pixel-edges

Modified version of :ref:`capes:Code 1`.


Graph 1
-------

.. figure:: images/capes/special/graph1.webp
    :width: 30em
    :class: pixel-edges

Designed by jckt.
Given to filefolder3 for compiling graphs and statistics about OptiFine releases.

Graph 2
-------

.. figure:: images/capes/special/graph2.webp
    :width: 30em
    :class: pixel-edges

Modified version of :ref:`capes:Graph 1`.


HD Grey 1
---------

.. figure:: images/capes/special/hdgrey1.webp
    :width: 30em
    :class: pixel-edges

Given to EskiMojo14 for testing high-resolution capes.

HD Grey 2
---------

.. figure:: images/capes/special/hdgrey2.webp
    :width: 30em
    :class: pixel-edges

Modified version of :ref:`capes:HD Grey 1`.

HD Grey 3
---------

.. figure:: images/capes/special/hdgrey3.webp
    :width: 30em
    :class: pixel-edges

Modified version of :ref:`capes:HD Grey 2`.

HD Red
------

.. figure:: images/capes/special/hdred.webp
    :width: 30em
    :class: pixel-edges

Designed by sp614x.
Given to therealokin.
Modified version of :ref:`capes:HD Grey 1`.

Lego
----

.. figure:: images/capes/special/lego.webp
    :width: 30em
    :class: pixel-edges

Designed by Jiingy.
Given to MrCheeze445 for moderating the OptiFine Discord.

Lion
----

.. figure:: images/capes/special/lion.webp
    :width: 30em
    :class: pixel-edges

Designed by sp614x.
Given to sp614x.

Orange Heart
------------

.. figure:: images/capes/special/orangeheart.webp
    :width: 30em
    :class: pixel-edges

Designed by Kirbyrocket.
Given to FakeRetroBot for moderating the OptiFine Discord.
It has since been revoked for being sold.

Postmaster 1
------------

.. figure:: images/capes/special/postmaster1.webp
    :width: 30em
    :class: pixel-edges

Designed by Jiingy.
Given to ZenW for working on support e-mail.

Postmaster 2
------------

.. figure:: images/capes/special/postmaster2.webp
    :width: 30em
    :class: pixel-edges

Modified version of :ref:`capes:Postmaster 1`.

Postmaster 3
------------

.. figure:: images/capes/special/postmaster3.webp
    :width: 30em
    :class: pixel-edges

Modified version of :ref:`capes:Postmaster 2`.

Rainbow
-------

.. figure:: images/capes/special/rainbow.webp
    :width: 30em
    :class: pixel-edges

Snow
----

.. figure:: images/capes/special/snow.webp
    :width: 30em
    :class: pixel-edges

Designed by Jiingy.
Given to Jiingy for moderating the OptiFine Discord.

Transgender
-----------

.. figure:: images/capes/special/transgender.webp
    :width: 30em
    :class: pixel-edges

Designed by ZenithKnight.
Given to KyriaBirb for moderation of the OptiFine Discord.

Locking
=======

Capes can be "locked", so they can only be moved to a different username after logging in through the OptiFine website.
Once locked, they cannot be moved from the in-game Cape Editor. This is to prevent stealing capes when a Minecraft account is compromised.

.. figure:: images/capes/locked.webp

    In the in-game editor, locked capes cannot be moved through the Minecraft account.

Technical details
=================

The OptiFine client fetches capes for donators by querying the URL ``http://s.optifine.net/capes/<NAME>.png``.

The native size of Minecraft's capes is 64px x 32px.
The OptiFine cape server generally returns capes that are 46px x 22px, with the exception of :ref:`capes:specialty capes <capes:special capes>` granted to specific users.

On load, a canvas is initialized with a size of 64px x 32px.
If the supplied cape image is larger in width or height, it doubles the dimensions of the canvas repeatedly until it is large enough to fit the original image.
The supplied cape image is then placed onto this larger canvas, ensuring that the resulting image inherits the proper 2:1 aspect ratio.

This has the added benefit of compatibility with the 44x34 legacy capes that did not include the elytra section.

.. figure:: images/capes/template/labeled.webp

    A labeled template of the cape UV map.

Unknown project capes
=====================

.. warning:: This information is not official and was `gathered <https://gitlab.com/whoatemybutter/optifinedocs/-/issues/5>`_ by ZenithKnight.

Two prototypes for new cape textures were presented by sp614x as part of an unknown project, but these were scrapped.

.. list-table::

    * - .. figure:: images/capes/secret1.webp
            :width: 200px
            :align: left
            :class: pixel-edges

            The Standard cape design.

      - .. figure:: images/capes/secret2.webp
            :width: 200px
            :align: left
            :class: pixel-edges

            A black and white variant.
