:hero: What do you mean you've seen this? It's brand new

Changelog
#########

This is the changelog for the ReadTheDocs documentation, **not** OptiFine.
This changelog follows `Keep A Changelog 1.1.0 <https://keepachangelog.com/en/1.1.0/>`__.

2025 February 23
================

Added
-----

* New banner cape colors and options to :doc:`capes`.
* Colorboxes where applicable.
* ``h+v`` and ``v+h`` methods to :doc:`ctm`.
* Some baby names to :doc:`cem_entity_names`.

Changed
-------

* Variables, functions, etc. are better labeled in :doc:`cem_animation`.
* Reorganized parts of :doc:`capes`.
* Reorganized and updated :doc:`custom_colors`.
* Updated GitHub source to commit `967a9fdb <https://github.com/sp614x/optifine/commit/967a9fdbf2c8f8d4466715cd6334a2d304642d5c>`__.

Removed
-------

* All ``versionadded`` and ``versionchanged`` admonitions.

2025 January 3
==============

Added
-----

* ``sizesAdd`` to :doc:`cem_parts` (`!26 <https://gitlab.com/whoatemybutter/optifinedocs/-/merge_requests/26>`__).
* New entity names to :doc:`cem_entity_names` (`!29 <https://gitlab.com/whoatemybutter/optifinedocs/-/merge_requests/29>`__).
* "External links" section to all relevant pages (`!30 <https://gitlab.com/whoatemybutter/optifinedocs/-/merge_requests/30>`__).
* More views (renders) and textures to :doc:`cpm` (`!31 <https://gitlab.com/whoatemybutter/optifinedocs/-/merge_requests/31>`__).
* JSON schemas about :doc:`cpm` (!31).

Changed
-------

* Name of internal shaders to debug shaders in :doc:`shaders` (`!27 <https://gitlab.com/whoatemybutter/optifinedocs/-/merge_requests/27>`__).
* Usage of ``modelOffset`` and ``chunkOffset`` in :doc:`shaders_dev` (`!28 <https://gitlab.com/whoatemybutter/optifinedocs/-/merge_requests/28>`__).
* GBuffers ``gbuffers_cloud`` default is now none (!28).
* Ranges of variables ``time``, ``frame_counter`` in :doc:`cem_animation` (!29).
* Link to shaderLABS in :doc:`texture_properties` (!30).
* Overhauled :doc:`cpm` (!31).
* CPM information in :doc:`easter_eggs` (!31).
* Bumped dependency requirements.
* Updated GitHub source to commit `4cc1b80c <https://github.com/sp614x/optifine/commit/4cc1b80c243c3cb83a75406ad4c98671fa64592a>`__.

Fixed
-----

* Location admonitions not separating items.

2024 September 26
=================

Added
-----

* :doc:`quick_info` document (`!18 <https://gitlab.com/whoatemybutter/optifinedocs/-/merge_requests/18>`__).
* 1.20.5 Components to :doc:`syntax`, under :ref:`syntax:components, nbt` (`!16 <https://gitlab.com/whoatemybutter/optifinedocs/-/merge_requests/16>`__).
* Explanation of Component/NBT types to :doc:`syntax`, under :ref:`syntax:Types`.
* :doc:`custom_sky` "settings" screenshot figure (`!17 <https://gitlab.com/whoatemybutter/optifinedocs/-/merge_requests/17>`__).
* :doc:`license` document (`!19 <https://gitlab.com/whoatemybutter/optifinedocs/-/merge_requests/19>`__).

Changed
-------

* Changed to a ``master`` and ``dev`` branch setup (`!20 <https://gitlab.com/whoatemybutter/optifinedocs/-/merge_requests/20>`__).
* Moved :doc:`syntax` *File name* section into :ref:`syntax:paths, file locations` (!16).
* Wording of :ref:`syntax:block states` in :doc:`syntax` (!16).
* All in-game "settings" screenshots have been retaken to be transparent, compressed, and consistent (!17).
* Captions of all "settings" screenshots' figures are now just the menu selection (!17).
* Wording of :doc:`custom_animations` (`!15 <https://gitlab.com/whoatemybutter/optifinedocs/-/merge_requests/15>`__).
* Updated GitHub source to commit `7b2f8c63 <https://github.com/sp614x/optifine/commit/7b2f8c635dfdd6a112dfcbf9972b7d18b1c088bb>`__ (`!21 <https://gitlab.com/whoatemybutter/optifinedocs/-/merge_requests/21>`__).
* Wind Charge names in :doc:`cem_entity_names`. (!15)

Removed
-------

* Icons from headers (`!22 <https://gitlab.com/whoatemybutter/optifinedocs/-/merge_requests/22>`__).
* Genindex document (`!23 <https://gitlab.com/whoatemybutter/optifinedocs/-/merge_requests/23>`__).
* Glossary document (!23).

Fixed
-----

* Cross-referencing warnings (`!25 <https://gitlab.com/whoatemybutter/optifinedocs/-/merge_requests/25>`_).

2024 September 4
================

Changed
-------

* Updated GitHub source to commit `c0f3d77 <https://github.com/sp614x/optifine/commit/c0f3d7786731e52e7a5341bbc253cb1adb4b4498>`_.


2024 September 1
================

Added
-----

* More examples to :ref:`syntax:strings`.

Fixed
-----

* Explanation of wildcards in :ref:`syntax:pattern`.
* Minor capitalization error in :ref:`syntax:strings`.

2024 July 27
============

Fixed
-----

* Typo in :doc:`emissive_textures`. (MrFrank2716)

2024 July 9
===========

Added
-----

* ``connectBlocks`` to ``method=overlay`` in :doc:`ctm`.
* Biome inversion to :doc:`ctm`.
* Biome inversion to :doc:`random_entities`.
* Note wrt. ``axis`` property in :doc:`ctm`.
* Note on older versions in :doc:`about`.

Changed
-------

* Reworded ``tintIndex`` in :doc:`ctm`.
* Reworded explanation of ``linked`` in :doc:`ctm`.
* Explanation of length of ``weights`` in :doc:`ctm`.
* Bumped dependency requirements.

Removed
-------

* ``symmetry`` from ``method=repeat`` in :doc:`ctm`.
* Incorrect note claiming that ``faces`` was ignored for non-full blocks in :doc:`ctm`.
* ``matchTiles`` name implication trick from :doc:`ctm`.
* ``weight`` property (*singular*, **not** ``weights``) from :doc:`ctm`.

Fixed
-----

* Heading nesting in :doc:`ctm`.
* Wrong JSON schema ``$id`` for :doc:`ctm`.
* Incorrect description of ``name`` property in :doc:`ctm`.
* Incorrect description of ``name`` property in :doc:`custom_guis`.
* Incorrect note of ``linked`` in :doc:`ctm`.
* rST epilog syntax error.

2024 June 12
============

Added
-----

* Tile index explanation to :doc:`ctm`.

Changed
-------

* Fandom link to Minecraft Wiki in :doc:`custom_guis`.
* Bumped pinned dependency versions.

Fixed
-----

* Moved and merged duplicate ``ctm.N`` property in :doc:`ctm`.
* Incorrect path explanation in :doc:`ctm`.

2024 May 22
===========

Added
-----

* Option of ``0`` at :doc:`natural_textures`.

2024 April 30
=============

Fixed
-----

* Incorrect image numbering in :doc:`ctm`.

2024 April 20
=============

Added
-----

* Arrow, shield, spectral arrow, trident to :doc:`cem_entity_names`.
* ``root`` part names to many entities in :doc:`cem_entity_names`.
* Explanation of "File location" syntax to :ref:`syntax:paths, file locations`.
* Attachment points to :doc:`cem_parts`.

Changed
-------

* Wording of ``N`` rule at :doc:`random_entities`.
* Updated GitHub source to commit `15ef3106 <https://github.com/sp614x/optifine/commit/15ef31064323d7e1c5959ab8f9e8a260f0750124>`_.
* Updated Sphinx Immaterial theme to commit `9519601a <https://github.com/jbms/sphinx-immaterial/commit/9519601aa71dd47a362d1b639a11cf37a3675536>`_.

Removed
-------

* ``dirt`` part name from :doc:`cem_entity_names`.

2024 March 13
=============

Added
-----

* ``breeze`` and such to :doc:`cem_entity_names`.
* ``wind_charge`` to :doc:`cem_entity_names`.
* Small icons to document titles (table of contents support unclear).
* :doc:`install_neoforge` for future releases (if applicable).
* :ref:`emissive_textures:translucency` section.
* Tutorial/guide to :doc:`emissive_textures`.

Changed
-------

* Updated ``bat`` in :doc:`cem_entity_names`.
* Hero of :doc:`easter_eggs`.
* :ref:`cem_animation:Entity variables` section's wording.
* Clarified ``print`` and ``printb`` in :ref:`cem_animation:numerical functions`.
* Bolded headers of :doc:`cem_animation`.
* Small wording change in :doc:`emissive_textures`.
* Updated GitHub source to commit `dc7b4aca <https://github.com/sp614x/optifine/commit/dc7b4aca0630ce34cf9bc9a77396809aedef90fe>`_.
* Updated Sphinx Immaterial theme to commit `d965e31c <https://github.com/jbms/sphinx-immaterial/commit/d965e31c466ff3322d2e8262d313a979f7572cce>`_.

Fixed
-----

* Incorrect figure scaling in :doc:`ctm`.

2024 January 30
===============

Added
-----

* New :doc:`custom_colors` options.

Changed
-------

* :doc:`custom_sky` blender model. (closes #24)
* Updated GitHub source to commit `feb2a450 <https://github.com/sp614x/optifine/commit/feb2a45042f0be7933ca01cf4910ea21ae9c2ace>`_.
* Updated Sphinx Immaterial theme to commit `cf8493ed <https://github.com/jbms/sphinx-immaterial/commit/cf8493ed9e32a3bd4caab15e233e3918afb7d6a9>`_.

Fixed
-----

* Keyboard extension warnings when building.

Removed
-------

* ``material`` option in :doc:`ctm`.

2023 November 19
================

Added
-----

* More images to :doc:`better_grass`.
* More images to :doc:`natural_textures`.
* Note that :doc:`custom_panoramas` is broken.
* Limitations section in :doc:`emissive_textures`.
* Darker dark mode.
* genindex, compliment to Glossary.
* A **Last update:** note to end of all pages.

Changed
-------

* Re-did the :doc:`homepage <index>`.
* Updated :doc:`faq`.
* Moved some questions from FAQ to :doc:`troubleshooting`.
* :doc:`cpm` now references the model JSONs.
* CPM model JSONs are now stored in ``include/cpm/`` and can be easily downloaded.
* Updated :doc:`syntax`.
* Updated images in :doc:`ctm`.
* Updated :doc:`ctm`.
* Simplified :doc:`hd_fonts`.
* Updated JSON schema for HD Fonts.
* Layout of :doc:`json_schemas`.
* Many article icons are now transparent and theme-neutral.

Fixed
-----

* Incorrect model render for ``head_nose_villager`` in :doc:`cpm`.
* Incorrect image in :doc:`better_grass`.
* Numerous typos and grammatical errors throughout.
* Autosectionlabel duplicate warning messages.

Removed
-------

* Icon from :doc:`custom_animations`; it was misleading.

2023 September 24
=================

Changed
-------

* Changed all Minecraft Fandom links to the new `Minecraft Wiki <https://minecraft.wiki>`_. (closes #22)

Fixed
-----

* Hyphen typo in :doc:`syntax`. (closes #19)


2023 September 13
=================

Fixed
-----

* ``rot_y`` and ``rot_x`` mistakes in :ref:`cem_animation:Entity parameters`.

2023 August 15
==============

Fixed
-----

* OpenGraph meta tags not showing.
* Small fixes and tab/space inconsistency issues.
* Tutorial links in :doc:`shaders_dev`.
* Mistake in :ref:`syntax:pattern`.

2023 August 14
==============

Added
-----

* Updated to commit `8ed2130d <https://github.com/sp614x/optifine/commit/8ed2130d8a619ae7c27edee9ac864e161b437575>`_.
* sp614x's birthday section in :doc:`easter_eggs`.
* More examples to :doc:`cem_models`.

2023 August 6
=============

Added
-----

* Better Style Guide.

Changed
-------

* Rearranged section order in :doc:`capes`.
* Renamed :ref:`capes:unknown project capes`

Fixed
-----

* Mistakes and inaccuracies in :ref:`capes:special capes`.
* Dead links to :doc:`cpm`.

2023 August 4
=============

Added
-----

* Reproducible builds.
    * ``.readthedocs.yaml`` file.
    * Pinned requirements.
* A :doc:`contributor's guide <contributing>`.
* More renders to :doc:`cpm`.
* Added more special capes to :ref:`capes:special capes`.
* More Glossary terms.

Changed
-------

* Renamed Special Cosmetics to :doc:`cpm`.
* :ref:`capes:special capes` have been reorganized.
* :doc:`easter_eggs` has been reorganized.

Fixed
-----

* Tense and capitalization fixes.

Removed
-------

* Top banner announcement.


2023 July 2
===========

Changed
-------

* Changed wording of overlay note in :doc:`ctm`.

2023 June 26
============

Added
-----

* Armor trims to :ref:`emissive_textures:armor trims`.

Changed
-------

* Updated to `commit 61a0581a <https://github.com/sp614x/optifine/commit/61a0581a1812c6c5a86ec004a3015ed0b9a52b7c>`_.

Fixed
-----

* Incorrect path syntax in :doc:`syntax`.
* Broken cross-heading references.

2023 June 10
============

Added
-----

* Re-added the accidentally removed "Complete file list" section from :ref:`Custom Colors <custom_colors:Miscellaneous colormaps>`.

Fixed
-----

* :doc:`capes` having incorrect Technical details wording.

Removed
-------

* Hovering tooltips, they don't work anymore.

2023 June 6
===========

Added
-----

* Checkboxes.
* Useful links at footer.
* Licensing terms to :doc:`about`.
* Most pages now have a "hero" subtitle (text at the header).
* :doc:`shaders` page.
* All pages now have attached a `JSON schema <https://json-schema.org>`_, if applicable.
* :doc:`cem_parts` page.
* :doc:`json_schemas` page.
* Social media cards (for embed links).
* More :doc:`troubleshooting problems <troubleshooting>`.
* More "version added" notes.

Fixed
-----

* Link errors in :doc:`about`.
* Link errors in :doc:`cpm`.
* Typos in :doc:`cem_limitations`.
* Inconsistent articles and grammar in many pages.
* Bad menuselection syntax.

Changed
-------

* Documentation theme is now `Sphinx Immaterial <https://jbms.github.io/sphinx-immaterial/index.html>`_.
* Changelog future entries now follow `Keep A Changelog 1.1.0 <https://keepachangelog.com/en/1.1.0/>`_.
    * Deviation from standard in date-keeping; this is intentional.
* "Table-style" properties have been overhauled and replaced with a better "header" system.
    * This allows for better linking to specific sections.
    * Sections can contain better examples and some may have pictures *in the future*.
* Titles in Installation documents.
* Link at :doc:`install_multimc`.
* CEM documents are now at the top-level (no subfolders).
* Updated **many** pages.
* Heavily overhauled :doc:`cem`.
* Location admonitions now use the `glob format <https://michaelcurrin.github.io/dev-cheatsheets/cheatsheets/shell/files/globbing.html>`_ (\* and \*\*).
* Updated to commit `22f0481b <https://github.com/sp614x/optifine/commit/22f0481b37de5bf5cfa8e7c8d428c333a35d4263>`_.
* Changed code `tab size <https://developer.mozilla.org/en-US/docs/Web/CSS/tab-size>`_ to 2 (from 4).
* :doc:`jvm_args` is now under Information.
* :doc:`cpm` is now under Information.

Removed
-------

* Tables for resource pack feature properties.
* Badges at top of index page; replaced by footer links on all pages.
* Smartquotes.

2023 April 12
=============

* Documentation is now open source, can be found at `<https://gitlab.com/whoatemybutter/optifinedocs>`_
    * Split into two branches, ``master`` and ``dev``
    * ``master`` is the hosted version
    * ``dev`` contains the future documentation updates

2023 February 9
===============

* Updated :doc:`custom_sky`
* Added download button styling

2023 February 5
===============

* Converted all images to lossless `WebP <https://en.wikipedia.org/wiki/WebP>`_ (`support <https://caniuse.com/webp>`_)
* Reorganized table of contents
* Added raw:, range:, exists: to :doc:`syntax`
* Overhauled installation instructions, see :doc:`install`
* Re=did changelog page, date format is now (year) (month) (day)
* Removed Broken Features
* Added :doc:`troubleshooting`
* Added more information about Banner Capes to :doc:`capes`

2022 December 8
===============

* Overhauled Properties Files page, it is now :doc:`syntax`
* Updated to commit `b98f576e <https://github.com/sp614x/optifine/commit/b98f576eff92065891e6b1b74fe2798d3987a369>`_
* Changed dark theme colors
* Added Glossary

2022 August 27
==============

* Added Broken Features
* Updated :doc:`Installation <install>`
* Updated to commit `8410499f <https://github.com/sp614x/optifine/commit/8410499f00c72792a7445aaf695ee78fad841eef>`_

2022 July 21
============

* Removed note about old versions; it is now expected for you to run an updated game
* Added :doc:`Special Cosmetics <cpm>`
* Updated :doc:`CEM animations <cem_animation>`
* Updated :doc:`CEM entity names <cem_entity_names>`
* Updated to commit `8174f510 <https://github.com/sp614x/optifine/commit/8174f510b2f3a81f467a6c3475a64818187b1abc>`_

2022 April 6
============

* Replaced Values, Required, Default table options with one unified list under Values
* Updated theme, adds a *Back To Top* button when you begin to scroll up, and adds many other small improvements
* Fixed wrong name for option of Banner capes in :doc:`capes <capes>`
* Fixed egg naming error in :doc:`custom colors <custom_colors>`
* Updated :doc:`CEM animations <cem_animation>`
* Updated :doc:`CEM entity names <cem_entity_names>`
* Updated to commit `b07063a4 <https://github.com/sp614x/optifine/commit/b07063a4173cef1865816e243706130acb94cb35>`_

2021 December 11
================

* Added more :doc:`debug keys <debug_keys>`
* Fixed various typos
* Adjust sidebar CSS

2021 November 20
================

* Expanded on :ref:`Paths <syntax:paths, file locations>`
* Added tutorials to :doc:`cem`
* Changed how the keyboard keys look
* Added :doc:`install`
* Added more examples to :ref:`NBT matching <syntax:components, nbt>`
* Fixed :ref:`custom_colors:Spawner egg colors`
* Fixed color on tooltip hovers

2021 November 2
===============

* Added the "10" anniversary capes to :doc:`capes`
* Moved Extra Cosmetics to :doc:`easter_eggs`
* Added more to :doc:`easter_eggs`

2021 October 31
===============

* Updated styling of notice at footer
* Added :doc:`extra_cosmetics <easter_eggs>`
* Updated :doc:`capes`

2021 September 20
=================

* Updated styling so TOC drawer doesn't overflow
* Removed copyright notice at footer
* Added :doc:`versioning`
* Changed tab style

2021 August 30
==============

* Updated to commit `431a82106b987229b2ab9f332dad20b560e67a5e <https://github.com/sp614x/optifine/commit/431a82106b987229b2ab9f332dad20b560e67a5e>`_

2021 August 29
==============

* Updated to commit `aedb5d527903882385b6953b887d0668860f447c <https://github.com/sp614x/optifine/commit/aedb5d527903882385b6953b887d0668860f447c>`_
* Updated to commit `9b8f72b07f1f3229584ffa404c7f86f6e6bcf459 <https://github.com/sp614x/optifine/commit/9b8f72b07f1f3229584ffa404c7f86f6e6bcf459>`_

2021 August 22
==============

* Updated :doc:`CIT <cit>`
* Compressed settings images
* Added styling to collapsible content
* Added more examples
* Changed some inline code to use GUI labels instead

2021 August 17
==============

* Split CEM into section files
* Split shaders into section files
* Add documentation changelog
* Change spaces to tabs
* Compress :doc:`HD fonts icon <hd_fonts>`
* Add icons for most shader files

2021 July 14
============

* Move shaders
* Add previewable links
* Expand :doc:`capes`

2021 July 7
===========

* Add :doc:`lagometer`
* Add :doc:`capes`
* Fix CSS
* Fix content overflowing on edges
* Add :doc:`CEM <cem>` is_ridden entity parameter

2021 July 3
===========

* Remove line numbers in code blocks
* Add better CSS
* More document icons
* More links & shortcuts
* Add internal template
* Use smaller image for icon and crisp-edge
* Fix Custom Animations glowsquid video

2021 July 2
===========

* Update wording
* Add tabs
* Expand and clarify sections
* Add file-location admonitions
* Add keyword shortcuts
* Add CEM limitations
* Remove first-person centric language

2021 June 13
============

* Manually fix OpenGraph meta extension
* Merge fixes for absolutes in embeds
* Fix description headline

2021 June 12
============

* Add icons to each document
* Add metadata for embeds
* Add lead text to embeds
* Add theme color to embeds

2021 May 31
===========

* Update to commit dbb3016
* Add :doc:`random_entities`
* :doc:`faq` updates

2021 May 27
===========

* Initial commit
