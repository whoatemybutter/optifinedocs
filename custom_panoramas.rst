:hero: That planet Earth turns slowly

Custom Panoramas
################

.. figure:: images/custom_panoramas/icon.webp
    :align: right
    :height: 180px

    The 1.17 main menu.

.. location::

    /assets/minecraft/optifine/gui/background.properties

**Custom Panoramas** control the behaviour of the main menu panorama.

.. danger:: This feature has been broken since 1.13 See `GH-2052 <https://github.com/sp614x/optifine/issues/2052>`_.

Alternative panorama folders
============================

.. note:: This is optional.

Alternative panorama folders can include a ``background.properties`` file to define custom properties for each panorama.

For example:

.. code::

    /assets/minecraft/optifine/gui/background1
        /panorama_0.png
        /panorama_1.png
        /panorama_2.png
        /panorama_3.png
        /panorama_4.png
        /panorama_5.png

Properties
==========

``weight``
----------

.. values::
    :values: Integer
    :required: No
    :default: ``1``

Weights of selections, in descending order; **higher** weights will be selected more often.

Blurs
-----

The main menu background uses **3** types of blur prior to 1.12.

.. warning:: Higher blur levels may decrease the main menu FPS.
.. danger:: This feature does not work past 1.12.

``blur1``
~~~~~~~~~

.. values::
    :values: Integer, ``1`` through ``64``
    :required: No
    :default: ``1``

``blur2``
~~~~~~~~~

.. values::
    :values: Integer, ``1`` through ``3``
    :required: No
    :default: ``1``

``blur3``
~~~~~~~~~

.. values::
    :values: Integer, ``1`` through ``3``
    :required: No
    :default: ``1``

Overlay colors
==============

.. note:: When the top and bottom colors are both ``0``, that overlay is disabled.

If enabled, two gradient overlays can be drawn on top of the background panorama.

The color format is **ARGB** (alpha [transparency], red, green, blue), in `hexadecimal <https://en.wikipedia.org/wiki/Hexadecimal>`__.

To read it, convert each interval of two values to decimal from hexadecimal (``AABBCCDD`` == ``0xAA``, ``0xBB``, ``0xCC``, ``0xDD`` == ``170``, ``187``, ``204``, ``221``).

``overlay1.top``
----------------

.. values::
    :values: AARRGGBB
    :required: No
    :default: ``80FFFFFF``

First overlay, top gradient color.

``overlay1.bottom``
-------------------

.. values::
    :values: AARRGGBB
    :required: No
    :default: ``00FFFFFF``

First overlay, bottom gradient color.

``overlay2.top``
----------------

.. values::
    :values: AARRGGBB
    :required: No
    :default: ``00000000``

Second overlay, top gradient color.

``overlay2.bottom``
-------------------

.. values::
    :values: AARRGGBB
    :required: No
    :default: ``80000000``

Second overlay, bottom gradient color.

JSON schema
===========

.. note:: Although this page is ``.properties`` based, it can be mapped to JSON.

.. literalinclude:: ./schemas/custom_panoramas.schema.json
