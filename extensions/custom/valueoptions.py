from docutils.parsers.rst import directives, Directive
from docutils import nodes
from docutils.statemachine import ViewList
from sphinx.util.nodes import nested_parse_with_titles


def yesno(question: str) -> bool:
    if question.lower() in ("true", "1", "yes"):
        return True
    return False


class OptionValuesDirective(Directive):

    required_arguments = 0
    optional_arguments = 0
    has_content = False
    option_spec = {
        "values": directives.unchanged_required,
        "required": directives.unchanged_required,
        "default": directives.unchanged,
    }

    def run(self):
        rst = ViewList()

        # Add the content one line at a time.
        # Second argument is the filename to report in any warnings
        # or errors, third argument is the line number.
        yn = yesno(self.options.get("required"))

        rst.append(f"| **Values:** {self.options.get('values')}", "fakefile.rst", 1)
        if yn:
            rst.append(f"| **Required**", "fakefile.rst", 2)
        else:
            rst.append(f"| *Optional*", "fakefile.rst", 2)
        if self.options.get("default", "*None*").lower() != "*none*":
            rst.append(
                f"| **Default:** {self.options.get('default')}", "fakefile.rst", 3
            )

        # Create a node.
        node = nodes.paragraph()
        node.document = self.state.document

        # Parse the rst.
        nested_parse_with_titles(self.state, rst, node)

        # And return the result.
        return node.children


def setup(app):

    app.add_directive("values", OptionValuesDirective)

    return {
        "version": "0.3",
        "parallel_read_safe": True,
        "parallel_write_safe": True,
    }
