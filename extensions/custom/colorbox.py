import html

from docutils.parsers.rst import Directive
from docutils import nodes


class ColorBoxDirective(Directive):

    required_arguments = 1

    def run(self):
        color_code = html.escape(self.arguments[0].upper(), quote=True)
        text_color = "#000" if self.calculate_luminance(color_code[1:]) > 150 else "#FFF"
        span = nodes.raw(
            "",
            f'<div style="max-width: fit-content; max-height: fit-content; padding: 0.1rem 0.5rem; background-color: {color_code};"><pre style="color: {text_color};">{color_code}</pre></div>',
            format="html"
        )
        return [span]

    @staticmethod
    def calculate_luminance(hex_color: str) -> int:
        r: int = int(hex_color[0:2], 16)
        g: int = int(hex_color[2:4], 16)
        b: int = int(hex_color[4:6], 16)

        return ((r << 1) + r + (b << 2) + b) >> 3

def setup(app):
    app.add_directive("color-box", ColorBoxDirective)
