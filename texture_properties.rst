Texture Properties
##################

.. figure:: images/texture_properties/icon.webp
    :align: right
    :height: 180px
    :class: pixel-edges

    PBR for pillow fluff.

.. location::

    /assets/minecraft/optifine/texture.properties

**Texture Properties** define how a texture should be interpreted.

.. seealso:: See :doc:`shaders_dev`: :ref:`shaders_dev/preprocessor:macros`, and :doc:`shaders_dev/textures`.
.. seealso:: See `<https://shaderlabs.org/wiki/LabPBR_Material_Standard>`_ for information on what labPBR is.

Properties
==========

``format``
----------

.. values::
    :values: ``lab-pbr`` or ``1.3``
    :required: No

The texture format used for normal and specular shader textures.

JSON schema
===========

.. note:: Although this page is ``.properties`` based, it can be mapped to JSON.

.. literalinclude:: ./schemas/texture_properties.schema.json
