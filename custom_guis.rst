:hero: Dark mode for brewing stands

Custom GUIs
###########

.. figure:: images/custom_guis/icon.webp
    :align: right
    :height: 180px
    :class: pixel-edges

    Custom Shulker box GUI

.. location::

    /assets/minecraft/optifine/gui/container/\*.properties

**Custom GUIs** can define a texture for each GUI, and apply them based on different criteria, such as the entity, biome, height, and more.

For each container GUI texture to override, create a ``.properties`` file in the ``/assets/minecraft/optifine/gui/container`` folder of the resource pack.
Properties files can be organized into subfolders of any depth, as long as everything is within the top-level ``/assets/minecraft/optifine/gui/container`` folder.

.. figure:: images/custom_guis/settings.webp

    :menuselection:`Video Settings --> Quality`

.. important:: Different container types have different requirements and restrictions.

General properties
==================

``container``
-------------

.. values::
    :values: ``anvil``, ``beacon``, ``brewing_stand``, ``chest``, ``crafting``, ``dispenser``, ``enchantment``, ``furnace``, ``hopper``, ``horse``, ``villager``, ``shulker_box``, ``creative``, or ``inventory``
    :required: Yes
    :default: None

Type of container GUI to apply to.

* ``creative`` refers to the creative inventory with the tabs.
* ``inventory`` refers to the normal survival inventory, with the player in a window.

``texture``, ``texture.PATH``
-------------------------------

.. values::
     :values: |file path|
     :required: Yes
     :default: None

The replacing texture for the GUI.

The ``texture`` property replaces the default GUI texture.
The ``texture.PATH`` property can be used to replace any GUI texture; ``PATH`` is relative to ``/assets/minecraft/textures/gui``.

.. important:: The creative inventory GUI does not have a default texture, so it must use PATH textures.

Example for creative inventory:

.. code-block:: properties
    :caption: /assets/minecraft/optifine/gui/container/creative/creative_desert.properties

    container=creative
    biomes=desert
    texture.container/creative_inventory/tab_inventory=tab_inventory_desert
    texture.container/creative_inventory/tabs=tabs_desert
    texture.container/creative_inventory/tab_items=tab_items_desert
    texture.container/creative_inventory/tab_item_search=tab_item_search_desert

.. important:: At least one ``texture`` or ``texture.PATH`` is required.

``name``
--------

.. values::
    :values: |drv|
    :required: No
    :default: None

Custom entity or block entity name.

For example: Beacon, Brewing Stand, Enchanting Table, Furnace, Dispenser, Dropper, Hopper, and Command Blocks.

This will apply the replacement GUI only when the container matches this rule.


``biomes``
----------

.. values::
    :values: Space-split list of `biomes <https://minecraft.wiki/w/Biome#Biome_IDs>`_
    :required: No
    :default: None

Biomes where this replacement applies.

Biomes added by mods can also be used with the same syntax.

``heights``
-----------

.. values::
    :values: Integer, or range of integers
    :required: No
    :default: None

Heights where this replacement applies.

Since 1.18, negative values may be specified for height.
When used in a range they must be put in parenthesis: ``(-3)-64``.

Specific properties
===================

These additional properties do not need any separate files.
They are just properties that apply only when ``container`` equals their required value.

Chests
------

.. note:: Implies ``container=chest``.

``large``
~~~~~~~~~

.. values::
    :values: Boolean
    :required: No

Whether to use the replacement GUI on a large (double) chest.

``trapped``
~~~~~~~~~~~

.. values::
    :values: Boolean
    :required: No

Whether to use the replacement GUI on a trapped chest.

``christmas``
~~~~~~~~~~~~~

.. values::
    :values: Boolean
    :required: No

Whether to use the replacement GUI on any `Christmas chest <https://minecraft.wiki/w/Chest#Christmas_chest>`_.
Christmas chests appear from December 24 to 26 of any year.

``ender``
~~~~~~~~~

.. values::
    :values: Boolean
    :required: No

Whether to use the replacement GUI on an `Ender Chest <https://minecraft.wiki/w/Ender_Chest>`_.

Beacons
-------

.. note:: Implies ``container=beacon``.

``levels``
~~~~~~~~~~

.. values::
    :values: Integer, or range of integers.
    :required: No
    :default: None

What levels of beacon power to apply the replacement to; how many bases of blocks.

.. figure:: images/custom_guis/beacon_pyramid.webp

    The levels from left to right: 4, 3, 2, 1.

Villagers
---------

.. note:: Implies ``container=villager``.

``professions``
~~~~~~~~~~~~~~~

.. values::
    :values: ``none``, ``armorer``, ``butcher``, ``cartographer``, ``cleric``, ``farmer``, ``fisherman``, ``fletcher``, ``leatherworker``, ``librarian``, ``mason``, ``nitwit``, ``shepherd``, ``toolsmith``, or ``weaponsmith``, along with an optional level experience format
    :required: No

List of villager professions with an optional level specifier.

Entry format: ``<profession>[:level1,level2,...]``

Examples:

* Professions farmer (all levels) or librarian (levels 1,3,4): ``professions=farmer librarian:1,3-4``
* Professions fisher, shepard, nitwit: ``professions=fisherman shepherd nitwit``


Horse
-----

.. note:: Implies ``container=horse``.

``variants``
~~~~~~~~~~~~

.. values::
    :values: ``horse``, ``donkey``, ``mule``, ``llama``
    :required: No

What specific horse type to apply replacement to.

Dispenser, dropper
------------------

.. note:: Implies ``container=dispenser``. Dropper applies as well.

``variants``
~~~~~~~~~~~~

.. values::
    :values: ``dispenser`` or ``dropper``
    :required: No
    :default: ``dispenser``

What specific block to apply the replacement GUI to.

Llama, shulker box
------------------

.. note:: Implies ``container=shulker_box`` or ``container=horse``.

.. important:: Despite the container being ``horse``, this property will apply to Llamas instead.

``colors``
~~~~~~~~~~

.. values::
    :values: ``white``, ``orange``, ``magenta``, ``light_blue``, ``yellow``, ``lime``, ``pink``, ``gray``, ``light_gray``, ``cyan``, ``purple``, ``blue``, ``brown``, ``green``, ``red``, ``black``
    :required: No

Shulker box color or llama carpet color to apply the replacement texture to.

JSON schema
===========

.. note:: Although this page is ``.properties`` based, it can be mapped to JSON.

.. literalinclude:: ./schemas/custom_guis.schema.json
