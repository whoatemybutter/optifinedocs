:hero: Twist and shout

Animation
#########

.. figure:: images/cem/icon_animation.webp
    :align: right
    :height: 180px

    He has the bends.

This is the reference configuration for animating OptiFine's **Custom Entity Models** (**CEMA**).

.. important::

    These apply inside of :doc:`cem_models` and this document is kept separate for organizational purposes **only**.
    It is **not** a separate file.

Each model variable which is to be animated is assigned a mathematical expression.
The expression is evaluated every time the model is rendered and its value is assigned to the variable.
This value controls the positions, scales, rotations, etc. of the parts of the model.

.. important:: Animations **must be in the parent bone**, *not* any sub-bone.

The variables and expressions are defined in the "animations" section of the JSON entity model **(JEM)**:
For example, in the object ``{"a": 5}``, "a" is the **key** and 5 is the **value**.

.. code:: javascript

    "animations": [
        {
            "variable1": "expression1",
            "variable2": "expression2"
        }
    ]

Essentially, we are writing equations in JSON:
``head.rx = swing_progress * 3`` will be transformed into a key-value pair of ``"head.rx": "swing_progress * 3"``.
In this, ``head.rx`` is the **destination** or *key*, and ``swing_process * 3`` is the **source** or *value* or *expression*.

.. note:: Some headers use ``{a, b, c}``. This means it may be any one of these options (e.x. ``pos_{x, y, z}`` means ``pos_x`` or ``pos_y`` or ``pos_z``).

Key: destination
================

The keys of the objects in the "animations" list are the targets for their value's expression.
They are strings that refer to different values that control an entity.

Model, arbitrary, and render variables are all valid options.

Model variables
---------------

**Model variables** are specified in the format ``MODEL_TARGET.MODEL_VARIABLE``. They refer to specific parts of the model.

Model target names
~~~~~~~~~~~~~~~~~~

The first model found by part name or ID is used if there are duplicates.

The hierarchical specification allows model groups (JSON part models) to be reused for different parts.
For example, one hand model (``shoulder:upper_arm:elbow:forearm:palm:finger[1..5]``) can be used for both left and right hands;
``left_hand:finger1`` can be for the left thumb and ``right_hand:finger1`` for the right thumb.

.. csv-table::
    :header: "Name", "Description"

    "``this``", "The current custom model."
    "``part``", "The original part model to which the custom model is attached."
    "<part name>", "Placeholder; a part name."
    "<id>", "Placeholder; the custom model by ID."
    "<part name>:<id>:...", "Placeholders; hierarchical query for nested parts by their names and sub-IDs. There may be any number of ``<id>``. The intermediate parents in the hierarchical specification can be skipped. The first deep nested match is used."
    "<id>:<id>:...", "Placeholders; hierarchical query for nested parts by their ID and sub-IDs. There may be any number of later ``<id>``. The intermediate parents in the hierarchical specification can be skipped. The first deep nested match is used."

Model variable names
~~~~~~~~~~~~~~~~~~~~

.. js:data:: tx, ty, tz

    **Translation** in X, Y, Z coordinates (movement).

.. js:data:: sx, sy, sz

    **Scale** in X, Y, Z coordinates (size).

.. js:data:: rx, ry, rz

    **Rotation** in X, Y, Z coordinates (spin, yaw, pitch).

.. js:data:: visible

    Whether to show model and submodels; boolean.

.. js:data:: visible_boxes

    Whether to show this model box only and not affect submodels; boolean.

Arbitrary variables
-------------------

.. warning:: Arbitrary variables are not supported for `block entities <https://minecraft.wiki/w/Block_entity>`_.

**Arbitrary variables** are user-defined variables that contain a formula that is calculated every frame.
These variables are associated with the rendered entity in-memory.
They are not "stored" with the entity in the game itself; unloading and reloading the world will reset them.

They may be useful for storing animation data between frames or storing constants for animation configuration.

The placeholder ``NAME`` may be any valid string.

.. js:data:: var.NAME

    Arbitrary variable name that stores floats.
    Default value is ``0``.

    For instance, the first calculation of ``"var.xyz": "var.xyz + 1"`` is ``1`` (``var.xyz = 0; var.xyz + 1 => 0 + 1 = 1``).

.. js:data:: varb.NAME

    Arbitrary variable name that stores booleans.
    Default value is ``false``.

Render variables
----------------

.. js:data:: render.shadow_size

    The size of the entity's shadow; this is a float from ``1.0`` (opaque) to ``0.0`` (invisible).

.. js:data:: render.shadow_opacity

    The opacity (solidness) of the entity's shadow.

.. js:data:: render.leash_offset_{x, y, z}

    When leashed, where the leash on the entity attachs to.

.. js:data:: render.shadow_offset_{x, z}

    An offset of the entity's shadow position.

Values: source
==============

The values of the objects in the "animations" list are expressions that are evaluated and stored in their corresponding keys.
They are strings of general mathematical expressions with brackets, constants, variables, operators, parameters, and functions.

For example, ``sin(35) + max(5, 50, 500)`` evaluates to ``500.5735764``.

.. hint::

    They most closely resemble the `AsciiMath <https://en.wikipedia.org/wiki/AsciiMath>`_ format, although they are not identical.

Constants
---------

**Constants** never change and always evaluate to the same value.

.. note:: A literal (such as ``5``) is also a constant, but has no *special* meaning apart from its value.

.. js:data:: pi

    Value of `pi <https://en.wikipedia.org/wiki/Pi>`__; ``3.1415926``.

.. js:data:: true

    True value.

.. js:data:: false

    False value.

Variables
---------

**Variables** are globally accessible names that can change.

.. notconfused::

    :ref:`cem_animation:keys`; these are to be used in expressions.

.. js:data:: MODEL.VAR

    Another model variable's value, see the :ref:`cem_animation:Model variables` section.

    .. important:: ``MODEL`` and ``VAR`` are placeholders.

.. js:data:: age

    How long the entity has existed in the world for the client, in ticks.

.. js:data:: anger_time

    The time the entity has been angry. 0 while neutral, 720 while aggressive, counts down to 0 when the target is lost.

.. js:data:: day_count

    How many days have passed in this world.

.. js:data:: day_time

    The current day time (0 to 24000).

.. js:data:: death_time

    Time stage on entity's death. Counts up from 0 to 20.

.. js:data:: dimension

    Dimension ID.

    .. csv-table::
        :header: "ID", "Dimension"

        "1", "The End"
        "0", "Overworld"
        "-1", "Nether"

.. js:data:: frame_counter

    Index of the current frame, wraps around at 10 minutes (0 to 27719).

.. js:data:: frame_time

    Time in seconds since the last frame.

.. js:data:: head_yaw

    Head yaw; X rotation.

.. js:data:: head_pitch

    Head pitch; Y rotation.

.. js:data:: health

    Current health.

.. js:data:: hurt_time

    Time stage of when entity has been hurt once. Counts down from 10 to 0.

.. js:data:: id

    A unique numeric identifier.

.. js:data:: is_aggressive

    If the entity is aggressive towards another entity.

.. js:data:: is_alive

    If the entity is alive; not dead.

.. js:data:: is_burning

    If the entity is burning.

.. js:data:: is_child

    If the entity is a child.

.. js:data:: is_glowing

    If the entity has the `Glowing <https://minecraft.wiki/w/Glowing>`_ status effect.

.. js:data:: is_hurt

    If the entity is taking damage.

.. js:data:: is_in_hand

    Item: if the entity is being held in the player's hand.

.. js:data:: is_in_item_frame

    Item: if the entity is in an item frame.
.. js:data:: is_in_ground

    Arrow, trident: if the entity is embedded into a block.

.. js:data:: is_in_gui

    If the entity is inside the GUI

.. js:data:: is_in_lava

    If the entity is touching lava.

.. js:data:: is_in_water

    If the entity is touching water.

.. js:data:: is_invisible

    If the entity has the `Invisibility <https://minecraft.wiki/w/Invisibility>`_ status effect.

.. js:data:: is_on_ground

    If the entity is on the ground; not flying.

.. js:data:: is_on_head

    Item: if the entity is on an armor head slot.
.. js:data:: is_on_shoulder

    Parrot: if the entity is on a player's shoulder.
.. js:data:: is_ridden

    If the entity is being ridden by another entity.

.. js:data:: is_riding

    If the entity is riding atop of another entity.

.. js:data:: is_sitting

    Cat, wolf, parrot: if the entity is sitting.
.. js:data:: is_sneaking

    Cat, ocelot: if the entity crouching.

.. js:data:: is_sprinting

    Cat, ocelot: if the entity is sprinting.

.. js:data:: is_tamed

    Cat, wolf, parrot: if the entity is tamed.
.. js:data:: is_wet

    If the entity is under rain or is touching water.

.. js:data:: limb_swing

    Counts up in ticks from 0 as the entity continues to move.

.. js:data:: limb_speed

    The current speed of the entity's limbs. Ranges from ``0.0`` (still) to ``1.0`` (sprinting).

.. js:data:: max_health

    Entity's maximum health.

.. js:data:: player_pos_{x, y, z}

    Player's X, Y, Z position (not necessarily the same entity).
.. js:data:: player_rot_{x, y}

    Player's `yaw <https://en.wikipedia.org/wiki/Yaw_(rotation)>`_ (left-right) and `pitch <https://en.wikipedia.org/wiki/Aircraft_principal_axes#Transverse_axis_(pitch)>`_ (up-down), respectively.
.. js:data:: pos_{x, y, z}

    Entity's X, Y, Z position.

.. js:data:: rot_{x, y}

    Entity's `yaw <https://en.wikipedia.org/wiki/Yaw_(rotation)>`_ (left-right) and `pitch <https://en.wikipedia.org/wiki/Aircraft_principal_axes#Transverse_axis_(pitch)>`_ (up-down), respectively.
.. js:data:: rule_index

    The index of the current matching :doc:`random models <random_entities>` rule. Defaults to 0.

.. js:data:: swing_progress

    How far through the attack animation the entity is; counts up from 0.0 to 1.0.

.. js:data:: time

    The total game time in integer ticks (0 to 27720); not related to the daylight cycle.

Operators
---------

Operators are symbols that operate on numbers. They are infix functions. They are ``+ - * / % ! && || < <= > >= == !=``.

Unfamiliar operators:

* ``%`` `Modulo <https://en.wikipedia.org/wiki/Modulo>`_
* ``!`` Negate
* ``&&`` Logical AND
* ``||`` Logical OR
* ``==`` Equality
* ``!=`` Inequality

Functions
---------

.. py:function:: sin(degrees: number) -> number

    `Sine <https://en.wikipedia.org/wiki/Sine>`__ of ``degrees``.

    .. code:: python

        >>> sin(35)
        0.5735764364...

.. py:function:: cos(degrees: number) -> number

    `Cosine <https://en.wikipedia.org/wiki/Cosine>`__ of ``degrees``.

    .. code:: python

        >>> cos(100)
        −0.1736481777...

.. py:function:: tan(degrees: number) -> number

    `Tangent <https://en.wikipedia.org/wiki/Tangent_(trigonometry)>`__ of ``degrees``.

    .. code:: python

        >>> tan(50)
        1.191753595...

.. py:function:: asin(x: number) -> number

    `Arcsine <https://en.wikipedia.org/wiki/Arcsine>`__ of ``x``.

    .. code:: python

        >>> asin(0.5735)
        34.99465382...

.. py:function:: acos(x: number) -> number

    `Arccosine <https://en.wikipedia.org/wiki/Arccosine>`__ of ``x``.

    .. code:: python

        >>> acos(-0.1736)
        99.99719705...

.. py:function:: atan(x: number) -> number

    `Arctangent <https://en.wikipedia.org/wiki/Arctangent>`__ of ``x``.

    .. code:: python

        >>> atan(1.1917)
        49.99873126...

.. py:function:: atan2(y: number, x: number) -> number

    Two-argument `arctangent <https://en.wikipedia.org/wiki/Atan2>`_ of ``y`` and ``x``.

    .. code:: python

        >>> atan2(1, 3)
        0.3217505544...

.. py:function:: torad(degrees: number) -> number

    Convert `degrees <https://en.wikipedia.org/wiki/Degree_(angle)>`__ to `radians <https://en.wikipedia.org/wiki/Radian>`__.

    .. code:: python

        >>> torad(90)
        1.570796327...

.. py:function:: todeg(radians: number) -> number

    Convert `radians <https://en.wikipedia.org/wiki/Radian>`__ to `degrees <https://en.wikipedia.org/wiki/Degree_(angle)>`__.

    .. code:: python

        >>> todeg(1.5707)
        89.99448088...

.. py:function:: min(*items: number) -> number

    The minimum of all given parameters.

    .. code:: python

        >>> min(1, 4, 3, -5, -100)
        -100

.. py:function:: max(*items: number) -> number

    The maximum of all given parameters.

    .. code:: python

        >>> max(1, 4, 3, -5, -100)
        4

.. py:function:: clamp(x: number, min: number, max: number) -> number

    The clamped value of ``x``, guaranteed to be between ``min`` and ``max`` values:

    * If ``x > max``, return ``x = max``
    * If ``x < min``, return ``x = min``

    .. code:: python

        >>> clamp(6, 1, 5)
        5
        >>> clamp(4, -8, 10)
        4

.. py:function:: abs(x: number) -> number

    Absolute value of ``x``.

    * If ``x < 0``, return ``-x``
    * If ``x >= 0``, return ```x```

    .. code:: python

        >>> abs(-25.3)
        25.3

.. py:function:: floor(x: number) -> int

    The `floor <https://en.wikipedia.org/wiki/Floor_(function)>`_ of ``x``.

    .. code:: python

        >>> floor(2.4)
        2
        >>> floor(-2.4)
        -3

.. py:function:: ceil(x: number) -> int

    The `ceiling <https://en.wikipedia.org/wiki/Ceiling_(function)>`_ of ``x``.

    .. code:: python

        >>> ceil(2.1)
        3
        >>> ceil(-2.1)
        2

.. py:function:: exp(x: number) -> number

    Euler's constant (*e*) raised to the power of ``x``; :math:`e^x`

    .. code:: python

        >>> exp(4)
        54.59815003...

.. py:function:: frac(x: number) -> number

    The fractional part (decimal) of ``x``.

    .. code:: python

        >>> frac(3.25)
        0.25
        >>> frac(-9.99992)
        -0.99992

.. py:function:: log(x: number) -> number

    The `logarithm <https://en.wikipedia.org/wiki/Logarithm>`_ of ``x``, base 10.

    .. code:: python

        >>> log(50)
        3.912023005428146...

.. py:function:: pow(base: number, power: number) -> number

    Raise ``base`` to the power ``power``; :math:`\mathit{base}^{\mathit{power}}`

    .. code:: python

        >>> pow(4, 5)
        1024
        >>> pow(-53, 3)
        -148877

.. py:function:: random(seed: Optional[number]) -> number

    A random number between 0.0 and 1.0. Parameter ``seed`` is optional and if given, randomizer is seeded with this value.
    .. code:: python

        >>> random()
        0.53235...

.. py:function:: round(x: number) -> int

    `Rounding <https://en.wikipedia.org/wiki/Rounding>`__ of ``x``.

    .. code:: python

        >>> round(5.4)
        5

.. py:function:: signum(x: number) -> int

    The `sign <https://en.wikipedia.org/wiki/Sign_function>`_ of ``x``.

    .. code:: python

        >>> signum(0)
        0
        >>> signum(-5253)
        -1
        >>> signum(5)
        1

.. py:function:: sqrt(x: number) -> number

    The `square root <https://en.wikipedia.org/wiki/Square_root>`__ of positive ``x``.

    .. code:: python

        >>> sqrt(25)
        5
        >>> sqrt(27.5)
        5.244044241...

.. py:function:: fmod(x: number, y: number) -> number

    Java's `floorMod function <https://docs.oracle.com/en/java/javase/23/docs/api/java.base/java/lang/Math.html#floorMod(int,int)>`__ of ``x`` and ``y``.
    See linked Javadoc.

    >>> fmod(4, 3)
    1
    >>> fmod(4, -3)
    -2
    >>> fmod(-4, -3)
    -1

.. py:function:: lerp(k: number, x: number, y: number) -> number

    The `linear interpolation <https://en.wikipedia.org/wiki/Linear_interpolation>`__ of ``k`` between the interval [``x``, ``y``]:
    :math:`((1 - k) \times x) + (k \times y)``
.. py:function:: if(cond: predicate, val: number, **conds: predicate, val_else: number) -> number

    Select a value based on one of more conditions (predicates):

    * Return ``val`` if ``cond`` is true, else...
    * Return ``val2`` if ``cond2`` is true, else...
    * *exhaust all condition pairs*, else...
    * Return ``val_else`` if all of ``cond`` are false

    Additional isolated conditions may be specified as additional two-pair of ``predicate, value``.
    The first condition pair that is true is returned.
    If no pairs are true, ``val_else`` is returned.

    A **predicate** is an expression that evaluates to a boolean (true or false). For example, ``limb_speed > 0.7``.

    .. code:: python

        >>> if(limb_speed > 0.7, 5, 0)
        5
        >>> if(true, 9, false, 3, 0)
        9
        >>> if(false, 0, true, 1, 2)
        1
        >>> if(false, 0, false, 1, false, 2, 3)
        3

.. py:function:: ifb(cond: predicate, val: boolean, **conds: predicate, val_else: boolean) -> boolean

    Select a *boolean* value based on one of more conditions (predicates):

    * Return ``val`` if ``cond`` is true, else...
    * Return ``val2`` if ``cond2`` is true, else...
    * *exhaust all condition pairs*, else...
    * Return ``val_else`` if all of ``cond`` are false

    Additional isolated conditions may be specified as additional two-pair of ``predicate, value``.
    The first condition pair that is true is returned.
    If no pairs are true, ``val_else`` is returned.

    A **predicate** is an expression that evaluates to a boolean (true or false). For example, ``limb_speed > 0.7``.
    .. code-block:: python

        >>> ifb(limb_speed > 0.7, true, false)
        true
        >>> ifb(true, false, true)
        false
        >>> ifb(1 > 0, false, true)
        false
        >>> ifb(0 == -1, false, 1 == -1, true, -1 == -1, true, false)
        true

.. py:function:: print(id: number, n: int, x: number) -> None:

    Print number ``x`` to the output log every ``n`` -th frame, qualified with ``id``.
.. py:function:: printb(id: int, n: int, x: boolean) -> None:

    Print boolean ``x`` to the output log every ``n`` -th frame, qualified with ``id``.
.. py:function:: between(x: number, min: number, max: number) -> boolean

    Is ``x`` between ``min`` and ``max``; :math:`\mathit{min} <= x <= \mathit{max}`

    .. code:: python

        >>> between(4, 0, 10)
        true
        >>> between(5, 1, 3)
        false

.. py:function:: equals(x: number, y: number, margin: number) -> boolean

    Is the difference of ``x`` and ``y`` within error margin ``margin``; :math:`|x - y| < \mathit{margin}`

    .. code:: python

        >>> equals(3, 6, 4)
        true
        >>> equals(0.05, 10, 10)
        true
        >>> equals(0.83, 0.06, 0.2)
        false

.. py:function:: in(x: number, *val: number) -> boolean

    Is ``x`` equal to any of ``val``?

    .. code:: python

        >>> in(1, 2, 3, 4, 1)
        true
        >>> in(7, 6, 4, 0)
        false

Examples
========

Basic structure
---------------

.. code:: javascript

    {
        "animations": [
            {
                "this.rx": "clamp(-0.5 * part.rx, 0, 90)",
                "this.tx": "3 * sin(limb_swing / 4) - 2",
                "this:Hoof.rx": "if(leg4:Hoof.rx > 90, leg4:Hoof.rx - 90, 0)"
            }
        ]
    }

Walking animation
-----------------

```x``` is a multipler to control how fast the leg swings back and forth,
and ``y`` is a multiplier to control how far it swings back and forth.

.. code:: javascript

    "left_leg.rx": "sin(limb_swing * x) * limb_speed * y"

Attack animation
----------------

```x``` is a multipler for how much it rotates.

.. code:: javascript

    "head.rx": "sin(swing_progress * pi) * x"

Hurt animation
--------------

```x``` is a multipler for how much it rotates.

.. code:: javascript

    "head.rx": "-sin(hurt_time / pi) * x"

Custom counter
--------------

This is a counter that will count up while an entity is in water, and count down again when it leaves.

.. code::

    "var.counter": "if(is_in_water, min(20, var.counter + 0.1 * frame_time * 20), max(0, var.counter - 0.1 * frame_time * 20))"

If statements
-------------

The leg will rotate by 45 degrees when the entity is not on the ground, otherwise it will stay at 0 deg.

.. code:: javascript

    "left_leg.rx": "if(!is_on_ground, torad(45), 0)"

The body will tilt forwards once the entity hits a certain movement speed.

.. code:: javascript

    "body.rx": "if(limb_speed > 0.7, torad(20), 0)"

External links
==============

.. warning:: |elwarn|

* `Wynem's CEM documentation <https://wynem.com/cemanimation/?tab=documentation>`_

JSON schema
===========

.. literalinclude:: ./schemas/cem_anim.schema.json
