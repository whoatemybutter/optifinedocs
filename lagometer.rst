:hero: Framerate roller coaster

Lagometer
#########

.. figure:: images/lagometer/icon.webp
    :width: 180px
    :align: right
    :class: pixel-edges

    ⬜, 🟩, 🟪, 🟨, 🟥

The **lagometer** displays a graph of the resources used when rendering each frame.
It is visible only when the debug screen (:keys:`F3`) is shown.

It replaces the `Vanilla frame time graph <https://minecraft.wiki/w/Debug_screen#Frame_time_graph>`_.

.. figure:: images/lagometer/settings.webp

    :menuselection:`Video Settings --> Other`

.. figure:: images/lagometer/fullmeter.webp

    A usual meter with mostly white

.. list-table::
    :header-rows: 1
    :widths: 1, 20, 90

    * - Color
      -
      - Meaning

    * - .. image:: images/lagometer/orange.webp
            :width: 60px
            :class: pixel-edges
      - Orange
      - Memory garbage collection
    * - .. image:: images/lagometer/cyan.webp
            :width: 60px
            :class: pixel-edges
      - Cyan
      - Tick
    * - .. image:: images/lagometer/blue.webp
            :width: 60px
            :class: pixel-edges
      - Blue
      - Scheduled executables
    * - .. image:: images/lagometer/purple.webp
            :width: 60px
            :class: pixel-edges
      - Purple
      - Chunk upload
    * - .. image:: images/lagometer/red.webp
            :width: 60px
            :class: pixel-edges
      - Red
      - Chunk updates
    * - .. image:: images/lagometer/yellow.webp
            :width: 60px
            :class: pixel-edges
      - Yellow
      - Visibility check
    * - .. image:: images/lagometer/green.webp
            :width: 60px
            :class: pixel-edges
      - Green
      - Render terrain
    * - .. image:: images/lagometer/white.webp
            :width: 60px
            :class: pixel-edges
      - White
      - Anything not in above categories
