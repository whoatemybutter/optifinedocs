:hero: Cha-cha real smooth

Custom Animations
#################

.. location::

    /assets/minecraft/optifine/anim/\*\*/\*.properties

**Custom Animations** allows all textures to be animated, regardless of type or purpose. This includes even textures specific to other OptiFine features such as :doc:`random mob skins <random_entities>` or :doc:`skyboxes <custom_sky>`.

Although Vanilla Minecraft allows many textures to be animated, OptiFine extends and adds onto this capability by allowing portions of textures to be animated as well.

OptiFine's animation format differs from Vanilla's mcmeta format.

.. important:: For whole block and item textures, including :doc:`CTM <ctm>` and :doc:`CIT <cit>` replacements, continue using Mojang's ``mcmeta`` method instead.

To build an animation, first choose a texture and determine the X and Y coordinates, and width and height of the area to animate. Create the animation as a vertical strip of frames. The **width** should be the same as the width of the area to animate. The **height** should be a multiple of the animation area height.

Multiple animation sources can be applied to the same destination at different positions.

Emissive animation is also possible, see :doc:`emissive_textures`.

Properties
==========

.. note:: ``duration``, ``interpolate``, ``skip``, ``tile``, ``duration`` are optional, rest are **required**

``from``
--------

.. values::
    :values: |file path|
    :required: Yes

Path to source texture of the animation to display.

``to``
------

.. values::
    :values: |file path|
    :required: Yes

Path to destination texture to replace and animate.

``x``, ``y``
------------

.. values::
    :values: Positive integers
    :required: Yes

Coordinates of top-left corner of the **destination** texture to animate to.
Normally, this is 0, 0.

``w``, ``h``
------------

.. values::
    :values: Positive integer
    :required: Yes

Width and height of an individual animation frame.

``duration``
------------

.. values::
    :values: Positive integer
    :required: No
    :default: *None*

Duration of each individual frame, in ticks.
For reference, there are 20 ticks in 1 second.

``interpolate``
---------------

.. values::
    :values: Boolean
    :required: No
    :default: *None*

Whether to `interpolate <https://en.wikipedia.org/wiki/Interpolation_(computer_graphics)>`_ between each animated frame.

.. raw:: html

    <video width=30% controls muted loop playsinline>
        <source src="_static/furnace.webm" type="video/webm">
    Your browser does not support the video tag.
    </video>

This furnace has an interpolated animation; focus on the fire inside.

``skip``
--------

.. values::
    :values: Positive integer
    :required: No
    :default: *None*

What frame number to skip/ignore during animation.

.. important:: Frame numbers start at 0, not 1.

``tile.N``
----------

.. values::
    :values: Positive integer, ``N`` is positive integer
    :required: No
    :default: *None*

What frame number (starting from 0) to display at the ``N``-th tick.
N can be greater than the number of frames.

``duration.N``
--------------

.. values::
    :values: Positive integer, ``N`` is positive integer
    :required: No
    :default: *None*

Duration in ticks to display tile ``N`` for.
This only applies to tiles for which a ``tile.N`` is declared.

Example
=======

Rainbow squid
-------------

.. code:: properties

    from=./glow_squid_glow.png
    to=textures/entity/squid/glow_squid.png
    x=0
    y=0
    w=64
    h=32
    duration=1
    interpolate=true
    skip=2

.. note:: See the :doc:`syntax` document for how to specify paths to texture files.

This creates an interpolating animation that plays each frame in order from top to bottom once for one tick *(1/20th second)* each and then loops infinitely.

.. raw:: html

    <video width=30% controls muted playsinline>
        <source src="_static/glowsquid.mp4" type="video/mp4">
    Your browser does not support the video tag.
    </video>


Frame order and timing
======================

Each custom animation may specify its animation speed and frame order.
In the properties file, add a series of entries:

.. code:: properties

    tile.X=Y
    duration.X=Z

``X`` starts at 0 and represents the order of animation frames based on tick.
``Y`` is the tile number in the animation frames, the first tile being 0, the second 1, etc.
``Z`` is the duration that frame should be displayed, in game ticks *(1 tick = 1/20 second)*.

If omitted, duration is assumed to be the default frame duration, or ``1`` if not configured.

For example, suppose the animation file is 16px x 48px, at 3 frames.
To make it run on a 5-frame cycle with a pause in the middle, the properties file might look like this:

.. code:: properties

    tile.0=0
    tile.1=1
    tile.2=2
    duration.2=5
    tile.3=1
    tile.4=0

The animation happens in this order:

1. Frame 0: Display animation tile 0 for 1 tick (default duration).
2. Frame 1: Display animation tile 1 for 1 tick (default duration).
3. Frame 2: Display animation tile 2 for 5 ticks (duration=5).
4. Frame 3: Display animation tile 1 for 1 tick (default duration).
5. Frame 4: Display animation tile 0 for 1 tick (default duration).
6. Go back to frame 0.

Total: 5 frames over 9 ticks.

JSON schema
===========

.. note:: Although this page is ``.properties`` based, it can be mapped to JSON.

.. literalinclude:: ./schemas/custom_animations.schema.json
