Pre-requirements
################

Before installing OptiFine, you may need to install some things first to ensure the installer runs properly.

Java runtime
------------

You may need to install a Java runtime (JRE) above version 8.
It is not recommended to use Oracle's Java. Instead, use Adoptium's *(formerly AdoptOpenJDK)* Java runtime.

You can choose to install any JRE; all work with the OptiFine installer.

.. md-tab-set::

    .. md-tab-item:: :si-icon:`fontawesome/brands/windows` Windows

        1. Go to `<https://adoptium.net/temurin/releases/>`_
        2. Under the *Operating System* filter box, select **Windows**.
        3. Under the *Package Type* filter box, select **JRE**. You do not need the JDK.
        4. Under the *Architecture* filter box, select **x64**.
        5. Under the *Version* filter box, select any number; they are all >= 8.
        6. Click the blue button labeled ``.msi``.
        7. Execute the downloaded MSI file.

    .. md-tab-item:: :si-icon:`fontawesome/brands/linux` Linux

        1. Go to `<https://adoptium.net/temurin/releases/>`_
        2. Under the *Operating System* filter box, select **Linux**.
        3. Under the *Package Type* filter box, select **JRE**.
        4. Under the *Architecture* filter box, select **x64**.
        5. Under the *Version* filter box, select any number; they are all >= 8.
        6. Click the blue button labeled ``.tar.gz``.
        7. Extract the archive to a location you can execute, or to your PATH.

    .. md-tab-item:: :si-icon:`fontawesome/brands/apple` Mac

        1. Go to `<https://adoptium.net/temurin/releases/>`_
        2. Under the *Operating System* filter box, select **macOS**.
        3. Under the *Package Type* filter box, select **JRE**.
        4. Under the *Architecture* filter box, select **x64**.
        5. Under the *Version* filter box, select any number; they are all >= 8.
        6. Click the blue button labeled ``.pkg``.
        7. Execute the downloaded PKG file.

Jarfix
------

.. image:: images/install/jarfix.webp
    :align: right
    :width: 128px

.. figure:: images/install/jarfox.webp
    :align: right
    :width: 128px

    A jarfox

.. important:: This is only for Windows. This issue does not occur on Linux or Mac.

If you have the Java runtime installed but can't open ``.jar`` files with it, you may need to install Jarfix.
Jarfix is an application that fixes the file association on Windows, so that JAR files will be executed as Java programs.

Download it from `<https://johann.loefflmann.net/en/software/jarfix/index.html>`_ and run it.

