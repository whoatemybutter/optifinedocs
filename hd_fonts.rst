:hero: Full quality, full stop

HD Fonts
########

.. figure:: images/hd_fonts/icon.webp
    :align: right
    :height: 180px
    :class: pixel-edges

    The default font texture snippet.

.. location::

    /assets/minecraft/optifine/font/\*\*/\*.{png,properties}

.. danger::

    | This feature is obsolete. **Do not use it.** Minecraft's font system has fixed the issues HD Fonts was created to resolve.
    | Characters outside the ASCII range are not supported.

**HD Fonts** can define custom widths for ASCII characters.

OptiFine first looks for fonts in the ``/assets/minecraft/optifine/font`` folder.
This allows having a custom font that works in vanilla and a higher-resolution font that requires OptiFine to display properly.

To allow for more control over the widths of individual characters, OptiFine offers a way to specify them manually.
Create a properties file corresponding to the font you want to customize.

Properties
==========

``width.<ascii>``
-----------------

.. values::
    :values: Integer, where ``<ascii>`` is a value from 0 to 255
    :required: Yes

The width of the ASCII character.

``blend``
---------

.. values::
    :values: Boolean
    :required: No

Whether to use alpha blending.

``offsetBold``
--------------

.. values::
    :values: Float
    :required: No

Horizontal offset by which to render the bold copy of a glyph.

JSON schema
===========

.. note:: Although this page is ``.properties`` based, it can be mapped to JSON.

.. literalinclude:: ./schemas/hd_fonts.schema.json
