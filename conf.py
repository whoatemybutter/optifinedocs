#!/usr/bin/env python3
# coding=utf-8
# SPDX-License-Identifier: CC0
"""Sphinx config for OptiDocs."""
import os
import sys

sys.path.append(os.path.abspath("extensions"))
sys.path.append(os.path.abspath("."))

needs_sphinx = "8.0"

project = "OptiDocs"
author = "sp614x, WhoAteMyButter"
project_copyright = "CC0 (public domain). Credit is optional, but appreciated"
root_doc = "index"

html_favicon = "images/home/favicon.webp"
html_logo = "images/home/favicon.webp"
html_title = "OptiDocs"
html_show_copyright = True
html_show_sphinx = True
html_show_sourcelink = True
html_last_updated_fmt = "%Y %B %d"
html_theme = "sphinx_immaterial"
html_theme_options = {
    "palette": [
        {
            "media": "(prefers-color-scheme: dark)",
            "scheme": "slate",
            "primary": "red",
            "accent": "amber",
            "toggle": {
                "icon": "material/weather-night",
                "name": "Switch to light mode",
            },
        },
        {
            "media": "(prefers-color-scheme: light)",
            "scheme": "default",
            "primary": "red",
            "accent": "amber",
            "toggle": {
                "icon": "material/weather-sunny",
                "name": "Switch to dark mode",
            },
        },
    ],
    "features": [
        "header.autohide",
        "toc.follow",
        "toc.sticky",
        "navigation.tracking",
        "navigation.sections",
        "navigation.top",
        "navigation.instant",
        "search.share",
        "search.highlight",
        "announce.dismiss"
    ],
    "site_url": "https://optifine.readthedocs.io",
    "repo_url": "https://gitlab.com/whoatemybutter/optifinedocs",
    "edit_uri": "/-/edit/master/",
    "repo_name": "OptifineDocs",
    "icon": {
        "repo": "fontawesome/brands/git-alt",
        "edit": "material/file-edit-outline",
        "logo": "material/library"
    },
    "social": [
        {
            "icon": "fontawesome/brands/gitlab",
            "link": "https://gitlab.com/whoatemybutter/optifinedocs",
            "name": "Open source on GitLab",
        },
        {
            "icon": "fontawesome/brands/github",
            "link": "https://github.com/sp614x/optifine/tree/master/OptiFineDoc/doc",
            "name": "Original documentation on GitHub",
        },
        {
            "icon": "fontawesome/solid/bullseye",
            "link": "https://optifine.net",
            "name": "OptiFine website",
        },
        {
            "icon": "fontawesome/brands/discord",
            "link": "https://discord.gg/3mMpcwW",
            "name": "OptiFine Discord",
        },
        {
            "icon": "fontawesome/brands/reddit",
            "link": "https://reddit.com/r/optifine",
            "name": "OptiFine subreddit",
        },
        {
            "icon": "fontawesome/solid/money-bill-1",
            "link": "https://optifine.net/donate",
            "name": "Donate to OptiFine",
        },
    ]
}
sphinx_immaterial_custom_admonitions = [
    {
        "name": "location",
        "title": "File location",
        "icon": "fontawesome/solid/location-dot",
        "color": (0, 200, 82),
        "classes": ["location"],
    },
    {
        "name": "notconfused",
        "title": "Not to be confused with",
        "icon": "material/file-question",
        "color": (155, 155, 155),
        "classes": ["notconfused"],
    },
    {
        "name": "legacy",
        "color": (236, 64, 11),
        "icon": "fontawesome/solid/recycle",
    },
    {
        "name": "default",
        "title": "Default value",
        "color": (132, 206, 39),
        "icon": "fontawesome/solid/gear",
        "classes": ["default-value"],
    },
]
html_css_files = ["custom.css"]
html_static_path = ["_static"]
smartquotes = False
default_role = "any"

pygments_style = "sphinx"

rst_epilog = r"""
.. |file path| replace:: :ref:`syntax:paths, file locations`
.. |range| replace:: :ref:`syntax:range`
.. |drv| replace:: :ref:`Data rule value<syntax:components, nbt>`
.. |->| replace:: ➠
.. |elwarn| replace:: These resources are not related to OptiDocs, but may provide some help.

----

| :si-icon:`material/update` Assumes the latest OptiFine & Minecraft versions.
| :si-icon:`material/source-pull` Updated to commit `967a9fdb <https://github.com/sp614x/optifine/commit/967a9fdbf2c8f8d4466715cd6334a2d304642d5c>`_.
| :si-icon:`material/git` Open source at `<https://gitlab.com/whoatemybutter/optifinedocs>`_.
"""

extensions = [
    "sphinx.ext.autosectionlabel",
    "sphinx_reredirects",
    "sphinxext.opengraph",
    "sphinx_immaterial",
    "sphinx_immaterial.task_lists",
    "sphinx_immaterial.kbd_keys",
    "custom.valueoptions",
    "custom.colorbox",
]

suppress_warnings = [
    "autosectionlabel.*"
]

redirects = {
    "system_jvm_arguments": "jvm_args.html",
    "custom_item_textures": "cit.html",
    "custom_entity_models": "cem.html",
    "guis": "custom_guis.html",
    "colors": "custom_colors.html",
    "lightmaps": "custom_lightmaps.html",
    "loading": "custom_loading_screens.html",
    "panorama": "custom_panoramas.html",
    "panoramas": "custom_panoramas.html",
    "emissive": "emissive_textures.html",
    "emmissives": "emissive_textures.html",
    "emmisive": "emissive_textures.html",
    "frequently_asked_questions": "common_issues.html",
    "fonts": "hd_fonts.html",
    "properties_files": "syntax.html",
    "cem/animation": "../cem_animation.html",
    "cem/limitations": "../cem_limitations.html",
    "cem/models": "../cem_models.html",
    "cem/entity_names": "../cem_entity_names.html",
    "special_cosmetics": "cpm.html",
}

copybutton_prompt_text = r">>> |\.\.\. |\$ |In \[\d*\]: | {2,5}\.\.\.: | {5,8}: "
copybutton_prompt_is_regexp = True
copybutton_only_copy_prompt_lines = False
copybutton_copy_empty_lines = True
copybutton_line_continuation_character = "\\"
copybutton_here_doc_delimiter = "EOT"

sphinx_tabs_disable_tab_closing = True

ogp_site_url = "https://optifine.readthedocs.io"
ogp_site_name = "OptiDocs"
ogp_image = "_static/favicon.webp"
ogp_use_first_image = True
ogp_description_length = 245
ogp_custom_meta_tags = ['<meta name="theme-color" content="#df2b38" />']
ogp_enable_meta_description = True

autosectionlabel_prefix_document = True

templates_path = ["_templates"]

exclude_patterns = ["_build", "venv", ".venv"]
