Versioning
##########

.. figure:: images/versioning/icon.webp
    :align: right
    :height: 180px

    E3, D8, D7, B3, B2, B1

OptiFine's versioning scheme follows most of `Semantic Versioning <https://semver.org/>`_.

- Letter: major version (A-Z)
- Number: minor version (1-9)
- ``preX``: preview X (>=1)

Feature sets and Minecraft version changes warrant a major version increment.

Bugfixes and small changes warrant a minor version increment.

Preview versions can increment arbitrarily in order, and their feature set is mutable.

Minecraft versions are generally independent of the major version and are included in the version identifier because multiple versions of the same major version may be ported to more than 1 Minecraft version.
