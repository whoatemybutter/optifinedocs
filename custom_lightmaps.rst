:hero: God damn the sun

Custom Lightmaps
################

.. figure:: images/custom_lightmaps/icon.webp
    :align: right
    :height: 180px
    :class: pixel-edges

    Daylight lightmap.

.. location::

    /assets/minecraft/optifine/lightmap/\*\*/\*.png

**Custom Lightmaps** can change the color of light from light sources and under different conditions.

Vanilla lighting
================

Every block has two light values from ``0`` to ``15`` assigned to it, one for sky brightness and one for torch brightness.

A block in direct sunlight has a sky value of ``15``.
A block in the shade directly adjacent to it has a value of ``14`` and so on.

Blocks deep underground far from any block that can see the sky have sky brightness ``0``. Similarly for torches.
A torch block has light value ``14`` *(15 for glowstone)* and the light value drops by ``1`` each block away from it, in a diamond shape.

.. figure:: images/custom_lightmaps/torch_light.webp

    A visualization of the light levels a torch gives off.

To generate the lighting actually seen in game, Minecraft uses a 16px x 16px lightmap image.
The image's axes correspond to the 16 light levels of each type. If a block has torch brightness ``x`` and sky brightness ``y``, then the point (``x``, ``y``) is used for its lightmap.

.. important:: The lightmap is not in any of the game's assets.

Two variables affect the lightmap: the time of day, and the torch flicker.
Minecraft implements dusk/dawn transitions and torch flicker by making the entire lightmap darker or lighter as a whole. rather than by adjusting the sky/torch brightness values.

Other lightmaps
===============

To create custom lighting, a lightmap palette needs to be created for each world:

* Nether: ``/assets/minecraft/optifine/lightmap/world-1.png``
* Overworld: ``/assets/minecraft/optifine/lightmap/world0.png``
* The End: ``/assets/minecraft/optifine/lightmap/world1.png``

For the overworld, **optional** rain and thunder palettes may also be specified:

* Overworld rain: ``/assets/minecraft/optifine/lightmap/world0_rain.png``
* Overworld thunder: ``/assets/minecraft/optifine/lightmap/world0_thunder.png``

The rain and thunder palettes are only active when the main Overworld palette is defined.

Each palette can be any width, but must be 32 or 64 pixels tall.
If it's 64, the bottom half is used for night vision; see :ref:`custom_lightmaps:night vision`.

Of the 32 rows of pixels, the top 16 represent sunlight and the bottom 16 represent torchlight.

Two columns (16 pixels from the top half, and 16 pixels from the bottom half) are chosen to form the axes of the final 16px x 16px lightmap used.

.. figure:: images/custom_lightmaps/template.webp
    :width: 256px
    :class: pixel-edges

    Blue: Night.
    Orange: Dusk/dawn.
    Cyan: Day.
    Yellow: Lightning.

In the top half, the left-hand side represents night and the right-hand side represents day, with the dusk/dawn transitions in between.
The very far right of the palette represents lightning flashes.

Again, there is no specified width for the palette, but more width means more room for detail in the transitions.

Torches work similarly, but in this case the ``x`` coordinate is simply a random value simulating torch flicker.
The variation along the ``x`` dimension will determine how noticable torch flicker is.
To have completely steady torchlight with no flicker, make all pixels along each row the same color.

Lightmaps work the same in all three worlds, but since there is no night or day in Nether and The End, the "time of day" value is constant.
For these worlds, simply give rows ``0`` through ``15`` the same color all the way across.

Night vision
============

In Vanilla, the night vision effect is computed by scaling the RGB values by ``1.0 / max(R,G,B)``.
For example, ``(0.2, 0.3, 0.6)`` would brighten to ``(0.333, 0.5, 1.0)`` after dividing by ``0.6``.

This behaviour can be overridden with a custom lightmap by making the height 64 pixels instead of 32.
Provide four palettes instead of two: normal sun, normal torch, night vision sun, and night vision torch.

Lightmap generation works exactly the same way but uses rows 32-47 and 48-63 instead.

External links
==============

.. warning:: |elwarn|

* `Pablo's Developer Tools - Lightmap Templates <https://www.planetminecraft.com/texture-pack/pablo-s-developer-tools-lightmap-templates/>`_

