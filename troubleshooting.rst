:hero: Did you try turning it off and on again?

Troubleshooting
###############

You may run into a problem when using OptiFine.
Below are the most common problems and solutions.
This is a non-exhaustive list; it does not contain every conceivable issue.

To get general support after confirming your solution is *not* here, `join the Discord <https://discord.gg/3mMpcwW>`_.

Downloading
===========

Did not get a .JAR file
-----------------------

.. danger:: If you did not get a .JAR file, **do not** run it.

This occurs because you did not click the correct link when following the instructions on :doc:`download`.
Ensure that you skip ads, or click the "(mirror)" link.

Installing
==========

FileNotFoundException (Access Denied)
-------------------------------------

Go to the file location ``C:\Users\<your username here>\AppData\Roaming\.minecraft\libraries\optifine\OptiFine`` and delete the folder corresponding to the OptiFine version you are trying to install.

There are errors in the following switches
------------------------------------------

.. image:: images/install/errors/switches.webp

This occurs because you did not follow the instructions in :doc:`install_pre`.
Scroll to the Jarfix section and follow the directions.

Cannot find Minecraft version
-----------------------------

.. image:: images/install/errors/version.webp

This occurs because you did not follow the instructions in :doc:`install_vanilla`.
In the Vanilla Launcher, create a new profile (installation) with the requested Vanilla version.
Run it, and then close it.
Re-run OptiFine.

ZipException: error in opening zip file
---------------------------------------

.. image:: images/install/errors/zip.webp

Copy the below code and paste it into a file ending in ``.bat``.
Run it.

.. code::

    <# :

    @echo off
    echo Select the OptiFine.jar file to install
    setlocal

    for /f "delims=" %%I in ('powershell -noprofile "iex (${%~f0} | out-string)"') do (
    java -jar %%~I
    )
    goto :EOF

    : #>

    Add-Type -AssemblyName System.Windows.Forms
    $f = new-object Windows.Forms.OpenFileDialog
    $f.InitialDirectory = pwd
    $f.Filter = "JAR File (*.jar)|*.jar"
    $f.ShowHelp = $true
    $f.Multiselect = $true
    [void]$f.ShowDialog()
    if ($f.Multiselect) { $f.FileNames } else { $f.FileName }

Or, download the script :download:`here <_static/optifine_zip_fix.bat>`.

Could not find the main class
-----------------------------

.. image:: images/install/errors/mainclass.webp

You did not follow the instructions in :doc:`install_pre`.
You need to install a Java runtime.

Launching
=========

modName:tomatoGuy
-----------------

.. image:: images/install/errors/tomatoguy.webp

This occurs because you have an old version of Complementary shaders. Either `update it <https://www.curseforge.com/minecraft/customization/complementary-shaders>`_ or remove the shader pack.

Could not create the Java Virtual Machine
-----------------------------------------

.. image:: images/install/errors/vm.webp

This occurs because you did not follow the instructions in :doc:`install_pre`.
You need to install a Java runtime, as well as Jarfix.

Using
=====

Purple and black checkerboard textures
--------------------------------------

.. figure:: images/troubleshooting/missing.webp
    :align: right
    :width: 180px
    :class: pixel-edges

    The texture shown.

This happens because a required texture did not load. Normally, two things cause this:

* Invalid path (check :ref:`syntax:paths, file locations`).
* Missing file

Check your ``latest.log`` for more specifics on which textures are failing to load and why.

Ensure all of your ``.properties`` files point to valid texture paths.

Warped shaders
--------------

.. figure:: images/troubleshooting/badshaders.webp

    Half of all textures appear warped.

This occurs because of a known bug. Enabling and disabling shaders with Forge requires a restart.

If running on Forge, do not swap shaders while in a world.

Same FPS
--------

There are many reasons why, in some cases, the FPS with OptiFine may be the same or lower than Vanilla:

* Higher-quality settings are enabled.
* FPS might be limited by the FPS slider in Options.
* An applied resource pack may be using OptiFine-specific features intensely.
* There may be a mod conflict if other mods are installed alongside OptiFine.

Not using GPU
-------------

.. note:: These instructions only work for Windows.

1. Open the Settings app.
2. Navigate to :menuselection:`System --> Display`, and scroll down until you see :guilabel:`Graphics Settings`.
3. Select :guilabel:`Browse`, then locate and add ``javaw.exe``.
    * The location of this file will vary, but you can usually find it at ``C:\Program Files (x86)\Minecraft Launcher\runtime\jre-x64\bin``.
4. Select ``Java(TM) Platform SE Binary``, and then select :guilabel:`Options`.
5. Set the graphics preference to :guilabel:`High Performance`, and then save.

.. raw:: html

    <video width=100% controls muted playsinline>
        <source src="_static/gpu_windows10.mp4" type="video/mp4">
    Your browser does not support the video tag.
    </video>

Cape stolen
-----------

.. important:: Administrators **can not** and **will not** move capes for you.

Login to `<https://optifine.net/login>`_, and simply update the username for the cape.

Make sure it is also marked as :ref:`locked <capes:locking>`.
If you cannot move the cape for cooldown reasons, lock it and wait.

.. note::

    If you're not the person who made the donation for the cape, you will have to ask the original donator to perform these actions.
    If your friend gave you the cape, ask them.

Ensure your account is secure to prevent future incidents.
