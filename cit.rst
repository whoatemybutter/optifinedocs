:hero: It's really a gold nugget but don't worry about it

Custom Item Textures
####################

.. figure:: images/cit/icon.webp
    :align: right
    :height: 204px
    :class: pixel-edges

    The amount changes the texture.

**Custom Item Textures** (**CIT**) can change items to different textures based on their properties, such as enchantments, names, and NBT rules.

.. figure:: images/cit/settings.webp

    :menuselection:`Video Settings --> Quality`

Global properties
=================

.. location::

    /assets/minecraft/optifine/cit.properties

This file contains global properties for CIT and should be in the ``optifine/cit`` folder of the resource pack.

For individual item textures, see the :ref:`cit:properties` section.

.. note:: ``average`` and ``layered`` methods with ``cap=1`` are equivalent and will both show only the first enchantment on an item.

.. danger:: Not implemented: ``method``, ``cap``, ``fade``.

``method``
----------

.. values::
    :values: ``average``, ``layered``, or ``cycle``
    :required: No
    :default: ``average``

Specifies how to apply multiple effects to the same item.
Depending on the method chosen, multiple effects can be rendered with different intensities from ``0`` (invisible) to ``1`` (fully visible).

* ``average``: Weighted average by enchantment level: ``intensity = enchantment_level / sum(enchantment_levels)``.
* ``layered``: Similar to ``average``, but ``max()`` is used instead of ``sum()``: ``intensity = enchantment_level / max(enchantment_levels)``.
* ``cycle``: Cycle through each effect in turn. The duration of each effect can be set via the ``duration`` property.
  The ``[group]`` value (if present) allows multiple sets of effects to be cycled through independently.

``cap``
-------

.. values::
    :values: Positive integer
    :required: No
    :default: *None*

Specifies how many layers can be rendered for ``average`` and ``layered`` methods.
The top-most layers have priority over the bottom-most layers, as determined by the layer value of each effect.

``fade``
--------

.. values::
    :values: Positive float
    :required: No
    :default: ``0.5``

The speed at which one effect will transition into another in a cycle.
This does not affect the duration of the actual effect when displayed. For that, use the effect's ``duration`` property.

``useGlint``
------------

.. values::
    :values: Boolean
    :required: No
    :default: ``true``

Whether to use the default ``glint.png`` enchantment texture or not.

* If **true**: ``glint.png`` is used if no other custom enchantment effect is available.
* If **false**: the default ``glint.png`` enchantment stops rendering completely.

This is important for items that have no specific enchantment but have an enchantment effect, such as potions and golden apples.

.. danger:: This has been broken since 1.12. See `GH-6480 <https://github.com/sp614x/optifine/issues/6480>`_.

Properties
==========

.. location::

    /assets/minecraft/optifine/cit/\*\*/\*.properties

For each item to override with a custom texture, create a ``.properties`` file in the ``/assets/minecraft/optifine/cit/`` folder of the resource pack.
Properties files can be organized into subfolders of any depth, as long as everything is within the top-level ``optifine/cit`` folder.

Each properties file specifies:

- A list of matching items.
- A replacement texture or model.
- An **optional** set of rules specifying when this CIT will apply to the item.

.. note:: | For best compatibility with tag matching, use escape sequences for characters outside the ASCII range: ``\u0107`` instead of ``ć``.
          | These properties apply to all CIT types.

``type``
--------

.. values::
    :values: ``item``, ``enchantment``, ``armor``, or ``elytra``
    :required: No
    :default: ``item``

Type of texture replacement.

.. md-tab-set::

    .. md-tab-item:: ``item``

        Simple item texture replacement.
        It applies to items in the GUI, held in hand, and in the world.
        If multiple CIT rules match the same item, only the first is used (sorted by weight, then by file name).

    .. md-tab-item:: ``enchantment``

        Overlay texture for enchantments *(replaces misc/glint.png)*.
        If multiple CIT rules match the same item, they are blended together using rules specified in `Global properties`_.

        .. danger:: Past 1.12, this no longer works.

    .. md-tab-item:: ``armor``

        Armor texture replacement.
        Applies to armor models worn by players and mobs.
        If multiple CIT rules match the same item, only the first (sorted by weight, then by file name) is used.

    .. md-tab-item:: ``elytra``

        Elytra texture replacement.
        Applies to elytra model worn by players and mobs.
        If multiple CIT rules match the same item, only the first (sorted by weight, then by file name) is used.

``items``
---------

.. values::
     :values: Space-split list of items
     :required: No
     :default: *None*

What `items <https://minecraft.wiki/w/Java_Edition_data_values#Items>`_ to apply the CIT to.
If more than 1 item is specified, the CIT will apply to *any* item in the list.

``texture``
-----------

.. values::
    :values: |file path|
    :required: No
    :default: *Name of properties file:* ``x.properties -> x.png``

Path to the replacement texture.

It can be a full path or a lone name:

* ``mytextures/excalibur.png |->| mytextures/excalibur.png``
* ``excalibur |->| optifine/cit/excalibur.png``

``model``
---------

.. values::
    :values: |file path|
    :required: No
    :default: *None*

Path to the replacement model.
The model must be in `vanilla format <https://minecraft.wiki/Model#Item_models>`_.

* ``item/mymodel → /assets/minecraft/models/item/mymodel.json``
* ``./mymodel → mymodel.json`` from the same folder as the properties file

.. note:: The model may reference textures from the same folder as where the originating CIT file is.

``damage``
----------

.. values::
    :values: Integer from ``0`` to ``65535``, or integer |range| from ``0`` to ``65535``, or percentage range
    :required: No
    :default: *None*

Damage values. CIT will apply only when when the item damage is a certain value or range.

For items with durability, damage starts at 0 for a new item and increases as it gets damaged.
A brand-new item's damage will always be 0.
The maximum damage an item can take `varies <https://www.minecraftwiki.net/wiki/Item_durability>`_

For 1.12 and below, ``damage`` represents different properties like potion type.
See `this page <https://minecraft.wiki/w/Java_Edition_data_values/Pre-flattening>`_ for specifics.

``damageMask``
--------------

.. values::
    :values: Integer bitmask
    :required: No
    :default: ``0``

A `bitmask <https://en.wikipedia.org/wiki/Mask_(computing)>`_ applied to the item's damage before checking it against the list of eligible damage values.

Examples:

* Match any Fire Resistance potion: ``damage=3 damageMask=15``
* Match any non-splash Fire Resistance potion: ``damage=3 damageMask=16399``
* Match non-splash Fire Resistance I potion only: ``damage=3 damageMask=16447``
* Match splash Fire Resistance II potion only: ``damage=16403 damageMask=16447``

.. note:: For a simpler way, see `Potions`_.

.. danger:: This is an extremely unreliable and largely-unused method of checking properties. Do not use it.

``stackSize``
-------------

.. values::
    :values: Integer from ``0`` to ``65535``, or integer |range| from ``0`` to ``65535``
    :required: No
    :default: ``0-65535``

The required amount(s) of item that must be in an inventory slot.

Although the maximum legitimate amount is 64, values up to 65535 are allowed.

.. note:: Values above 64 are useless.

``enchantments``
----------------

.. values::
    :values: Space-split list of strings
    :required: No
    :default: *Any*

List of enchantment names to match.

The enchantment names may be short (``flame``) or in full (``minecraft:flame``).
For example: ``enchantments=minecraft:silk_touch sharpness smite``.

.. note:: If the ``enchantmentLevels`` rule is not specified, this rule matches **any** enchantment level.

``enchantmentIDs``
------------------

.. legacy::

    Alias to ``enchantments``.

``enchantmentLevels``
---------------------

.. values::
    :values: Space-split list of integers from ``0`` to ``255``
    :required: No
    :default: *Any*

List of enchantment levels.

Also allows ranges.
For example: ``enchantmentLevels=1 3 5 10``, or ``enchantmentLevels=5-``.

.. note:: If ``enchantments`` is not specified, this rule matches **any** enchantment type.

``hand``
--------

.. values::
    :values: ``any``, ``main``, or ``off``
    :required: No
    :default: ``any``

Hand on which the item is placed onto: main hand or off-hand.

When rendered in the inventory GUI, the item is considered to be in the ``main`` hand.

.. figure:: images/cit/dual_wield.webp
    :width: 320px

    CIT can apply conditionally if you're holding the item in the left or right hand.

``nbt``
-------

.. values::
    :values: Any valid :ref:`NBT matching rule <syntax:nbt format>`
    :required: No
    :default: *None*

.. legacy::

    For 1.20.5+, use ``components`` instead.

NBT-based rule.

``components``
--------------

.. values::
    :values: Any valid :ref:`Component matching rule <syntax:component format>`
    :required: No
    :default: *None*

Component-based rule.

Replacement texture is used only when the component rules all match against the item's components.
See :ref:`syntax:component format`.
You can have infinitely many data rules in a CIT.

Type-specific properties
========================

Items
-----

.. note:: Implies ``type=item``.

.. _item.texture:

``texture``
~~~~~~~~~~~

.. values::
    :values: |file path|
    :required: Yes
    :default: *None*

Replacement texture.

Animations **must** use Mojang's system of ``.mcmeta`` files for frame order and timing.

.. _item.texturename:

``texture.<name>``
~~~~~~~~~~~~~~~~~~

.. values::
    :values: |file path|
    :required: No
    :default: *None*

Replacement for alternate textures.

For items with more than one texture, this allows specifying replacements for each texture separately.
These textures depend on the item's model.
For example, the bow has four possible textures depending on its state:
``bow_standby``, ``bow_pulling_0``, ``bow_pulling_1``, or ``bow_pulling_2``.

To replace **all four**, this can be used:

.. code:: properties

    texture.bow_standby=my_special_bow_standby
    texture.bow_pulling_0=my_special_bow_pulling_0
    texture.bow_pulling_1=my_special_bow_pulling_1
    texture.bow_pulling_2=my_special_bow_pulling_2

Potions also have two textures. To replace them, use:

.. code:: properties

    texture.potion_overlay=...
    texture.potion_bottle=...

.. note:: If no ``texture.<name>`` property matches, the generic texture property is used instead.

``model.<name>``
~~~~~~~~~~~~~~~~

.. values::
    :values: |file path|
    :required: No
    :default: *None*

Replacement for alternate models.

For items with more than one model, this allows specifying replacements for each model separately.

For example, the bow has four possible textures depending on its state:
``bow_standby``, ``bow_pulling_0``, ``bow_pulling_1``, ``bow_pulling_2``.
To replace **all four**, this can be used:

.. code:: properties

    model.bow_standby=my_special_bow_standby
    model.bow_pulling_0=my_special_bow_pulling_0
    model.bow_pulling_1=my_special_bow_pulling_1
    model.bow_pulling_2=my_special_bow_pulling_2

``weight``
~~~~~~~~~~

.. values::
    :values: Positive integer
    :required: No
    :default: ``0``

If multiple CIT rules match the same item, the highest-weighted one is used (the biggest number).
In the event of a tie, the properties filenames are compared alphabetically.

Enchantments
------------

.. note:: Implies ``type=enchantment``.
.. note:: ``duration`` only works for ``cycle`` enchantments.

.. danger:: Past 1.12, this no longer works.

.. _enchantment.texture:

``texture``
~~~~~~~~~~~

.. values::
    :values: |file path|
    :required: Yes
    :default: *None*

The enchantment texture can be any resolution.

To animate an enchantment, use the ``anim/*.properties`` method with ``to=full path to enchantment texture``

``blend``
~~~~~~~~~

.. values::
    :values: String
    :required: No
    :default: ``add``

Blend method when applying texture to the texture below it.

See :ref:`syntax:blending methods` for a list of valid blending methods.

.. danger:: This has been broken. See `GH-5304 <https://github.com/sp614x/optifine/issues/5304>`_.

``speed``
~~~~~~~~~

.. values::
    :values: Positive integer
    :required: No
    :default: ``1``

Scrolling speed of texture.

``0`` means no scrolling.

``rotation``
~~~~~~~~~~~~

.. values::
    :values: Positive integer from ``0`` to ``360``
    :required: No

Angle of texture (in degrees) relative to the item.

If ``speed`` is non-zero, the texture will also scroll in this direction.

``layer``
~~~~~~~~~

.. values::
    :values: Positive integer
    :required: No
    :default: ``0``

Specifies a unique layer and the ordering of the layers as they overlap each other.

If two or more effects use the same layer, ``weight`` next determines which effect is rendered (the other is not rendered).

.. _enchantment.weight:

``weight``
~~~~~~~~~~

.. values::
    :values: Positive integer
    :required: No
    :default: ``0``

The relative priority of the enchantment within a layer.

Of the matching effects, only the highest weighted one within a layer is rendered.
In other words:

* The layer property determines the **ORDER** in which effects are rendered.
* The weight property determines **WHICH** effect is rendered for each layer.

If two effects have the same weight and layer, the properties filenames are sorted and compared alphabetically.

``duration``
~~~~~~~~~~~~

.. values::
    :values: Positive integer
    :required: No
    :default: ``0``

Duration in seconds of the enchantment glint in a cycle.

Armor
-----

.. note:: Implies ``type=armor``

.. _armor.texturename:

``texture.<name>``
~~~~~~~~~~~~~~~~~~

.. values::
    :values: |file path|
    :required: Yes
    :default: *None*

Replacement textures.

A replacement for each texture is needed in ``minecraft:textures/models/armor/`` for that armor type.

For diamond armor *(2 layers total)*:

.. code:: properties

    texture.diamond_layer_1=my_diamond_armor_1
    texture.diamond_layer_2=my_diamond_armor_2

For leather armor *(4 layers total)*:

.. code:: properties

    texture.leather_layer_1=my_leather_armor_1
    texture.leather_layer_1_overlay=my_leather_armor_1_overlay
    texture.leather_layer_2=my_leather_armor_2
    texture.leather_layer_2_overlay=my_leather_armor_2_overlay

The texture should match the format of the corresponding armor texture.

For animated textures, use the ``anim/*.properties`` method with ``to``.

Potions
=======

.. note:: While there is no specific potion-related tag, ``nbt`` should be used.

Potions with custom effects can be matched using their NBT ``Potion`` string or with ``nbt.CustomPotionEffects.*.Id``.

.. md-tab-set::

    .. md-tab-item:: NBT Potion string

        .. code:: properties

            type=item
            items=potion
            nbt.Potion=minecraft:strength

    .. md-tab-item:: CustomPotionEffects ID

        .. code:: properties

            type=item
            items=potion
            nbt.CustomPotionEffects.*.Id=20

Stronger versions of potions can be matched by prefixing ``strong_`` to the ``Potion`` NBT tag match.
Longer versions of potions can be matched by prefixing ``long_`` to the ``Potion`` NBT tag match.
Lingering and Splash potions can be matched with the same method by simply changing the ``items`` tag appropriately.

Shortcut
--------

.. note:: Everything described here **can** be done via CIT properties files; this is a **shortcut**.
.. note:: No properties files are necessary for this method.

As an alternative to listing potion damage values or testing NBT,
replacement textures for potions can be specified using a file name-based system.

There are **three** directories for potions:

1. ``optifine/cit/potion/normal``: `drinkable potions <https://minecraft.wiki/w/Potion>`__.
2. ``optifine/cit/potion/splash``: `splash potions <https://minecraft.wiki/w/Splash_Potion>`__.
3. ``optifine/cit/potion/linger``: `lingering potions <https://minecraft.wiki/w/Lingering_Potion>`__.

Within any of these directories, create a PNG file with the name of the potion effect:

.. note:: | Effect names in *italic* means they are obtainable in-game.
          | Effects not in *italic* can only be created via commands.
.. important:: This replaces **both** ``potion.png/potion_splash.png`` and ``potion_contents.png`` from the standard potion rendering.
.. warning:: Be sure to include the *colored* liquid in the replacement textures. Tint is not applied.

.. csv-table::
    :header: "Effect name", "File name"

    "Absorption", "``absorption.png``"
    "Blindness", "``blindness.png``"
    "Confusion", "``confusion.png``"
    "*Damage Boost*", "``damageboost.png``"
    "Mining Fatigue", "``digslowdown.png``"
    "Haste", "``digspeed.png``"
    "*Fire Resistance*", "``fireresistance.png``"
    "*Harming*", "``harm.png``"
    "*Healing*", "``heal.png``"
    "Health Boost", "``healthboost.png``"
    "Hunger", "``hunger.png``"
    "*Invisibility*", "``invisibility.png``"
    "Leaping", "``jump.png``"
    "*Slowness*", "``moveslowdown.png``"
    "*Speed*", "``movespeed.png``"
    "*Night Vision*", "``nightvision.png``"
    "*Poison*", "``poison.png``"
    "*Regeneration*", "``regeneration.png``"
    "Resistance", "``resistance.png``"
    "Saturation", "``saturation.png``"
    "Water Breathing", "``waterbreathing.png``"
    "*Weakness*", "``weakness.png``"
    "Wither", "``wither.png``"

The names are the same as the potion.
The replacement texture will automatically be used for that potion type; no properties file is required.
Note that this replaces **both**.

Similarly, textures can be replaced for the various "no effect" potions.
These have drinkable versions **only**, the rest are in the code and are listed here only for completeness.

.. note:: | Effect names in *italic* means they are obtainable in-game
          | Effects not in *italic* can only be created via commands

.. csv-table::
    :header: "Effect name", "File name"

    "Artless", "``artless.png``"
    "*Awkward*", "``awkward.png``"
    "Bland", "``bland.png``"
    "Bulky", "``bulky.png``"
    "Bungling", "``bungling.png``"
    "Buttered", "``buttered.png``"
    "Charming", "``charming.png``"
    "Clear", "``clear.png``"
    "Cordial", "``cordial.png``"
    "Dashing", "``dashing.png``"
    "Debonair", "``debonair.png``"
    "Elegant", "``elegant.png``"
    "Fancy", "``fancy.png``"
    "Flat", "``flat.png``"
    "Foul", "``foul.png``"
    "Gross", "``gross.png``"
    "Harsh", "``harsh.png``"
    "Milky", "``milky.png``"
    "*Mundane*", "``mundane.png``"
    "Odorless", "``odorless.png``"
    "Potent", "``potent.png``"
    "Rank", "``rank.png``"
    "Sparkling", "``sparkling.png``"
    "Stinky", "``stinky.png``"
    "Suave", "``suave.png``"
    "*Thick*", "``thick.png``"
    "Thin", "``thin.png``"
    "Uninteresting", "``uninteresting.png``"

If a single texture for all "no effect" potions is preferred,
``/assets/minecraft/optifine/cit/potion/normal/other.png`` is used as a fallback for any that do not have a specific replacement as listed above.

Two additional textures *(drinkable only)* can also be provided:

- ``optifine/cit/potion/normal/water.png``: water bottle
- ``optifine/cit/potion/normal/empty.png``: empty glass bottle

Examples
========

Stacked coins
-------------

.. code:: properties

    type=item
    items=iron_nugget
    stackSize=61-64
    texture=iron_coins_16

Splash potion
-------------

.. md-tab-set::

    .. md-tab-item:: /assets/minecraft/optifine/cit/fire_resistance_splash.properties

        .. code:: properties

            type=item
            items=splash_potion
            model=item/fire_resistance_splash
            nbt.Potion=minecraft:fire_resistance

    .. md-tab-item:: /assets/minecraft/models/item/fire_resistance_splash.json

        .. code:: javascript

            {
                "parent": "item/generated",
                "textures": {
                    "layer0": "item/fire_resistance_overlay",
                    "layer1": "item/fire_resistance_splash"
                }
            }

External links
==============

.. warning:: |elwarn|

* `MCStacker <https://mcstacker.net/>`_ - for generating items that can help you test conditions.

JSON schema
===========

.. note:: Although this page is ``.properties`` based, it can be mapped to JSON.

.. literalinclude:: ./schemas/cit.schema.json

.. literalinclude:: ./schemas/cit_global.schema.json
    :caption: For global CIT only
