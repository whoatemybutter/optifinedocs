:hero: Jack, John, Jennifer, and Jasper walk into a bar

Entity names
############

.. figure:: images/cem/icon_entity_names.webp
    :align: right
    :height: 180px
    :class: pixel-edges

    Too many names

This is a reference table of entities and their applicable part names. Part names must be matched with the entity they will apply to.

.. important::

    All models except ``banner``, ``bed``, ``conduit`` and ``decorated_pot`` have a "root" part.
    Only the elements "models" and "part" are required.

Normal entities
===============

.. csv-table::
    :widths: 10, 90
    :header: "Entity name", "Part name"

    "`allay <https://minecraft.wiki/w/allay>`_", "body, head, left_arm, right_arm, left_wing, right_wing"
    "`armadillo <https://minecraft.wiki/w/armadillo>`_", "body, head, left_ear, right_ear, left_ear_cube, right_ear_cube, back_left_leg, back_right_leg, front_left_leg, front_right_leg, cube, tail"
    "`armor_stand <https://minecraft.wiki/w/armor_stand>`_", "body, head, headwear, left_arm, right_arm, left_leg, right_leg, right, left, waist, base"
    "`arrow <https://minecraft.wiki/w/arrow>`_", "body, cross_1, cross_2"
    "`axolotl <https://minecraft.wiki/w/axolotl>`_", "body, head, leg1 ... leg4, tail, top_gills, left_gills, right_gills"
    "`banner <https://minecraft.wiki/w/banner>`_", "slate, stand, top"
    "`bat <https://minecraft.wiki/w/bat>`_", "body, head, right_wing, left_wing, outer_right_wing, outer_left_wing, feet, root"
    "`bee <https://minecraft.wiki/w/bee>`_", "body, torso, right_wing, left_wing, front_legs, middle_legs, back_legs, stinger, left_antenna, right_antenna"
    "`bed <https://minecraft.wiki/w/bed>`_", "head, foot, leg1 ... leg4"
    "`bell <https://minecraft.wiki/w/bell>`_", "body" 
    "`blaze <https://minecraft.wiki/w/blaze>`_", "head, stick1 ... stick12"
    "`bogged <https://minecraft.wiki/w/bogged>`_", "body, head, headwear, left_arm, right_arm, left_leg, right_leg, mushrooms"
    "bogged_outer", "body, head, headwear, left_arm, right_arm, left_leg, right_leg"
    "`breeze <https://minecraft.wiki/w/breeze>`_", "body, rods, head, wind_body, wind_middle, wind_bottom, wind_top"
    "breeze_wind", "body, rods, head, wind_body, wind_middle, wind_bottom, wind_top"
    "`breeze_wind_charge <https://minecraft.wiki/w/wind_charge>`_", "*all parts of* ``wind_charge``"
    "`camel <https://minecraft.wiki/w/camel>`_", "body, hump, tail, head, left_ear, right_ear, back_left_leg, back_right_leg, front_left_leg, front_right_leg, saddle, reins, bridle"
    "`cat <https://minecraft.wiki/w/cat>`_", "body, head, tail, tail2, back_left_leg, back_right_leg, front_left_leg, front_right_leg"
    "`cat_collar <https://minecraft.wiki/w/collar>`_", "*all parts of* ``cat``"
    "`cave_spider <https://minecraft.wiki/w/cave_spider>`_", "body, head, neck, leg1 ... leg8"
    "`chicken <https://minecraft.wiki/w/chicken>`_", "body, head, right_leg, left_leg, right_wing, left_wing, bill, chin"
    "`cod <https://minecraft.wiki/w/cod>`_", "body, fin_back, head, nose, fin_right, fin_left, tail"
    "`cow <https://minecraft.wiki/w/cow>`_", "body, head, leg1 ... leg4"
    "`creaking <https://minecraft.wiki/w/creaking>`_", "body, upper_body, head, right_arm, left_arm, right_leg, left_leg"
    "`creeper <https://minecraft.wiki/w/creeper>`_", "body, head, leg1 ... leg4"
    "`creeper_charge <https://minecraft.wiki/w/charged_creeper>`_", "body, head, leg1 ... leg4"
    "`dolphin <https://minecraft.wiki/w/dolphin>`_", "body, back_fin, left_fin, right_fin, tail, tail_fin, head"
    "`donkey <https://minecraft.wiki/w/donkey>`_", "body, neck, back_right_leg, back_left_leg, front_right_leg, front_left_leg, tail, saddle, head, mane, mouth, left_ear, right_ear, left_bit, right_bit, left_rein, right_rein, headpiece, noseband, left_chest, right_chest"
    "`dragon <https://minecraft.wiki/w/dragon>`_", "head, jaw, body, neck1 ... neck4, neck5, tail1 ... tail12, left_wing, left_wing_tip, front_left_leg, front_left_shin, front_left_foot, back_left_leg, back_left_shin, back_left_foot, right_wing, right_wing_tip, front_right_leg, front_right_shin, front_right_foot, back_right_leg, back_right_shin, back_right_foot"
    "`drowned <https://minecraft.wiki/w/drowned>`_", "body, head, headwear, left_arm, right_arm, left_leg, right_leg"
    "drowned_outer", "body, head, headwear, left_arm, right_arm, left_leg, right_leg"
    "`elder_guardian <https://minecraft.wiki/w/elder_guardian>`_", "body, eye, spine1 ... spine12, tail1 ... tail3"
    "`end_crystal <https://minecraft.wiki/w/end_crystal>`_", "cube, outer_glass, inner_glass, base"
    "`enderman <https://minecraft.wiki/w/enderman>`_", "body, head, headwear, left_arm, right_arm, left_leg, right_leg"
    "`endermite <https://minecraft.wiki/w/endermite>`_", "body1 ... body4"
    "`evoker <https://minecraft.wiki/w/evoker>`_", "body, head, hat, arms, right_leg, left_leg, nose, right_arm, left_arm"
    "`evoker_fangs <https://minecraft.wiki/w/evoker_fangs>`_", "base, upper_jaw, lower_jaw"
    "`fox <https://minecraft.wiki/w/fox>`_", "body, head, leg1 ... leg4, tail"
    "`frog <https://minecraft.wiki/w/frog>`_", "body, head, eyes, tongue, left_arm, right_arm, left_leg, right_leg, croaking_body"
    "`ghast <https://minecraft.wiki/w/ghast>`_", "body, tentacle1 ... tentacle9"
    "`giant <https://minecraft.wiki/w/giant>`_", "body, head, headwear, left_arm, right_arm, left_leg, right_leg"
    "`glow_squid <https://minecraft.wiki/w/glow_squid>`_", "body, tentacle1 ... tentacle8"
    "`goat <https://minecraft.wiki/w/goat>`_", "body, head, leg1 ... leg4, left_horn, right_horn, nose"
    "`guardian <https://minecraft.wiki/w/guardian>`_", "body, eye, spine1 ... spine12, tail1 ... tail3"
    "`hoglin <https://minecraft.wiki/w/hoglin>`_", "body, mane, head, right_ear, left_ear, front_right_leg, front_left_leg, back_right_leg, back_left_leg"
    "`horse <https://minecraft.wiki/w/horse>`_", "body, neck, back_right_leg, back_left_leg, front_right_leg, front_left_leg, tail, saddle, head, mane, mouth, left_ear, right_ear, left_bit, right_bit, left_rein, right_rein, headpiece, noseband"
    "`horse_armor <https://minecraft.wiki/w/horse_armor>`_", "body, neck, back_right_leg, back_left_leg, front_right_leg, front_left_leg, tail, saddle, head, mane, mouth, left_ear, right_ear, left_bit, right_bit, left_rein, right_rein, headpiece, noseband"
    "`husk <https://minecraft.wiki/w/husk>`_", "body, head, headwear, left_arm, right_arm, left_leg, right_leg"
    "`illusioner <https://minecraft.wiki/w/illusioner>`_", "body, head, hat, arms, right_leg, left_leg, nose, right_arm, left_arm"
    "`iron_golem <https://minecraft.wiki/w/iron_golem>`_", "body, head, right_arm, left_arm, left_leg, right_leg"
    "`lead_knot <https://minecraft.wiki/w/lead_knot>`_", "knot"
    "`llama <https://minecraft.wiki/w/llama>`_", "body, head, leg1 ... leg4, chest_right, chest_left"
    "llama_decor", "body, head, leg1 ... leg4, chest_right, chest_left"
    "`llama_spit <https://minecraft.wiki/w/llama_spit>`_", "body"
    "`magma_cube <https://minecraft.wiki/w/magma_cube>`_", "core, segment1 ... segment8"
    "`mooshroom <https://minecraft.wiki/w/mooshroom>`_", "body, head, leg1 ... leg4"
    "`mule <https://minecraft.wiki/w/mule>`_", "body, neck, back_right_leg, back_left_leg, front_right_leg, front_left_leg, tail, saddle, head, mane, mouth, left_ear, right_ear, left_bit, right_bit, left_rein, right_rein, headpiece, noseband, left_chest, right_chest"
    "`ocelot <https://minecraft.wiki/w/ocelot>`_", "body, head, tail, tail2, back_left_leg, back_right_leg, front_left_leg, front_right_leg"
    "`panda <https://minecraft.wiki/w/panda>`_", "body, head, leg1 ... leg4"
    "`parrot <https://minecraft.wiki/w/parrot>`_", "body, tail, left_wing, right_wing, head, left_leg, right_leg"
    "parrot_shoulder", "*all parts of* ``parrot``"
    "`phantom <https://minecraft.wiki/w/phantom>`_", "body, head, left_wing, left_wing_tip, right_wing, right_wing_tip, tail, tail2"
    "`pig <https://minecraft.wiki/w/pig>`_", "body, head, leg1 ... leg4"
    "`pig_saddle <https://minecraft.wiki/w/saddle>`_", "body, head, leg1 ... leg4"
    "`piglin <https://minecraft.wiki/w/piglin>`_", "body, head, headwear, left_arm, right_arm, left_leg, right_leg, left_sleeve, right_sleeve, left_pants, right_pants, jacket, left_ear, right_ear"
    "`piglin_brute <https://minecraft.wiki/w/piglin_brute>`_", "body, head, headwear, left_arm, right_arm, left_leg, right_leg, left_sleeve, right_sleeve, left_pants, right_pants, jacket, left_ear, right_ear"
    "`pillager <https://minecraft.wiki/w/pillager>`_", "body, head, hat, arms, right_leg, left_leg, nose, right_arm, left_arm"
    "`polar_bear <https://minecraft.wiki/w/polar_bear>`_", "body, head, leg1 ... leg4"
    "`puffer_fish_big <https://minecraft.wiki/w/puffer_fish>`_", "body, fin_right, fin_left, spikes_front_top, spikes_middle_top, spikes_back_top, spikes_front_right, spikes_front_left, spikes_front_bottom, spikes_middle_bottom, spikes_back_bottom, spikes_back_right, spikes_back_left"
    "`puffer_fish_medium <https://minecraft.wiki/w/puffer_fish>`_", "body, fin_right, fin_left, spikes_front_top, spikes_back_top, spikes_front_right, spikes_back_right, spikes_back_left, spikes_front_left, spikes_back_bottom, spikes_front_bottom"
    "`puffer_fish_small <https://minecraft.wiki/w/puffer_fish>`_", "body, eye_right, eye_left, fin_right, fin_left, tail"
    "`rabbit <https://minecraft.wiki/w/rabbit>`_", "body, left_foot, right_foot, left_thigh, right_thigh, left_arm, right_arm, head, right_ear, left_ear, tail, nose"
    "`ravager <https://minecraft.wiki/w/ravager>`_", "body, head, jaw, leg1 ... leg4, neck"
    "`salmon <https://minecraft.wiki/w/salmon>`_", "body_front, body_back, head, fin_back_1, fin_back_2, tail, fin_right, fin_left"
    "`salmon_large <https://minecraft.wiki/w/salmon>`_", "*all parts of* ``salmon``"
    "`salmon_small <https://minecraft.wiki/w/salmon>`_", "*all parts of* ``salmon``"
    "`sheep <https://minecraft.wiki/w/sheep>`_", "body, head, leg1 ... leg4"
    "sheep_wool", "body, head, leg1 ... leg4"
    "`shield <https://minecraft.wiki/w/shield>`_", "plate, handle"
    "`shulker <https://minecraft.wiki/w/shulker>`_", "base, lid, head"
    "`shulker_bullet <https://minecraft.wiki/w/shulker_bullet>`_", "bullet"
    "`silverfish <https://minecraft.wiki/w/silverfish>`_", "body1 ... body7, wing1 ... wing3"
    "`skeleton <https://minecraft.wiki/w/skeleton>`_", "body, head, headwear, left_arm, right_arm, left_leg, right_leg"
    "`skeleton_horse <https://minecraft.wiki/w/skeleton_horse>`_", "body, neck, back_right_leg, back_left_leg, front_right_leg, front_left_leg, tail, saddle, head, mane, mouth, left_ear, right_ear, left_bit, right_bit, left_rein, right_rein, headpiece, noseband"
    "`slime <https://minecraft.wiki/w/slime>`_", "body, left_eye, right_eye, mouth"
    "slime_outer", "body"
    "`sniffer <https://minecraft.wiki/w/sniffer>`_", "body, back_left_leg, back_right_leg, middle_left_leg, middle_right_leg, front_left_leg, front_right_leg, head, left_ear, right_ear, nose, lower_beak"
    "`snow_golem <https://minecraft.wiki/w/snow_golem>`_", "body, body_bottom, head, right_hand, left_hand"
    "`spectral_arrow <https://minecraft.wiki/w/spectral_arrow>`_", "*all parts of* ``arrow``"
    "`spider <https://minecraft.wiki/w/spider>`_", "body, head, neck, leg1 ... leg4, leg5, leg6, leg7, leg8"
    "`squid <https://minecraft.wiki/w/squid>`_", "body, tentacle1 ... tentacle8"
    "`stray <https://minecraft.wiki/w/stray>`_", "body, head, headwear, left_arm, right_arm, left_leg, right_leg"
    "stray_outer", "body, head, headwear, left_arm, right_arm, left_leg, right_leg"
    "`strider <https://minecraft.wiki/w/strider>`_", "body, right_leg, left_leg, hair_right_bottom, hair_right_middle, hair_right_top, hair_left_top, hair_left_middle, hair_left_bottom"
    "`strider_saddle <https://minecraft.wiki/w/saddle>`_", "body, right_leg, left_leg, hair_right_bottom, hair_right_middle, hair_right_top, hair_left_top, hair_left_middle, hair_left_bottom"
    "`tadpole <https://minecraft.wiki/w/tadpole>`_", "body, tail"
    "`trader_llama <https://minecraft.wiki/w/trader_llama>`_", "body, head, leg1 ... leg4, chest_right, chest_left"
    "trader_llama_decor", "body, head, leg1 ... leg4, chest_right, chest_left"
    "`trident <https://minecraft.wiki/w/trident>`_", "body, base, left_spike, middle_spike, right_spike"
    "trident_hand", "*all parts of* ``trident``"
    "`tropical_fish_a <https://minecraft.wiki/w/tropical_fish>`_", "body, tail, fin_right, fin_left, fin_top"
    "`tropical_fish_b <https://minecraft.wiki/w/tropical_fish>`_", "body, tail, fin_right, fin_left, fin_top, fin_bottom"
    "tropical_fish_pattern_a", "body, tail, fin_right, fin_left, fin_top"
    "tropical_fish_pattern_b", "body, tail, fin_right, fin_left, fin_top, fin_bottom"
    "`turtle <https://minecraft.wiki/w/turtle>`_", "body, head, leg1 ... leg4, body2"
    "`vex <https://minecraft.wiki/w/vex>`_", "body, head, right_arm, left_arm, right_wing, left_wing"
    "`villager <https://minecraft.wiki/w/villager>`_", "body, head, headwear, headwear2, bodywear, arms, right_leg, left_leg, nose"
    "`vindicator <https://minecraft.wiki/w/vindicator>`_", "body, head, hat, arms, right_leg, left_leg, nose, right_arm, left_arm"
    "`wandering_trader <https://minecraft.wiki/w/wandering_trader>`_", "body, head, headwear, headwear2, bodywear, arms, right_leg, left_leg, nose"
    "`warden <https://minecraft.wiki/w/warden>`_", "body, torso, head, right_leg, left_leg, right_arm, left_arm, right_tendril, left_tendril, right_ribcage, left_ribcage"
    "`wind_charge <https://minecraft.wiki/w/wind_charge>`_", "wind, charge"
    "`witch <https://minecraft.wiki/w/witch>`_", "body, head, headwear, headwear2, bodywear, arms, right_leg, left_leg, nose, mole"
    "`wither <https://minecraft.wiki/w/wither>`_", "body1 ... body3, head1 ... head3"
    "wither_armor", "body1, body2, body3, head1, head2, head3"
    "`wither_skeleton <https://minecraft.wiki/w/wither_skeleton>`_", "body, head, headwear, left_arm, right_arm, left_leg, right_leg"
    "`wither_skull <https://minecraft.wiki/w/wither#wither_skull>`_", "head"
    "`wolf <https://minecraft.wiki/w/wolf>`_", "body, head, leg1 ... leg4, tail, mane"
    "`wolf_armor <https://minecraft.wiki/w/wolf_armor>`_", "body, head, leg1 ... leg4, tail, mane"
    "`wolf_collar <https://minecraft.wiki/w/collar>`_", "<fallback to wolf>"
    "`zoglin <https://minecraft.wiki/w/zoglin>`_", "body, mane, head, right_ear, left_ear, front_right_leg, front_left_leg, back_right_leg, back_left_leg"
    "`zombie <https://minecraft.wiki/w/zombie>`_", "body, head, headwear, left_arm, right_arm, left_leg, right_leg"
    "`zombie_horse <https://minecraft.wiki/w/zombie_horse>`_", "body, neck, back_right_leg, back_left_leg, front_right_leg, front_left_leg, tail, saddle, head, mane, mouth, left_ear, right_ear, left_bit, right_bit, left_rein, right_rein, headpiece, noseband"
    "`zombie_villager <https://minecraft.wiki/w/zombie_villager>`_", "body, head, headwear, left_arm, right_arm, left_leg, right_leg"
    "`zombified_piglin <https://minecraft.wiki/w/zombified_piglin>`_", "body, head, headwear, left_arm, right_arm, left_leg, right_leg, left_sleeve, right_sleeve, left_pants, right_pants, jacket, left_ear, right_ear"

Baby entities
=============

.. note:: All baby entities have the same part names as their adult variants.

.. csv-table::
    :widths: 10, 90
    :header: "Entity name", "Part name"

    "armadillo_baby", "*all parts of* ``armadillo``"
    "armor_stand_small", "*all parts of* ``armor_stand``"
    "axolotl_baby", "*all parts of* ``axolotl``"
    "bee_baby", "*all parts of* ``bee``"
    "camel_baby", "*all parts of* ``camel``"
    "cat_baby", "*all parts of* ``cat``"
    "cat_baby_collar", "*all parts of* ``cat_collar``"
    "chicken_baby", "*all parts of* ``chicken``"
    "cow_baby", "*all parts of* ``cow``"
    "dolphin_baby", "*all parts of* ``dolphin``"
    "donkey_baby", "*all parts of* ``donkey``"
    "drowned_baby", "*all parts of* ``drowned``"
    "drowned_baby_outer", "*all parts of* ``drowned_outer``"
    "fox_baby", "*all parts of* ``fox``"
    "glow_squid_baby", "*all parts of* ``glow_squid``"
    "goat_baby", "*all parts of* ``goat``"
    "hoglin_baby", "*all parts of* ``hoglin``"
    "horse_baby", "*all parts of* ``horse``"
    "horse_baby_armor", "*all parts of* ``horse_armor``"
    "husk_baby", "*all parts of* ``husk``"
    "llama_baby", "*all parts of* ``llama``"
    "llama_baby_decor", "*all parts of* ``llama_decor``"
    "mooshroom_baby", "*all parts of* ``mooshroom``"
    "mule_baby", "*all parts of* ``mule``"
    "ocelot_baby", "*all parts of* ``ocelot``"
    "panda_baby", "*all parts of* ``panda``"
    "pig_baby", "*all parts of* ``pig``"
    "pig_baby_saddle", "*all parts of* ``pig_saddle``"
    "piglin_baby", "*all parts of* ``piglin``"
    "polar_bear_baby", "*all parts of* ``polar_bear``"
    "rabbit_baby", "*all parts of* ``rabbit``"
    "sheep_baby", "*all parts of* ``sheep``"
    "sheep_baby_wool", "*all parts of* ``sheep_wool``"
    "skeleton_horse_baby", "*all parts of* ``skeleton_horse``"
    "sniffer_baby", "*all parts of* ``sniffer``"
    "squid_baby", "*all parts of* ``squid``"
    "strider_baby", "*all parts of* ``strider``"
    "strider_baby_saddle", "*all parts of* ``strider_saddle``"
    "trader_llama_baby", "*all parts of* ``trader_llama``"
    "trader_llama_baby_decor", "*all parts of* ``trader_llama_decor``"
    "turtle_baby", "*all parts of* ``turtle``"
    "villager_baby", "*all parts of* ``villager``"
    "wolf_baby", "*all parts of* ``wolf``"
    "wolf_baby_armor", "*all parts of* ``wolf_armor``"
    "wolf_baby_collar", "*all parts of* ``wolf_collar``"
    "zoglin_baby", "*all parts of* ``zoglin``"
    "zombie_baby", "*all parts of* ``zombie``"
    "zombie_horse_baby", "*all parts of* ``zombie_horse``"
    "zombie_villager_baby", "*all parts of* ``zombie_villager``"
    "zombified_piglin_baby", "*all parts of* ``zombified_piglin``"

Boat entities
=============

.. csv-table::
    :widths: 10, 90
    :header: "Entity name", "Part name"

    "`boat <https://minecraft.wiki/w/boat>`_", "bottom, back, front, right, left, paddle_left, paddle_right"
    "`chest_boat <https://minecraft.wiki/w/chest_boat>`_", "bottom, back, front, right, left, paddle_left, paddle_right, chest_base, chest_lid, chest_knob"
    "boat_patch", "water_patch"
    "`raft <https://minecraft.wiki/w/raft>`_", "bottom, paddle_left, paddle_right"
    "`chest_raft <https://minecraft.wiki/w/chest_boat>`_", "bottom, paddle_left, paddle_right, chest_base, chest_lid, chest_knob"


Minecart entities
=================

.. csv-table::
    :widths: 10, 90
    :header: "Entity name", "Part name"

    "`minecart <https://minecraft.wiki/w/minecart>`_", "bottom, back, front, right, left"
    "`chest_minecart <https://minecraft.wiki/w/chest_minecart>`_", "*all parts of* ``minecart``"
    "`command_block_minecart <https://minecraft.wiki/w/command_block_minecart>`_", "*all parts of* ``minecart``"
    "`furnace_minecart <https://minecraft.wiki/w/furnace_minecart>`_", "*all parts of* ``minecart``"
    "`hopper_minecart <https://minecraft.wiki/w/hopper_minecart>`_", "*all parts of* ``minecart``"
    "`tnt_minecart <https://minecraft.wiki/w/tnt_minecart>`_", "*all parts of* ``minecart``"
    "`spawner_minecart <https://minecraft.wiki/w/spawner_minecart>`_", "*all parts of* ``minecart``"

Block entities
==============

A `block entity <https://minecraft.wiki/w/Block_entity>`_ (or tile entity) is a block that stores additional data, beyond what blockstates can provide.

.. csv-table::
    :widths: 10, 90
    :header: "Entity name", "Part name"

    "`banner_base <https://minecraft.wiki/w/banner>`_", "stand, top"
    "`banner_flag <https://minecraft.wiki/w/banner>`_", "slate"
    "`bed <https://minecraft.wiki/w/bed>`_", "head, foot, leg1 ... leg4"
    "`bell <https://minecraft.wiki/w/bell>`_", "body, base"
    "`book <https://minecraft.wiki/w/book>`_", "cover_right, cover_left, pages_right, pages_left, flipping_page_right, flipping_page_left, book_spine"
    "`chest <https://minecraft.wiki/w/chest>`_", "base, lid, knob"
    "`chest_left <https://minecraft.wiki/w/chest_left>`_", "base, lid, knob"
    "`chest_right <https://minecraft.wiki/w/chest_right>`_", "base, lid, knob"
    "`conduit <https://minecraft.wiki/w/conduit>`_", "eye, wind, base, cage"
    "`decorated_pot <https://minecraft.wiki/w/decorated_pot>`_", "neck, front, back, left, right, top, bottom"
    "`enchanting_book <https://minecraft.wiki/w/enchanting_book>`_", "*all parts of* ``book``"
    "`enchanting_screen_book <https://minecraft.wiki/w/enchanting_screen_book>`_", "*all parts of* ``enchanting_book``"
    "`ender_chest <https://minecraft.wiki/w/ender_chest>`_", "base, lid, knob"
    "`hanging_sign <https://minecraft.wiki/w/hanging_sign>`_", "board, plank, chains, chain_left1, chain_left2, chain_right1, chain_right2, chains_v"
    "`head_creeper <https://minecraft.wiki/w/head_creeper>`_", "head"
    "`head_dragon <https://minecraft.wiki/w/head_dragon>`_", "head, jaw"
    "`head_piglin <https://minecraft.wiki/w/head_piglin>`_", "head, left_ear, right_ear"
    "`head_player <https://minecraft.wiki/w/head_player>`_", "head"
    "`head_skeleton <https://minecraft.wiki/w/head_skeleton>`_", "head"
    "`head_wither_skeleton <https://minecraft.wiki/w/head_wither_skeleton>`_", "head"
    "`head_zombie <https://minecraft.wiki/w/head_zombie>`_", "head"
    "`lectern_book <https://minecraft.wiki/w/lectern_book>`_", "*all parts of* ``book``"
    "`shulker_box <https://minecraft.wiki/w/shulker_box>`_", "base, lid"
    "`sign <https://minecraft.wiki/w/sign>`_", "board, stick"
    "`trapped_chest <https://minecraft.wiki/w/trapped_chest>`_", "base, lid, knob"
    "`trapped_chest_left <https://minecraft.wiki/w/trapped_chest_left>`_", "base, lid, knob"
    "`trapped_chest_right <https://minecraft.wiki/w/trapped_chest_right>`_", "base, lid, knob"
    "`wall_sign <https://minecraft.wiki/w/wall_sign>`_", "board, *and all parts of* ``sign``"

Model variants
==============

.. csv-table::
    :widths: 10, 90
    :header: "Model variant name", "Variant type name"

    "`<variant>_boat <https://minecraft.wiki/w/boat>`_", "oak, spruce, birch, jungle, acacia, cherry, dark_oak, pale_oak, mangrove, *and all parts of* ``boat``"
    "`<variant>_chest_boat <https://minecraft.wiki/w/chest_boat>`_", "oak, spruce, birch, jungle, acacia, cherry, dark_oak, pale_oak, mangrove, *and all parts of* ``chest_boat``"
    "<variant>_boat_patch", "oak, spruce, birch, jungle, acacia, cherry, dark_oak, pale_oak, mangrove, *and all parts of* ``boat_patch``"
    "<variant>_chest_boat_patch", "oak, spruce, birch, jungle, acacia, cherry, dark_oak, pale_oak, mangrove, *and all parts of* ``boat_patch``"
    "`<variant>_sign <https://minecraft.wiki/w/sign>`_", "oak, spruce, birch, jungle, acacia, cherry, dark_oak, pale_oak, mangrove, crimson, warped, bamboo, *and all parts of* ``sign``"
    "`<variant>_wall_sign <https://minecraft.wiki/w/wall_sign>`_", "oak, spruce, birch, jungle, acacia, cherry, dark_oak, pale_oak, mangrove, crimson, warped, bamboo, *and all parts of* ``wall_sign``"
    "`<variant>_hanging_sign <https://minecraft.wiki/w/hanging_sign>`_", "oak, spruce, birch, jungle, acacia, cherry, dark_oak, pale_oak, mangrove, crimson, warped, bamboo, *and all parts of* ``hanging_sign``"

