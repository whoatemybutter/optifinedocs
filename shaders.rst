:hero: Shimmering waves

Shaders
#######

.. figure:: images/shaders/icon.webp
  :width: 180px
  :align: right

  Shaders in action.

.. location::

    $minecraftfolder/shaderpacks/\*.zip

    $minecraftfolder/shaderpacks/\*/

**Shaders** are programs that manipulate the rendering pipeline of the game to create realistic lighting, shadows, reflections, and other visual effects.
They can significantly transform the game's appearance, making it more immersive and visually appealing.

OptiFine alone does not include shaderpacks.
Shader packs must be downloaded from other sources and installed in order to be used.

.. figure:: images/shaders/fullview.webp

    A large example of shaders in the Overworld.
    Shown here is `FastPBR <https://rre36.com/projects/fastpbr/fastpbr-v1-0-initial-release>`_.

Downloading
===========

.. figure:: images/shaders/shadersdownload.webp

In game, the Download button in the Shaders menu opens the URL `<https://optifine.net/shaderPacks>`_, which currently redirects to `<https://wiki.shaderlabs.org/wiki/Shaderpacks>`_.
This is the officially supported list of shader packs, and includes the latest updates, download links, platforms, Minecraft version, and more.

It is recommended that you download shaderpacks from this website.

Shader packs may also be downloaded from anywhere, and some of the most known websites are:

* `PlanetMinecraft <https://www.planetminecraft.com/texture-packs/>`_
* `Modrinth <https://modrinth.com/shaders>`_
* `CurseForge <https://www.curseforge.com/minecraft/search?class=customization&page=1&pageSize=20&search=Shaders&sortType=1>`_
* Shader developers' personal websites

Regardless of where a shader pack is from, it is **always** a ``.zip`` file.
If you did not get a ``.zip`` file, it is **not** a shader pack.

Installing
==========

Navigate to your Minecraft folder.
If you do not know how to do this, `click here <https://minecraft.wiki/w/.minecraft>`_.

If it does not already exist, create the ``shaderpacks`` folder.

Move the shader pack file into this ``shaderpacks`` folder.

Re-open the shaders menu and it should appear. Click on it to enable it.

Configuring
===========

Different shaders expose different configuration options.

Unless you know what you are doing, you should not change the right-hand settings at the shader screen (Normal Map, Old Lighting).

Shader options are accessed through the bottom-right button :menuselection:`Shader Options...`.

Debug shaders
=============

.. figure:: images/shaders/internal.webp

**Debug shaders** are internal shaders for debugging the shader pipeline.
Prior to J2 pre6, these were called **internal shaders**.

If you are not a shader developer, **you should never enable this**.
