:hero: *Stays at 99%*

Custom Loading Screens
######################

.. figure:: images/custom_loading_screens/icon.webp
    :align: right
    :class: pixel-edges
    :height: 120px

    The dimension switching screen.

**Custom Loading Screens** define the screen when changing worlds, loading a world, starting the game, or reloading datapacks.

.. location::

    /assets/minecraft/optifine/gui/loading/loading.properties


Custom loading screen backgrounds per dimension can be defined as: ``/assets/minecraft/optifine/gui/loading/background<DIM>.png``

Where ``<DIM>`` is the dimension ID:

* ``1``: End
* ``0``: Overworld
* ``-1``: Nether

.. note:: Mods may extend this ID list.

.. figure:: images/custom_loading_screens/mojang.webp

    The Vanilla loading screen since 1.16.

Properties
==========

.. note:: The properties ``scaleMode``, ``scale``, and ``center`` can also be configured per dimension:

    .. code:: properties

        dim<dim>.scaleMode=<fixed|full|stretch>
        dim<dim>.scale=2
        dim<dim>.center=<true|false>

``scaleMode``
-------------

.. values::
    :values: ``fixed``, ``full``, or ``stretch``
    :required: No
    :default: ``fixed``

Custom scale mode for the background texture:

* ``fixed``: use fixed scale, pixel for pixel *(default)*.
* ``full``: fullscreen, keep aspect ratio.
* ``stretch``: fullscreen, stretch picture.

``scale``
---------

.. values::
    :values: Integer
    :required: No
    :default: ``scaleMode=fixed``: ``2``, ``scaleMode=full``: ``1``

Custom scale for the background texture.

For scale mode ``fixed``, it defines the pixel scale to use.
This is combined with the curent GUI scale.

For scale modes ``full`` and ``stretch``, it defines how many tiled textures should fit on the screen.

``center``
----------

.. values::
    :values: Boolean
    :required: No
    :default: ``false``

Defines if the background texture should be centered on the screen.

JSON schema
===========

.. note:: Although this page is ``.properties`` based, it can be mapped to JSON.

.. literalinclude:: ./schemas/custom_loading_screens.schema.json
