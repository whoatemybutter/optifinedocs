JVM Arguments
#############

OptiFine supports running the game with arguments, some of which are not available in the options menu.

The system properties have to be added in the field "JVM Arguments" in the launcher profile.

.. important:: All arguments must be prefixed with ``-D``.

.. figure:: images/jvm_args/launcher_field.webp
    :align: left

    The profile's "edit" screen, with the **JVM ARGUMENTS** field selected.

.. figure:: images/jvm_args/launcher_installations.webp
    :align: right

    A list of the installations, with the "edit" drop-down option hovered.

.. csv-table::
    :header: "Argument", "Values", "Meaning"
    :widths: 20, 10, 70

    "``log.detail``", "Boolean", "Enables extended logging."
    "``saveTextureMap``", "Boolean", "Save the final texture map/atlas in the folder ``debug/``."
    "``shaders.debug.save``", "Boolean", "Save the final shader sources in the folder ``shaderpacks/debug/``."
    "``animate.model.living``", "Boolean", "Automatically animate all mob models. Useful for testing :doc:`cem`."
    "``player.models.local``", "Boolean", "Load the player models from the folder ``playermodels/``. See :doc:`cpm`."
    "``player.models.reload``", "Boolean", "Automatically reload player models every 5 seconds."
    "``frame.time``", "Boolean", "Show frame time in milliseconds instead of FPS."
    "``gl.debug.groups``", "Boolean", "Enable OpenGL debug groups."
    "``gl.ignore.errors``", "Comma-split list of integers", "Ignore `OpenGL errors <https://www.khronos.org/opengl/wiki/OpenGL_Error#Meaning_of_errors>`_ based on the comma seperated list of error IDs."
    "``cem.debug.models``", "Boolean", "CEM debug models. Automatically generate CEM models for all supported entities using different colors for each model part. The part names and colors are written in the log."
    "``chunk.debug.keys``", "Boolean", "Enables chunk debug keys, see :doc:`debug_keys`.
        .. note:: For example, to enable extended logging, add ``-Dlog.detail=true`` to the JVM arguments."
