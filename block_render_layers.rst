:hero: Like an onion

Block Render Layers
###################

.. location::

    (shaderpack)/shaders/block.properties

**Block Render Layers** changes how blocks' textures are rendered and in what order.

.. warning::

    Blocks which are solid opaque cubes (stone, dirt, ores, etc) can't be rendered on a custom layer as this would
    affect face culling, ambient occlusion, light propagation and so on.

Properties
==========

``layer.solid``
---------------

.. values::
    :values: Space-split list of blocks
    :required: No
    :default: *None*

No alpha, no blending (solid textures).

``layer.cutout``
----------------

.. values::
    :values: Space-split list of blocks
    :required: No
    :default: *None*

Alpha, no blending (cutout textures).

``layer.cutout_mipped``
-----------------------

.. values::
    :values: Space-split list of blocks
    :required: No
    :default: *None*

Alpha, no blending, mipmaps (cutout with mipmaps)

``layer.translucent``
----------------------

.. values::
    :values: Space-split list of blocks
    :required: No
    :default: *None*

Alpha, blending, mipmaps (water, stained glass).

Example
=======

.. code:: properties

    layer.translucent=glass_pane fence wooden_door
    layer.cutout=oak_stairs
    layer.solid=stone dirt

JSON schema
===========

.. note:: Although this page is ``.properties`` based, it can be mapped to JSON.

.. literalinclude:: ./schemas/block_render_layers.schema.json
