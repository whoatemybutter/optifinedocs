:hero: Purple has always been a quirky color

Colormaps
#########

.. figure:: images/colormap/vanilla_foliage.webp
    :align: right
    :height: 180px
    :class: pixel-edges

    The vanilla foliage.png.

.. location::

    /assets/minecraft/optifine/colormap/\*\*/\*

**Colormaps** modify a texture's tint based on its biome and height.

A custom colormap can consist of either a PNG file, a ``.properties`` file, or both, depending on what is intended.
OptiFine greatly expands this functionality to other blocks and to ambient sky and fog colors.

This can be used to give each biome its own color tone.

Custom colormaps can be applied to any one block or to a set of blocks.
They can also be applied to ambient fog, sky, and underwater colors.

Formats
=======

"vanilla" format
----------------

.. seealso:: See the `wiki page on tint <https://minecraft.wiki/w/Tint>`_ for more details.

.. warning:: This format is difficult to manipulate and is not recommended.

The format used by vanilla Minecraft is a 256px by 256px PNG, with the axes representing temperature and humidity, respectively.
Each biome has fixed base temperature and humidity values corresponding to a **single** pixel in the colormap.
As the **y** coordinate increases, the position in the colormap slowly moves toward the **↘** lower-right.

.. figure:: images/colormap/vanilla.webp

    An approximation of how Vanilla colormaps work.

A `forum post <http://www.minecraftforum.net/forums/mapping-and-modding/resource-packs/resource-pack-discussion/1256322-new-biome?comment=124>`__ by khanador illustrates how this works.

.. note:: The vanilla format is used for all custom colormaps as well, **unless this behavior is overridden**!

Biome colormaps use a triangular gradient by default.
However, only the colors in the lower-left half of the image are used, even though the upper-right side of ``foliage.png`` is colored.

.. figure:: images/colormap/vanilla_foliage.webp

    The Vanilla ``foliage.png`` file.
    The upper-right side of ``foliage.png`` is colored is **entirely unused**.

Furthermore, a select few pixels are considered when the colormap is read by the game and are determined by the code below.

The adjusted temperature and adjusted rainfall values are used when determining the biome color to select from the colormap.
Treating the bottom-right corner of the colormap as ``Temperature = 0.0`` and ``Rainfall = 0.0``,
the adjusted temperature increases to 1.0 along the X-axis, and the adjusted rainfall increases to 1.0 along the Y-axis.
The values used to retrieve the colors are computed as follows:

.. code:: python

    new_temperature = clamp(temperature, 0.0, 1.0)
    new_rainfall = clamp(rainfall, 0.0, 1.0) * new_temperature

"grid" format
-------------

.. seealso:: The `MCPatcher source <https://bitbucket.org/prupe/mcpatcher/wiki/Biome_Palettes_(Grid)>`_

.. location::

    /assets/minecraft/optifine/colormap/<ANY NAME>.png

An alternative format that offers finer control over each biome.

This format is similar to Vanilla's 256by by 256px format,
but the **x** coordinate represents the `biome ID number <https://minecraft.wiki/w/Biome/ID>`__,
and the **y** coordinate represents the height.

This allows complete separation between biomes and gives full control from minimum to maximum build height.

Each column in the colormap represents a single biome.

.. figure:: images/colormap/biome_grid_template.webp

Please note that the above image is "flipped" vertically:

- The **bottom** of the world (``y=0``) is at the **top** of the image.
- The normal maximum build height (``y=255``) is at the **bottom**.
- Sea level is ``y=64``.

Forward compatibility
~~~~~~~~~~~~~~~~~~~~~

Unused columns in the map represent unassigned biome IDs that may be used by either future Minecraft versions or mods.
Color schemes can be created for particular modded biomes if the IDs that they use are known.

If the IDs **aren't** known,
it is best to at least pick a neutral-looking gradient for unused columns so that new biomes will have a reasonable default appearance,
even if the pack isn't updated.

Backward compatibility
~~~~~~~~~~~~~~~~~~~~~~

The vanilla ``grass.png`` and ``foliage.png`` maps in ``/assets/minecraft/textures/colormap`` are always in the vanilla format,
regardless of any properties file setting.

This preserves compatibility for non-OptiFine users.

To use the grid format with grass or leaves, a custom colormap must be present in ``/assets/minecraft/optifine/colormap/blocks`` and be applied to the appropriate block(s).
For OptiFine users, the custom colormap overrides the vanilla one; for non-OptiFine users, only the vanilla one will be used.

Resolution
~~~~~~~~~~

While colormaps in this format are generally 256px by 256px, there is no strict requirement as there is with the vanilla format.

Minecraft 1.7 introduced rare variants of many biomes. For example, "Birch Forest M" (ID 155) is the rare version of "Birch Forest" (ID 27).

Conveniently, the rare is always common + ``128``. This fact can be utilized if all rare biomes should use the same color schemes as the corresponding non-rare ones.

Simply make the colormap 128 pixels wide instead of 256, and OptiFine will "wrap" it in the ``x`` direction when assigning columns to biomes.
Similarly, a 1 pixel wide colormap gives the same height-based color gradient across **all** biomes.

In the ``y`` direction, if more than 256 pixels are provided, OptiFine will use them if the server's build height is higher than 256, as is the case with 1.17 onward.
Similarly, if the colormap is shorter than 256 pixels, it will simply "top out" at that height, giving all blocks above that the same color as the top-most pixel of the map.

In particular, a height of 64 pixels allows for variation underground and a fixed color above sea level.

A height of 192 pixels combined with a property of ``yOffset=64`` gives just the opposite: variation above ground and a fixed color below.
A height of 1 pixel allows for variation across biomes **but not by height**.

"fixed" format
--------------

OptiFine offers a simple "fixed" colormap format.

This format **does not** require an image; it is simply a single color applied to all blocks. regardless of location.
Its primary purpose is to override certain hardcoded block colors, like sugar cane.

Properties
==========

.. location::

    /assets/minecraft/optifine/colormap/\*.properties

.. note:: The ``format`` property does not affect the vanilla "grass.png" and "foliage.png" files in ``/assets/minecraft/textures/colormap``; those are **always interpreted in the vanilla format** in order to preserve compatibility for non-OptiFine users.

If the format is not ``fixed``, the location of this file should sit in the same folder as the colormap image it will apply to.
It should have the same base name as the texture.

``format``
----------

.. values::
    :values: String: ``grid``, ``vanilla``, or ``fixed``
    :required: No
    :default: ``vanilla``

The format to use for this colormap.
If not specified, the ``vanilla`` format is used.
Has exceptions; see **NOTE** above.

``blocks``
----------

.. values::
    :values: Space-split list of blocks
    :required: No
    :default: *None*

List of blocks to apply this colormap to.
For colormaps applied to terrain (as opposed to fog, sky, and underwater), this is a list of blocks and optional property values to apply the map to.
If this property is not specified, the block name is taken from the filename: ``cobblestone.properties`` ↔ ``blocks=minecraft:cobblestone``

Example: ``blocks=stone minecraft:sand minecraft:lever:face=wall:facing=east,west``.
See :ref:`syntax:blocks, items` for more information.

``source``
----------

.. values::
    :values: |file path|
    :required: No
    :default: *None*

File path to colormap texture.

.. note:: This is for ``vanilla`` and ``grid`` colormaps only.

If this property is omitted, the colormap defaults to a PNG with the same name and directory as the properties file itself: ``stone.properties`` <-> ``source=stone.png``.

``color``
---------

.. values::
    :values: String: hexadecimal RGB value without leading ``#``
    :required: No
    :default: ``ffffff``

Differing behavior depending on ``format``:

.. md-tab-set::

    .. md-tab-item:: ``format=fixed``

        This color will be applied to all matching blocks.

    .. md-tab-item:: ``format=vanilla`` or ``format=grid``

        This color is used for held and dropped blocks.

``yVariance``
-------------

.. values::
    :values: Integer
    :required: No
    :default: ``0``

If set, this property adds a random integer to the Y coordinate before sampling from the colormap, giving flat areas a more varied appearance.
A value of ``2`` causes the game to pick a Y coordinate of ``y + randomInteger(0, 2)``.

.. note:: This only applies to the ``grid`` format.

``yOffset``
-----------

.. values::
    :values: Integer
    :required: No
    :default: ``0``

Subtracts a fixed value from the block's **Y** coordinate in the world before sampling from the colormap.

For example, a value of ``64`` will use the pixel at ``y=0`` for blocks on Y-level ``64``.
A block at ``y=65`` will use pixel ``1``. A block whose ``y=66`` uses pixel ``2``, and so on.

Applying a colormap
===================

Block-based colormaps can be applied in one of two ways:

.. md-tab-set::

    .. md-tab-item:: As a list in ``assets/minecraft/optifine/color.properties``

        .. warning:: If the player is using multiple resource packs, only the *first* ``color.properties`` file will be read by the game.

        .. csv-table::
           :header: "Key", "Values", "Meaning"

           "``palette.block.<colormap image>``", ".. values::
            :values: List of blocks
            :required: No", "Assigns each block's colormap", "*None*"

        For example, the below assigns Oak Leaves and tall grass their own
        colormaps:

        - ``palette.block.colormap/oak.png=oak_leaves``
        - ``palette.block.colormap/tall_grass_up.png=tall_grass:half=upper``
        - ``palette.block.colormap/tall_grass_low.png=tall_grass:half=lower``

    .. md-tab-item:: As separate files under ``assets/minecraft/optifine/colormap/blocks``

        .. important:: This is assuming "oak.png", "tall_grass_up.png", and "tall_grass_low.png" are all in the same folder.

        Subfolders are allowed and are useful to make organization easier.
        The left-side tabbed example could also be done this way:

        * In ``assets/minecraft/optifine/colormap/blocks/oak.properties``: ``blocks=oak_leaves``
        * In ``assets/minecraft/optifine/colormap/blocks/tall_grass_up.properties``: ``blocks=tall_grass:half=upper``
        * In ``assets/minecraft/optifine/colormap/blocks/tall_grass_low.properties``: ``blocks=tall_grass:half=lower``

Grass and foliage
=================

Custom colormaps will override the vanilla ``grass.png`` and ``foliage.png``.
This means vanilla maps can be left in place for compatibility, creating custom ones for OptiFine users.

Biome grass and foliage colors are selected from two 256px by 256px colormap images: ``grass.png`` and ``foliage.png``.
Both colormaps, shown below, can be found in ``/assets/minecraft/textures/colormap/``.

.. list-table::

   * - .. figure:: images/colormap/vanilla_grass.webp

         The Vanilla ``grass.png`` file sets the colors for the grass block top and sides (along with other types of grass, such as tall grass, ferns, double tall grass, etc.).

     - .. figure:: images/colormap/grass_and_foliage.webp

         A template for foliage colormaps, created by Rodrigo Al.

     - .. figure:: images/colormap/vanilla_foliage.webp

         The Vanilla ``foliage.png`` file sets the colors for tree leaves (with the exception of spruce and birch).

.. note:: ``blocks=grass`` property is not needed since it is in the filename.

In ``assets/minecraft/optifine/colormap/blocks/grass.properties``:

.. code:: properties

    format=grid
    yVariance=2

In ``assets/minecraft/optifine/colormap/blocks/oak.properties``:

.. code:: properties

    format=grid
    blocks=oak_leaves

Swamp, mesa colors
==================

Vanilla Minecraft has no support for colormaps on Swamp and Mesa biomes.
This is `intentional <https://bugs.mojang.com/browse/MC-30445>`_, but OptiFine can override this behavior.

.. figure:: images/colormap/swamp_colors.webp

    Button and tooltip for the option, found in :menuselection:`Video Settings --> Quality`.

Fixing sugar cane in 1.7+
=========================

.. note:: This only applies to 1.7 and above

From 1.7 onward, Minecraft applies the ``grass.png`` color to sugar cane.

A ``fixed`` colormap of ``ffffff`` (white) effectively reverts to the 1.6- behavior.
A 256px by 256px all-white colormap would accomplish the same thing, but this method is more efficient.

The simplest way to do this is to create a properties file containing just one line:

In ``assets/minecraft/optifine/colormap/blocks/reeds.properties``:

.. code:: properties

    format=fixed

This works because the blocks property defaults to the filename (reeds) and the color property defaults to ``ffffff`` for fixed colormaps.

Other colors
============

.. note:: These behave like terrain-based ones, except that they do not care about the blocks property.

.. figure:: images/colormap/water.webp

   A template for water colormaps, created by Rodrigo Al.

These specifically named colormaps **override** the default fixed ambient colors:

.. csv-table::
    :header: "Key", "Meaning"

    "``~/colormap/redstone.png``", "16px x 1px redstone colors (``0``: fully off, ``15``: fully on)."
    "``~/colormap/pumpkinstem.png``", "8px x 8px pumpkin stem colors (``0``: sprout, ``7``: fully grown)."
    "``~/colormap/melonstem.png``", "8px x 8px melon stem colors (``0``: sprout, ``7``: fully grown)."
    "``~/colormap/lavadrop.png``", "``<T>`` px x 1px lava drop colors (``<T>``: age of particle in ticks)."
    "``~/colormap/myceliumparticle.png``", "Any size, random mycelium particle colors."
    "``~/colormap/xporb.png``", "Any size, array of experience orb colors."
    "``~/colormap/durability.png``", "Any size, array of item durability colors."
    "``~/colormap/swampgrass.png``", "256px x 256px swamp grass color palette."
    "``~/colormap/swampfoliage.png``", "256px x 256px swamp foliage color palette."
    "``~/colormap/pine.png``", "256px x 256px **spruce** tree color palette."
    "``~/colormap/birch.png``", "256px x 256px birch tree color palette."
    "``~/colormap/water.png``", "256px x 256px water color palette."
    "``~/colormap/underwater.png``", "256px x 256px underwater color."
    "``~/colormap/underlava.png``", "256px x 256px underlava color."
    "``~/colormap/fog0.png``", "256px x 256px fog color for the overworld."
    "``~/colormap/sky0.png``", "256px x 256px sky color for the overworld."

Each file can have a corresponding properties file to specify the format or other settings.

Examples
========

Single block
------------

This is the most simple case.
Since a custom colormap is applied to a single block type, **it does not need a properties file**.

For example, ``assets/minecraft/optifine/colormap/blocks/sand.png`` applies to sand blocks without the need to specify ``blocks=sand`` in the colormap properties file.

Multiple blocks
---------------

.. note:: The ``source`` property is unneeded if the colormap is *also* named the same.

To apply the same colormap to all stone and ore blocks, use a properties file:

In ``assets/minecraft/optifine/colormap/blocks/stone_and_ore.properties``:

.. code:: properties

    blocks=stone gold_ore iron_ore coal_ore lapis_ore diamond_ore redstone_ore redstone_ore:lit=true emerald_ore

Use ``format=grid`` if using the ``grid`` format.

In ``color.properties`` this can also be written as:

.. code:: properties

    palette.block.~/colormap/custom/stone.png=stone gold_ore iron_ore coal_ore lapis_ore diamond_ore redstone_ore redstone_ore:lit=true emerald_ore

Add ``palette.format=grid`` to use grid format for all the custom colormaps (except the vanilla "grass.png" and "foliage.png").

Custom biome palettes
=====================

.. note::

    | This does not affect the vanilla foliage and grass colormaps in ``/assets/minecraft/textures/colormap``.
    | This can be overridden per-colormap in each individual properties file in ``~/colormap/custom``.

Custom biome palettes may be assigned to any standard block (one that does not already have its own special color multiplier method).
Each custom colormap should have a ``.properties`` file in ``~/colormap/custom``.

In Vanilla Minecraft, the grass and leaf textures vary in color depending on the climate of the surrounding biome.
This is controlled by two files:

1. ``/assets/minecraft/textures/colormap/grass.png``
2. ``/assets/minecraft/textures/colormap/foliage.png``

Each file is a 256px x 256px colormap applied to the base grass or leaf texture (which is usually grayscale).

External links
==============

.. warning:: |elwarn|

* `Default 1.20 Grid Colormaps (+PSD) <https://www.planetminecraft.com/texture-pack/default-1-16-optifine-grid-colormaps-for-resource-pack-developers/>`_

JSON schema
===========

.. note:: Although this page is ``.properties`` based, it can be mapped to JSON.

.. literalinclude:: ./schemas/colormap.schema.json
