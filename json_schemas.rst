JSON Schemas
############

Throughout this documentation,
many resource pack feature pages have a corresponding `JSON schema <https://json-schema.org>`_ file attached at the bottom.

These files are intended to be used for validating resource packs that use OptiFine.

Features that use a ``.properties``-based system will need to have their files mapped to simple JSON files:

.. code-block:: properties
    :caption: Properties in

    a=4
    b=somestring
    c="quotes"

.. code-block:: javascript
    :caption: JSON out

    {
        "a": 4,
        "b": "something",
        "c": "\"quotes\""
    }

List
====

.. csv-table::

    ":download:`Better Grass <schemas/better_grass.schema.json>`", ":ref:`page <better_grass:json schema>`"
    ":download:`Block Render Layers <schemas/block_render_layers.schema.json>`", ":ref:`page <block_render_layers:json schema>`"
    ":download:`CEM Animation <schemas/cem_anim.schema.json>`", ":ref:`page <cem_animation:json schema>`"
    ":download:`CEM Model <schemas/cem_model.schema.json>`", ":ref:`page <cem_models:json schema>`"
    ":download:`CEM Part <schemas/cem_part.schema.json>`", ":ref:`page <cem_parts:json schema>`"
    ":download:`CIT <schemas/cit.schema.json>`", ":ref:`page <cit:json schema>`"
    ":download:`CIT Global <schemas/cit_global.schema.json>`", ":ref:`page <cit:global properties>`"
    ":download:`Colormaps <schemas/colormap.schema.json>`", ":ref:`page <colormaps:json schema>`"
    ":download:`CPM <schemas/cpm_player.schema.json>`", ":ref:`page <cpm>`"
    ":download:`CTM <schemas/ctm.schema.json>`", ":ref:`page <ctm:json schema>`"
    ":download:`Custom Animations <schemas/custom_animations.schema.json>`", ":ref:`page <custom_animations:json schema>`"
    ":download:`Custom Loading Screens <schemas/custom_loading_screens.schema.json>`", ":ref:`page <custom_loading_screens:json schema>`"
    ":download:`Custom Panoramas <schemas/custom_panoramas.schema.json>`", ":ref:`page <custom_panoramas:json schema>`"
    ":download:`Custom Sky <schemas/custom_sky.schema.json>`", ":ref:`page <custom_sky:json schema>`"
    ":download:`Dynamic Lights <schemas/dynamic_lights.schema.json>`", ":ref:`page <dynamic_lights:json schema>`"
    ":download:`Emissive Textures <schemas/emissive_textures.schema.json>`", ":ref:`page <emissive_textures:json schema>`"
    ":download:`HD Fonts <schemas/hd_fonts.schema.json>`", ":ref:`page <hd_fonts:json schema>` *(deprecated)*"
    ":download:`Natural Textures <schemas/natural_textures.schema.json>`", ":ref:`page <natural_textures:json schema>`"
    ":download:`Random Entities <schemas/random_entities.schema.json>`", ":ref:`page <random_entities:json schema>`"
    ":download:`Texture Properties <schemas/texture_properties.schema.json>`", ":ref:`page <texture_properties:json schema>`"
