Install With Vanilla Launcher
#############################

.. image:: images/install/vanilla.webp
    :align: center

.. important::
    To install OptiFine for a version, that same Vanilla version **must be installed** *and* ran beforehand.
    If the version is not installed, **or** if it has not been played before, OptiFine will not install.

1. Follow the instructions at :doc:`download`.
2. Run the downloaded ``.jar`` file.
3. Follow the instructions and click "Install".
4. Open the Minecraft launcher and select the OptiFine profile, and click "Play".

.. figure:: images/install/installer.webp

    The OptiFine installer
