:hero: Become a real keyboard warrior

Debug Keys
##########

**Debug Keys** are a feature of `Vanilla Minecraft <https://minecraft.wiki/w/Debug_screen#More_debug-keys>`_ that perform various debugging functions
when a specific key combination is pressed, usually involving the :keys:`F3` button.

**In addition** to the Vanilla set of debug keys, OptiFine adds some of its own.

.. csv-table::
    :header: "Keys", "Result"

    ":keys:`F3 + V`", "Load all visible chunks"
    ":keys:`F3 + O`", "Open shader options"
    ":keys:`F3 + R`", "Reload shaders"

.. csv-table:: Requires :doc:`system property <jvm_args>` ``chunk.debug.keys``
    :header: "Keys", "Result"

    ":keys:`F3 + E`", "Show chunk path"
    ":keys:`F3 + L`", "Smart cull"
    ":keys:`F3 + U`", "Capture frustum"
    ":keys:`Alt + F3 + U`", "Capture shadow frustum"
    ":keys:`Shift + F3 + U`", "Release frustum"
    ":keys:`F3 + V`", "Chunk visibility"
